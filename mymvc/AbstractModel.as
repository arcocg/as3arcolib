﻿package arco.mymvc
{
	import flash.events.EventDispatcher;
	/**
	 * AbstractModel
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * Base class for all Models in MYMVC
	 * 
	 * @author arcomailbox@gmail.com
	 */
	
	public class  AbstractModel extends EventDispatcher
	{
		public static const STATE_INIT:String = "stateInit";
		
		protected var _lastError:String; //Last error occurs in Model
		
		protected var _state:String;
		protected var _stateOld:String;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function AbstractModel(){
			changeState(STATE_INIT);
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		public function init():void 
		{
			dispatchEvent(new AbstractModelEvent(AbstractModelEvent.INITED));
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		/**
		* This method should be overriden to hadle state chage
		* 
		* @param instate	State model changed to
		**/
		protected function changeState(instate:String):void 
		{
			if (_state != instate) {
				_stateOld = _state;
				_state = instate;
				dispatchEvent(new AbstractModelEvent(AbstractModelEvent.STATE_CHANGED, { state:_state, oldstate:_stateOld} ));
			}
			
		}
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		
		
		public function get lastError():String {
			return(_lastError);
		}
		
		
		public function get state():String { return _state; }
		public function set state(value:String):void 
		{
			if (_state == value) return;
			
			_stateOld = _state;
			_state = value;
			
			changeState(_state);
			
		}
	}
	
}
