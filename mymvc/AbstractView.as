﻿package arco.mymvc
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;

	/**
	 * AbstractView
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * Base class for all Views in MYMVC
	 * 
	 * @author arcomailbox@gmail.com
	 */
	
	public class AbstractView extends MovieClip
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		protected var _model:AbstractModel;
		protected var _controller:AbstractController;
		protected var _skin:MovieClip; //MovieClip with view's graphic interface
		
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function AbstractView(inmodel:AbstractModel, incontroller:AbstractController, inskin:MovieClip){
			this._model = inmodel;
			this._controller = incontroller;
			this.skin = inskin;
			
			/*inskin.x = 0;
			inskin.y = 0;
			inskin.rotation = 0;*/
			
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, onStage_added);
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		private function onStage_added(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onStage_added);
			addEventListener(Event.REMOVED_FROM_STAGE, onStage_removed);
			init();
		}
		
		private function onStage_removed(e:Event):void 
		{
			remove();
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		//---------init
		///Init all View stuff here, calls from ADDED_TO_STAGE handler, super.init() should called after all init in children class
		public function init():void 
		{
			_controller.registerEventHandlers();
		}
		
		//------hide
		public function hide():void {
			_controller.unregisterEventHandlers();
			this.visible = false;
		}
		
		//------show
		public function show():void {
			_controller.registerEventHandlers();
			this.visible = true;
		}
		
		//------remove
		///Remove View stuff here
		protected function remove():void 
		{
			_controller.unregisterEventHandlers();
			removeEventListener(Event.REMOVED_FROM_STAGE, onStage_removed);
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		public function get skin():MovieClip 
		{
			return(_skin);
		}
		
		public function set skin(inskin:MovieClip):void
		{
			_skin = inskin;
			
			this.x = _skin.x;
			this.y = _skin.y;
			this.rotation = _skin.rotation;
			_skin.x = _skin.y = _skin.rotation = 0;
		
			//this.stage.addChild(_skin);
			if (_skin) {
				addChild(_skin);
			}
		}
		
		public function get controller():AbstractController {
			return (_controller);
		}
	}
	
}