﻿package arco.mymvc
{
	import flash.display.MovieClip;
	
	/**
	 * MVCLinker
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * @playerversion AIR 1.5;
	 * 
	 * @author arcomailbox@gmail.com
	 * 
	 * Creates MVC triad
	 * 
	 */
	
	public final class MVCCreator 
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		public static const VERSION:Number = 0.53;
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		private static var _instance:MVCCreator = new MVCCreator();
		private static var _model:AbstractModel;
		private static var _view:AbstractView;
		private static var _controller:AbstractController;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function MVCCreator()
		{
            if(_instance) throw new Error ("Singleton is a singleton class, use getInstance() instead");	
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		///Create MVC Triad taking already created Model and creates View and Controller by given classes
		///Result objects returned thru getters view() and controller()
		public static function create(inmodel:AbstractModel, inviewClass:Class, incontrollerClass:Class, inskin:MovieClip = null, inview:AbstractView = null, incontroller:AbstractController = null):void
		{
			_model = inmodel;
			
			//Create new or use existing controller
			if (!incontroller) _controller = new incontrollerClass(inmodel);
			else  _controller = incontroller;
			
			//Create new or use existing view
			if (!inview) _view = new inviewClass(_model , _controller, (inskin)?inskin:(new MovieClip()));
			else _view = inview;
			
			_controller.view = _view;
			//incontrollerClass(_controller).init();
		}
		
		public static function getInstance():MVCCreator {
			return (_instance);
		}
		
		public static function get view():AbstractView {
			return(_view);
		}
		
		public static  function get controller():AbstractController{
			return(_controller);
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}
	
}