﻿package arco.mymvc
{
	import flash.events.MouseEvent;
	/**
	 * AbstractController
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * Base class for all Controllers in MYMVC
	 * 
	 * @author arcomailbox@gmail.com
	 */
	
	public class AbstractController 
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		protected var _model:AbstractModel;
		protected var _view:AbstractView;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function AbstractController(inmodel:AbstractModel, inview:AbstractView = null){
			_model = inmodel;
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		protected function onViewClick(e:MouseEvent):void 
		{
			
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		public function registerEventHandlers():void 
		{
			_view.addEventListener(MouseEvent.MOUSE_UP, onViewClick);
		}
		
		public function unregisterEventHandlers():void 
		{
			_view.removeEventListener(MouseEvent.MOUSE_UP, onViewClick);
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------	
		public function set view(inview:AbstractView):void {
			_view = inview;
		}
	}
	
}