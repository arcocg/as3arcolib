﻿package arco.mymvc.starling
{
	import arco.mymvc.AbstractModel;
	import arco.mymvc.AbstractView;
	import flash.events.MouseEvent;
	/**
	 * AbstractControllerStarling
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * Base class for all Controllers in MYMVC for Starling framework
	 * 
	 * @author arcomailbox@gmail.com
	 */
	
	public class AbstractControllerStarling 
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		protected var _model:AbstractModel;
		protected var _view:AbstractViewStarling;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function AbstractControllerStarling(inmodel:AbstractModel, inview:AbstractViewStarling = null){
			_model = inmodel;
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		protected function onViewClick(e:MouseEvent):void 
		{
			
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		public function registerEventHandlers():void 
		{
			_view.addEventListener(MouseEvent.MOUSE_UP, onViewClick);
		}
		
		public function unregisterEventHandlers():void 
		{
			_view.removeEventListener(MouseEvent.MOUSE_UP, onViewClick);
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------	
		public function set view(inview:AbstractViewStarling):void {
			_view = inview;
		}
	}
	
}