﻿package arco.mymvc.starling
{
	import arco.mymvc.starling.AbstractControllerStarling;
	import arco.mymvc.AbstractModel;
	import flash.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;

	/**
	 * AbstractViewStarling
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * Base class for all Views in MYMVC for Starling framework
	 * 
	 * @author arcomailbox@gmail.com
	 */
	
	public class AbstractViewStarling extends Sprite
	
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		protected var _model:AbstractModel;
		protected var _controller:AbstractControllerStarling;
		protected var _skin:MovieClip; //MovieClip with view's graphic interface
		
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function AbstractViewStarling(inmodel:AbstractModel, incontroller:AbstractControllerStarling, inskin:MovieClip) {
			
			this._model = inmodel;
			this._controller = incontroller;
			if (inskin) this.skin = inskin;
			
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, onStage_added);
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		private function onStage_added(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onStage_added);
			addEventListener(Event.REMOVED_FROM_STAGE, onStage_removed);
			init();
		}
		
		private function onStage_removed(e:Event):void 
		{
			remove();
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		//---------init
		///Init all View stuff here, calls from ADDED_TO_STAGE handler, super.init() should called after all init in children class
		public function init():void 
		{
			_controller.registerEventHandlers();
		}
		
		//------hide
		public function hide():void {
			_controller.unregisterEventHandlers();
			this.visible = false;
		}
		
		//------show
		public function show():void {
			_controller.registerEventHandlers();
			this.visible = true;
		}
		
		//------remove
		///Remove View stuff here
		protected function remove():void 
		{
			_controller.unregisterEventHandlers();
			removeEventListener(Event.REMOVED_FROM_STAGE, onStage_removed);
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		public function get skin():MovieClip 
		{
			return(_skin);
		}
		
		public function set skin(inskin:MovieClip):void
		{
			_skin = inskin;
			
			this.x = _skin.x;
			this.y = _skin.y;	
			this.rotation = _skin.rotation;
			_skin.x = _skin.y = _skin.rotation = 0;
		
			//this.stage.addChild(_skin);
			/*if (_skin) {
				addChild(_skin);
			}*/
		}
		
		public function get controller():AbstractControllerStarling {
			return (_controller);
		}
	}
	
}