﻿package arco.mymvc.starling
{
	import arco.mymvc.AbstractModel;
	import flash.display.MovieClip;
	/**
	 * MVCLinker
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * @playerversion AIR 1.5;
	 * 
	 * @author arcomailbox@gmail.com
	 * 
	 * Creates MVC triad
	 * 
	 */
	
	public final class MVCCreatorStarling 
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		public static const VERSION:Number = 0.53;
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		private static var _instance:MVCCreatorStarling = new MVCCreatorStarling();
		private static var _model:AbstractModel;
		private static var _view:AbstractViewStarling;
		private static var _controller:AbstractControllerStarling;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function MVCCreatorStarling()
		{
            if(_instance) throw new Error ("Singleton is a singleton class, use getInstance() instead");	
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		///Create MVC Triad taking already created Model and creates View and Controller by given classes
		///Result objects returned thru getters view() and controller()
		public static function create(inmodel:AbstractModel, inviewClass:Class, incontrollerClass:Class, inskin:MovieClip = null, inview:AbstractViewStarling = null, incontroller:AbstractControllerStarling = null):void
		{
			_model = inmodel;
			
			//Create new or use existing controller
			if (!incontroller) _controller = new incontrollerClass(inmodel);
			else  _controller = incontroller;
			
			//Create new or use existing view
			if (!inview) _view = new inviewClass(_model , _controller, (inskin)?inskin:null);
			else _view = inview;
			
			_controller.view = _view;
			//incontrollerClass(_controller).init();
		}
		
		public static function getInstance():MVCCreatorStarling {
			return (_instance);
		}
		
		public static function get view():AbstractViewStarling {
			return(_view);
		}
		
		public static  function get controller():AbstractControllerStarling{
			return(_controller);
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}
	
}