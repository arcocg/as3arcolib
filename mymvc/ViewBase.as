package arco.mymvc 
{
	import arco.mymvc.AbstractController;
	import arco.mymvc.AbstractModel;
	import arco.mymvc.AbstractView;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.utils.Dictionary;
	
		
	/**
	 * ViewBase
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class ViewBase extends AbstractView
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		private var _activeStates:Dictionary;
		protected var _registered:Boolean = false;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------ViewBase
		public function ViewBase(inmodel:AbstractModel, incontroller:AbstractController, inskin:MovieClip, inactivestates:Array = null) 
		{			
			super(inmodel, incontroller, inskin);
			
			
			if (inactivestates) {
				_activeStates = new Dictionary(true);
				for (var i:int = 0; i < inactivestates.length; i++) 
					_activeStates[inactivestates[i]] = true;
			}
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		protected function onStage_resize(e:Event):void 
		{
			resize();
		}
		
		protected function onModelState_changed(e:AbstractModelEvent):void 
		{
			
			if (!_activeStates || _activeStates[e.data.state] ) {
				if (!_registered) register();
				if (!visible) show();
			}
			else if (!_activeStates[e.data.state]) {
				if (_registered) unregister(); 
				if (visible) hide();
			}
		}
		
		protected function onModel_inited(e:AbstractModelEvent):void 
		{
			_model.removeEventListener(AbstractModelEvent.INITED, onModel_inited);
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		override public function init():void 
		{
			super.init();
			
			checkActiveStates();
			_model.addEventListener(AbstractModelEvent.INITED, onModel_inited);
			_model.addEventListener(AbstractModelEvent.STATE_CHANGED, onModelState_changed);
		}
		//--------------------------------------	
		//  PRIVATE METHODS
		//--------------------------------------
		protected function checkActiveStates():void 
		{
			if (_activeStates) {
				if (_activeStates[_model.state]) {
					register();
					show();
				} else {
					hide();
				}
			}
			
		}
		protected function resize():void 
		{
		}
		
		/**
		* Should be overrided to register all stuff needs be changed
		**/
		protected function register():void 
		{
			_registered = true;
			startResize();
		}
		
		override public function show():void 
		{
			super.show();
			resize();
		}
		
		/**
		* Should be overrided to unregister all stuff when exiting
		**/
		protected function unregister():void 
		{
			_registered = false;
			stopResize();
		}
	
		protected function startResize():void 
		{
			//if (stage) stage.addEventListener(Event.RESIZE, onStage_resize);
		}
		
		protected function stopResize():void 
		{
			//if (stage) stage.removeEventListener(Event.RESIZE, onStage_resize);
		}
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		public function set activeStates(value:Array):void 
		{			
			if (value) {
				_activeStates = new Dictionary(true);
				for (var i:int = 0; i < value.length; i++) 
					_activeStates[value[i]] = true;
				
				checkActiveStates();
			}
			
		}

		//public function get model():ModelMain { return _model as ModelMain; }
	}

}