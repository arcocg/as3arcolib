package arco.socnet 
{
	import arco.socnet.api.ISocnetWrapper;
	import arco.socnet.api.SocNetAPIOk2;
	import arco.socnet.api.SocNetAPIVk;
	import arco.socnet.api.SocNetAPIVkIFrame;
	import arco.web.WebDoc;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import kz.edde.racing.Game;
	
	import arco.utils.Dbg;
	
	import arco.socnet.api.ISocNetAPI;
	import arco.socnet.api.SocNetAPIVk;
		
	/**
	 * SocNetWrapper
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * Wrapper for SocNets
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class SocNetWrapper extends MovieClip implements ISocnetWrapper
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		private var _params:Object; //Flash-vars parameters 
		private var _socnetType:String = SocNetType.SOCNET_TYPE_NONE; //Type of SocNet
		private var _socnetTestmodeType:String;
		private var _socnetAPI:ISocNetAPI = null; //API module
		private var _wrapper:Object; //Socnet wrapper for instant calls
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		private var _fTestMode:Boolean = false;
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function SocNetWrapper(inTestmodeType:String = null) 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			_socnetTestmodeType = inTestmodeType;
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		private function onAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			init();
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		//------init
		protected function init():void 
		{
			
			_params = (_wrapper)?_wrapper.loaderInfo.parameters:root.loaderInfo.parameters; //Get flash-vars params
			_wrapper = parent.parent;
			initSocnet(_params); //Init SocNet API
			
			
			Dbg.log("[SocNetWrapper] init: _wrapper = " + _wrapper );
			Dbg.log("[SocNetWrapper] init: socnetType = " + _socnetType);

		}
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//------initSocNetType
		/**
		* Initizllize _socnetType by checking vars setted for application
		* 
		* @param inparams Params recieved by application thru flash vars
		* 
		**/
		private function initSocNetType(inparams:Object):void
        {
			if (inparams["app_id"] != undefined && inparams["vid"] && inparams["window_id"] != undefined)
				_socnetType = SocNetType.SOCNET_TYPE_MM;
			if (inparams["application_key"] != undefined && inparams["api_server"] && inparams["logged_user_id"] != undefined)
				_socnetType = SocNetType.SOCNET_TYPE_OK;
			if (inparams["api_url"] != undefined && inparams["api_id"] && inparams["auth_key"] != undefined)
				_socnetType = SocNetType.SOCNET_TYPE_VK;
        }
		
		//------initSocnet
		private function initSocnet(inparams:Object):void 
		{
			initSocNetType(_params );
			
			switch (_socnetType) 
			{
				
				case SocNetType.SOCNET_TYPE_OK:
					CONFIG::DEV_OK {
						_socnetAPI = new SocNetAPIOk2(stage, inparams["application_key"], Game.SOCNET_OK_SECRET, inparams["api_server"]);
						trace("[SocNetWrapper] initSocnet: _socnetAPI = " + _socnetAPI );
					}
				break;				
				case SocNetType.SOCNET_TYPE_VK:
					CONFIG::DEV_VK {
						Dbg.log("[SocNetWrapper] initSocnet: _params['secret'] = " + _params["secret"] );
						Dbg.log("[SocNetWrapper] initSocnet: _params['sid'] = " + _params["sid"] );
						Dbg.log("[SocNetWrapper] initSocnet: _params['viewer_id'] = " + _params["viewer_id"] );
						_socnetAPI = new SocNetAPIVkIFrame(_params["api_url"], _params["viewer_id"], _params["api_id"], _params["secret"], _wrapper, false, _params["sid"]);
					}
				break;
				
				case SocNetType.SOCNET_TYPE_MM:
					CONFIG::DEV_MM {}
				break;
				case SocNetType.SOCNET_TYPE_NONE:
					initTestMode();
				break;	
			}
			
			dispatchEvent(new SocnetWrapperEvent(SocnetWrapperEvent.INITED, { socnet:_socnetType } ));
		}
			
		//------initTestMode
		private function initTestMode(inparams:Object = null):void 
		{
			_fTestMode = true;
		}
		
		
		/* INTERFACE arco.socnet.api.ISocnetWrapper */
		
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		public function get api():ISocNetAPI
		{
			return(_socnetAPI);
		}
		
		public function get isTestMode():Boolean 
		{
			return(_fTestMode);
		}
		
		public function get params():Object { return _params; }
		
		/**
		* Type of socnet defined in SocNetType	
		**/
		public function get socnetType():String {	return _socnetType; }
	}

}