package arco.socnet.api 
{
	
	/**
	 * ...
	 * @author arco
	 */
	public interface ISocnetWrapper 
	{
		function get api():ISocNetAPI;
		function get params():Object;
	}
	
}