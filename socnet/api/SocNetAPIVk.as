package arco.socnet.api
{
	import arco.socnet.api.ISocNetAPI;
	import arco.utils.CommonUtils;
	import com.adobe.images.JPGEncoder;
	import com.adobe.serialization.json.JSON;
	import com.greensock.TweenMax;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import ru.inspirit.net.MultipartURLLoader;
	import ru.inspirit.net.events.MultipartURLLoaderEvent;
	
	import vk.VkApi;
	
	import arco.utils.Dbg;
		
	/**
	 * SocNetAPIVk
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class SocNetAPIVk extends SocNetAPIBase implements ISocNetAPI
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		private var _vkApi:VkApi;
		private var _vkApiProfiles:VkApi;
		private var _responsesCache:Dictionary = new Dictionary();
		private var _uid:String;
		private var _wrapper:Object;
		
		private var _photoUploadAID:String; //Id of album where photo uploaded to
		private var _photoUploadBmpd:BitmapData; 
		private var _photoUploadData:Object = null; 
		
		private var _wallUploadID:String;
		private var _wallUploadBmpd:BitmapData;
		private var _wallUploadMsg:String;
		private var _wallUploadData:Object = null;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------SocNetAPIVk
		public function SocNetAPIVk(inapiurl:String, inuid:String, inappid:String, insecret:String, inwrapper:Object = null, intestmode:Boolean = false, insessionid:String = null ) 
		{
			init();
			Dbg.log("[SocNetAPIVk] SocNetAPIVk: intestmode = " + intestmode );
			_vkApi = new VkApi(this, inapiurl, uint(inuid), uint(inappid), insecret, intestmode, insessionid);
			_vkApiProfiles = new VkApi(this, inapiurl, uint(inuid), uint(inappid), insecret, intestmode, insessionid);
			
			_uid = inuid;
			_wrapper = inwrapper;
			Dbg.log("[SocNetAPIVk] SocNetAPIVk: _wrapper = " + _wrapper );
			
			
			
			TweenMax.delayedCall(0.1, init);
			//_vkApi.getWallUploadServer();
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		 * init
		 * 
		 * @param 
		**/
		public function init():void{
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.INITED));
		}
		
		/* INTERFACE arco.socnet.api.ISocNetAPI */
		
		public function createAlbum(title:String, privacy:uint = 0, comment_privacy:uint = 0, description:String = ""):void 
		{
			_vkApi.createAlbum(title, privacy, comment_privacy, description);
		}
		
		public function getAppFriends(inuseCache:Boolean = true):void
		{
			if (inuseCache && _responsesCache[SocNetApiEvent.ON_APP_FRIENDS]!=undefined) dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_APP_FRIENDS));
			else _vkApi.getAppFriends();
		}
		
		public function getFriends(inuseCache:Boolean = true):void
		{
			if (inuseCache && _responsesCache[SocNetApiEvent.ON_FRIENDS]!=undefined) dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_FRIENDS));
			else _vkApi.getFriends();
		}
		
		public function getUserBalance():void
		{
			_vkApi.getUserBalance();
		}
		
		public function getUserAlbums(inuid:String = "", inaids:Array = null):void
		{
			_vkApi.getUserAlbums((inuid == "")?_uid:inuid);
		}
		/**
		 * Requests Vk for user profiles
		 * 
		 * @param uids		UIDs of requested profiles 
		 * @param fields	Fields needed in profile
		 * @return Unique ID for request, on complete event it come in @id param
		**/
		public function getUsersProfiles(uids:Array, fields:String = null):Number
		{
			var reqid:Number = CommonUtils.getUniqueID();
			var arrProfilesToRequest:Array = getProfilesNeededToRequest(uids);
			if 	(!fields) fields = "first_name,last_name,photo"; //,nickname,sex, 
			
			//No need to requests profiles, all profiles in cache
			if (arrProfilesToRequest.length == 0) TweenMax.delayedCall(0.1, function():void { dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_USERS_PROFILES, { id: reqid } )) } );
			else _vkApiProfiles.getUserProfiles(arrProfilesToRequest.join(","), fields, { id:reqid } );
			
			return reqid;
		}
		
		public function getUserSettings():void
		{
			
		}
		
		public function  saveToStream(immessage:String, inattachment:Object):void 
		{
			dispatchNotAvailable("saveToStream");
		}
		
		/**
		* Shows box to invite friends
		**/
		public function showInviteBox(intext:String = ""):void {
			if (!_wrapper)	dispatchNotAvailable("showInviteBox");
			else _wrapper.external.showInviteBox();
		}
		
		/**
		* Shows box with transfering vk golosa to application account
		**/
		public function showPaymentBox(inammount:uint, inname:String = "", incode:String = "", inoptions:Object = null):void {
			
			if (!_wrapper) dispatchNotAvailable("showPaymentBox");
			else {
				_wrapper.external.showPaymentBox(inammount);
			}
		}
		
		public function showPermissionstBox(inpermessions:Array):void 
		{
			dispatchNotAvailable();
		}
		
		/**
		* Uploads photo in specified album
		* 
		* @param inalbumid	ID of album
		* @param inphoto	BitmapData of uploaded photo 
		**/
		public function uploadPhoto(inalbumid:String, inphoto:BitmapData):void 
		{		
			_photoUploadBmpd = inphoto;
			_photoUploadAID = inalbumid;
			_vkApi.getUploadServer(inalbumid);
		}
		
		/**
		* Save post to wall with given id, image and message
		**/
		public function wallSavePost(wall_id:String, inbmpd:BitmapData, message:String, inimgid:String = null):void
		{
			_wallUploadID = wall_id;
			_wallUploadBmpd = inbmpd;
			_wallUploadMsg = message;
			
			if (inimgid) _vkApi.wallSavePost(_wallUploadID, inimgid, _wallUploadMsg, null);
			else _vkApi.getWallUploadServer();
		}
		
		//--------------------------------------
		//  CALLBACKS METHODS
		//--------------------------------------
		public function on_albumCreated(indata:Object):void 
		{
			_responsesCache[SocNetApiEvent.ON_ALBUM_CREATED] = indata.friends;
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_ALBUM_CREATED, indata));
		}
		
		public function on_app_friends(indata:Object):void 
		{
		//indata.friends = [134038087, 18211892, 1476011, 51032116, 101551240, 8041925, 44980867, 5057680, 132108241, 89412023, 134469784, 7819716, 23899277, 65158469, 33808237, 123581420, 8983309, 55632931, 49512300, 20422872, 25306732, 68847127, 73508536, 117509383, 137641452, 50917527, 19282838, 70864027, 2897671, 42501794, 12934245, 44030895, 94159627, 6172128, 66041820, 24327963, 18214204, 70150699, 124156677, 113935835, 3057909, 24093213, 124165916, 88100841, 134348807, 20830443, 53319453, 24717891, 11480094, 30213543, 96921582, 17692124, 136670966, 58504224, 13807207, 81249216, 40372661, 106934397, 27927728, 98985632, 26414453, 29784832, 28128141, 31633961, 121804061, 67723913, 57199594, 122456462, 670601, 22880346, 71499363, 89220824, 97288186, 10560885, 132791863, 49187604, 24768658, 5364207, 35594970, 113131806, 14897643, 21841364, 111879279, 3030512, 109867012, 47180264, 36464737, 31163613, 84638458, 50947985, 70002839, 80790505, 82543043, 38613825, 123568227, 41333088, 116399891, 95855717, 56808418, 52766302];
		if (indata.friends==undefined || indata.friends[0] == undefined) _responsesCache[SocNetApiEvent.ON_APP_FRIENDS] = [];
			else _responsesCache[SocNetApiEvent.ON_APP_FRIENDS] = indata.friends;
			Dbg.log("[SocNetAPIVk] on_app_friends: indata.friends = " + indata.friends );
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_APP_FRIENDS));
		}
		
		
		
		public function on_friends(indata:Object):void 
		{
			/*Dbg.log("[SocNetAPIVk] on_friends:");
			Dbg.inspect(indata.friends);*/
			/*var _arrUIDs:Array = new Array();
			var n:int = 0;
			while (_arrUIDs.length < indata.friends.length) _arrUIDs.push(indata.friends[n++].uid);*/
			
			_responsesCache[SocNetApiEvent.ON_FRIENDS] = indata.friends;
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_FRIENDS));
			
		}
		
		public function on_profiles(indata:Object):void 
		{
			/*Dbg.log("[SocNetAPIVk] on_users_profiles:");
			Dbg.inspect(indata);*/
			if (indata.profiles == undefined) return;
			
			var arrProfiles:Array = new Array();
			var n:int = 0;
			while (arrProfiles.length < indata.profiles.length) arrProfiles.push(indata.profiles[n++]);
			
			cacheProfiles(arrProfiles);
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_USERS_PROFILES, {id:indata.userdata.id}));
			
		}
		
		public function on_userAlbums(indata:Object):void 
		{	
			if (indata[0] == undefined) _responsesCache[SocNetApiEvent.ON_USER_ALBUMS] = [];
			else _responsesCache[SocNetApiEvent.ON_USER_ALBUMS] = indata;
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_USER_ALBUMS));
		}
		
		public function on_userbalance(indata:Object):void 
		{
			_responsesCache[SocNetApiEvent.ON_USER_BALANCE] = indata.balance;
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_USER_BALANCE));
		}
		
		public function on_wallUploadPhoto(e:Object):void 
		{
			e.currentTarget.removeEventListener(e.type, arguments.callee);

			var o:Object = JSON.decode(e.currentTarget.loader.data as String);
			_vkApi.wallSavePost(_wallUploadID, null, _wallUploadMsg, o);
		}
		
		public function on_wallUploadServer(indata:Object):void 
		{
			_wallUploadData = indata;
			
			var jpgEncoder:JPGEncoder = new JPGEncoder(100);
			var jpgStream:ByteArray = jpgEncoder.encode(_wallUploadBmpd);
			
			var mll:MultipartURLLoader = new MultipartURLLoader();
			mll.addEventListener(Event.COMPLETE, on_wallUploadPhoto);
			mll.addFile(jpgStream, "photo.png", "photo");
			mll.load(_wallUploadData.upload_url);
		}
		
		public function on_photosSaved(e:Object):void 
		{
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_PHOTO_UPLOADED));
		}
		
		public function on_photoUploadComplete(e:Object):void 
		{
			e.currentTarget.removeEventListener(e.type, arguments.callee);
			var o:Object = JSON.decode(e.currentTarget.loader.data as String);
			_vkApi.savePhotos(o.aid, o.server, o.photos_list, o.hash);
		}
		
		
		public function on_getUploadServer(indata:Object):void 
		{
			_photoUploadData = indata;
			
			var jpgEncoder:JPGEncoder = new JPGEncoder(90);
			var jpgStream:ByteArray = jpgEncoder.encode(_photoUploadBmpd);
			
			var mll:MultipartURLLoader = new MultipartURLLoader();
			mll.addEventListener(Event.COMPLETE, on_photoUploadComplete);
			mll.addFile(jpgStream, "photo.png", "photo");
			mll.load(_photoUploadData.upload_url);
		}
		
		public function on_wallSavePost(indata:Object):void 
		{
			if (_wrapper) _wrapper.external.callMethod("saveWallPost",indata.post_hash);
			else dispatchNotAvailable("on_wallSavePost");
		}
		
		
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		///Array with friends uids, who uses the application
		public function get dataAppFriends():Array 
		{
			return(_responsesCache[SocNetApiEvent.ON_APP_FRIENDS]);
		}
				
		///Array with user friends' uids
		public function get dataFriends():Array 
		{
			return(_responsesCache[SocNetApiEvent.ON_FRIENDS]);
		}
		
		///Array with friends user's albums data
		public function get dataUserAlbums():Array 
		{
			return(_responsesCache[SocNetApiEvent.ON_USER_ALBUMS]);
		}
		
		///Array with user friends' uids
		public function get dataUserBalance():uint 
		{
			return(_responsesCache[SocNetApiEvent.ON_USER_BALANCE]);
		}
		
		///Array from all recieved profiles
		public function get dataUserProfiles():Array 
		{
			return(_usersProfiles);
		}
		
		///User ID in Socnet
		public function get uid():String 
		{
			return(_uid);
		}
	}

}