package arco.socnet.api
{
	import flash.display.BitmapData;
	import flash.events.EventDispatcher;
	
	/**
	 * ISocNetAPI
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public interface ISocNetAPI
	{
		
		function createAlbum(title:String, privacy:uint = 0, comment_privacy:uint = 0, description:String = ""):void;
		function getAppFriends(inuseCache:Boolean = true):void;
		function getCachedProfiles(inuids:Array):Array;
		function getFriends(inuseCache:Boolean = true):void;
		function getUserAlbums(inuid:String = "", inaids:Array = null):void;
		function getUserBalance():void;
		function getUsersProfiles(uids:Array, fields:String = null):Number;
		function getUserSettings():void;	
		function saveToStream(immessage:String, inattachment:Object):void;
		function uploadPhoto(inalbumid:String, inphoto:BitmapData):void ;
		/**
		* @param wall_id	ID of wall to post where
		* @param inbmpd		Bitmapdata of image
		* @param message	Message to post
		* @param imgid		ID of uploaded already image, inbmpd ignored when using
		**/
		function wallSavePost(wall_id:String, inbmpd:BitmapData, message:String, imgid:String = null):void;
		function showInviteBox(intext:String = ""):void;
		function showPaymentBox( inammount:uint, inname:String = "", incode:String = "", inoptions:Object = null):void;		
		function showPermissionstBox(inpermessions:Array):void;		
		
		
		/*
		
		function getUploadServer(aid:String, save_big:uint):void;
		function getProfileUploadServer():void;
		function getPhotoUploadServer():void;
		function putVariable(key:uint, value:*, user_id:String, session:*):void;
		function savePhotos(aid:String, server:String, photos_list:String, hash:String):void;
		function saveProfilePhoto(o:Object):void;*/
				
		//Data getters
		function get dataAppFriends():Array;
		function get dataFriends():Array;
		function get dataUserAlbums():Array
		function get dataUserBalance():uint;
		function get dataUserProfiles():Array;
			
		function get uid():String;
		
		
	}
	
}