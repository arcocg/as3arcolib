package arco.socnet.api
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author arco
	 */
	public class SocNetApiEvent extends Event 
	{
		/** Definitions for all event types. **/
		public static const INITED:String = "onInited";
		public static const ERROR:String = "onError"; //data: error data returned by server
		public static const ERROR_PERMISSION_DENIED:String = "onErrorPermissionDenied"; //data: error data returned by server
		
		public static const ON_ALBUM_CREATED:String = "onAlbumCreated";
		public static const ON_APP_FRIENDS:String = "onAppFriends";
		public static const ON_FRIENDS:String = "onFriends"; 
		public static const ON_PHOTO_UPLOADED:String = "onPhotoUploaded"; 
		public static const ON_USER_ALBUMS:String = "onUserAlbums"; //User's balance
		public static const ON_USER_BALANCE:String = "onUserBalans"; //User's balance
		public static const ON_USERS_PROFILES:String = "onUsersProfiles";
		
		public static const NOT_AVAILABLE:String = "onNotAvailable"; //Method not available to execute
		/** The data associated with the event. **/
		private var _data:Object;
		
		/**
		* Constructor; sets the event type and inserts the new value.
		*
		* @param typ	The type of event.
		* @param dat	An object with all associated data.
		**/
		public function SocNetApiEvent(type:String, dat:Object=undefined, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			_data = dat;
		}
		
		/** Returns the data associated with the event. **/
		public function get data():Object {
			return _data;
		};
		
		public override function clone():Event 
		{ 
			return new SocNetApiEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("SocNetWrapperEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}