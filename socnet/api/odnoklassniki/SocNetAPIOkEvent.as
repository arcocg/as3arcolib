/**
* Definitions for all event types fired by the JamModel
*
**/
package arco.socnet.api.odnoklassniki {


import flash.events.Event;


public class SocNetAPIOkEvent extends Event {


	/** Definitions for all event types. **/
	public static const CONFIRM_OK:String = "onConfirmOK";
	
	/** The data associated with the event. **/
	private var _data:Object;


	/**
	* Constructor; sets the event type and inserts the new value.
	*
	* @param typ	The type of event.
	* @param dat	An object with all associated data.
	* 
	**/
	public function SocNetAPIOkEvent(typ:String,dat:Object=undefined,bbl:Boolean=false,ccb:Boolean=false):void {
		super(typ,bbl,ccb);
		_data = dat;
	};


	/** Returns the data associated with the event. **/
	public function get data():Object {
		return _data;
	};


}


}