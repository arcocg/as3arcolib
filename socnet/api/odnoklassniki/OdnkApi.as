﻿package arco.socnet.api.odnoklassniki
{
  import flash.events.Event;  
  import flash.events.TimerEvent;
  import flash.events.IOErrorEvent;
  import flash.events.SecurityErrorEvent;
  import flash.net.URLVariables;
  import flash.net.URLRequest;
  import flash.net.URLRequestMethod;
  import flash.net.URLLoader;
  import flash.utils.Timer;
  
  import com.api.forticom.SignUtil;
  
  import vk.BkRequest;
  import arco.utils.Dbg;
  import vk.MD5;
  import vk.MyJson;
  import com.serialization.json.JSON;
  
  //import utils.*;

  public class OdnkApi 
  {
    public var par:* = null; //Object for back-calling

    private var api_url:String = null;
    private var viewer_id:String = null;
    private var app_id:String = null;
    private var secret:String = null;
    
    private var timer:Timer = null;
    private var rqCommon:BkRequest = null;
    
    // -------------------------------------- Request types.
    // Common requests.
    private static const LOC_GET:uint = 501; 
	private static const SETT_GET:uint = 502;
	private static const PROFILE_GET:uint = 503;
	private static const FRIENDS_PROFILE_GET:uint = 504;
	private static const GET_FRIENDS:uint = 505;
	private static const GET_PROFILES:uint = 506;
	private static const GET_USER_BALANCE:uint = 507;
	private static const GET_APP_FRIENDS:uint = 508;
	
	private static const GET_USER_ALBUMS:uint = 509;
	private static const CREATE_ALBUM:uint = 510;
	private static const GET_UPLOAD_SERVER:uint = 511;
	private static const PHOTOS_SAVE:uint = 512;
	private static const GET_PROFILE_UPLOAD_SERVER:uint = 513;	
	private static const PROFILE_PHOTO_SAVE:uint = 514;
	private static const GET_PHOTO_UPLOAD_SERVER:uint = 515;
	private static const WALL_SAVE_POST:uint = 516;
	private static const GET_PHOTO_UPLOAD_SERVER_MY_CAR:uint = 518;
	private static const USERS_PROFILE_GET:uint = 517;
	private static const SAVE_STATUS:uint = 520;
	private static const LOGIN:uint = 521;
    
    // Game requests.


    //
    public function OdnkApi( par:*, api_url:String, viewer_id:String, app_id:String, secret:String):void
    {
	  if (api_url == null) api_url = "";
      this.par = par;	  
      this.api_url = api_url;
      this.viewer_id = viewer_id;
      this.app_id = app_id;
      this.secret = secret;
    }

    // ------------------------------------------------------------------------------ Common methods.
    public function getLocValues():void
    {
      var p_arr:Array = [];
      p_arr.push( {pn: "method", pv: "language.getValues"} );
      p_arr.push( {pn: "all", pv: 1} );

      rqCommon = new BkRequest( LOC_GET );
      sendRequestCommon( p_arr );
    }
    
	public function getUserSettings():void {		
		var p_arr:Array = [];
		p_arr.push( {pn: "method", pv: "getUserSettings"} );		

		rqCommon = new BkRequest(SETT_GET);
		sendRequestCommon(p_arr);	
	}
	public function getUserProfile(uid:*):void {		
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "getProfiles" } );
		p_arr.push( { pn: "uids", pv: uid} );
		p_arr.push( {pn: "fields", pv: "first_name,last_name,nickname,sex,bdate,city,country,timezone,photo,photo_medium,photo_big,has_mobile,rate,contacts,education"} );		
		rqCommon = new BkRequest(PROFILE_GET);
		sendRequestCommon(p_arr);
	}
	public function getUserProfiles(uid:*,fields:String =  "first_name,last_name,name,gender,pic_1", inuserdata:Object = null):void {		
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "users.getInfo" } );
		p_arr.push( { pn: "uids", pv: uid} );
		p_arr.push( {pn: "fields", pv: fields} );		
		rqCommon = new BkRequest(GET_PROFILES);
		rqCommon.userdata = inuserdata;
		sendRequestCommon(p_arr);
	}
	public function getFriends():void {		
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "friends.get" } );
		rqCommon = new BkRequest(GET_FRIENDS);
		sendRequestCommon(p_arr);
	}
	public function getAppFriends():void {		
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "friends.getAppUsers" } );
		rqCommon = new BkRequest(GET_APP_FRIENDS);
		sendRequestCommon(p_arr);
	}
	public function getFriendsProfile(uids:String):void {
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "users.getInfo" } );
		p_arr.push( { pn: "uids", pv: uids} );
		p_arr.push( { pn: "fields", pv: "first_name,last_name,name,gender,pic_1" } );
		rqCommon = new BkRequest(FRIENDS_PROFILE_GET);
		sendRequestCommon(p_arr);
	}
	public function getUsersProfiles(uids:String, fields:String = "first_name,last_name,name,gender,pic_1"):void {		
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "users.getInfo" } );
		p_arr.push( { pn: "uids", pv: uids} );
		p_arr.push( { pn: "fields", pv: fields} );
		rqCommon = new BkRequest(USERS_PROFILE_GET);
		sendRequestCommon(p_arr);
	}
	public function getUserBalance():void {
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "getUserBalance" } );
		rqCommon = new BkRequest(GET_USER_BALANCE);
		sendRequestCommon(p_arr);
	}
	public function getUserAlbums(uid:String = ""):void {
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "photos.getAlbums" } );
		//if (uid != "") p_arr.push( { pn: "uid", pv: uid } );
		//if (aids != null) p_arr.push( { pn: "aids", pv: aids.join(",") } );
		rqCommon = new BkRequest(GET_USER_ALBUMS);
		sendRequestCommon(p_arr);
	}
	public function createAlbum(title:String, intype:String = "public", comment_privacy:uint = 0, description:String = ""):void 
	{
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "photos.createAlbum" } );
		p_arr.push( { pn: "title", pv: title } );
		p_arr.push( { pn: "type", pv: intype } );
		//p_arr.push( { pn: "comment_privacy", pv: comment_privacy } );
		//p_arr.push( { pn: "description", pv: description } );
		rqCommon = new BkRequest(CREATE_ALBUM);
		sendRequestCommon(p_arr);
	}
	public function getUploadServer(aid:String):void 
	{
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "photos.getUploadUrl" } );
		p_arr.push( { pn: "aid", pv: aid } );
		//p_arr.push( { pn: "save_big", pv: save_big } );
		rqCommon = new BkRequest(GET_UPLOAD_SERVER);
		sendRequestCommon(p_arr);
	}
	public function getProfileUploadServer():void 
	{
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "photos.getProfileUploadServer" } );
		rqCommon = new BkRequest(GET_PROFILE_UPLOAD_SERVER);
		sendRequestCommon(p_arr);
	}
	public function getPhotoUploadServer():void 
	{
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "photos.getUploadUrl" } );
		rqCommon = new BkRequest(GET_PHOTO_UPLOAD_SERVER);
		sendRequestCommon(p_arr);
	}
	
	public function login():void {		
		var p_arr:Array = [];
		p_arr.push( {pn: "method", pv: "auth.login"} );		
		p_arr.push( {pn: "user_name", pv: "arcocg"} );		
		p_arr.push( {pn: "password", pv: "arcocg_pwd"} );		

		rqCommon = new BkRequest(LOGIN);
		sendRequestCommon(p_arr);	
	}
	
	public function savePhotos(aid:String, server:String, photos_list:String, hash:String):void 
	{
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "photos.save" } );
		p_arr.push( { pn: "aid", pv: aid } );
		p_arr.push( { pn: "server", pv: server } );
		p_arr.push( { pn: "photos_list", pv: photos_list } );
		p_arr.push( { pn: "hash", pv: hash } );
		rqCommon = new BkRequest(PHOTOS_SAVE);
		sendRequestCommon(p_arr);
	}
	public function saveProfilePhoto(o:Object):void 
	{
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "photos.saveProfilePhoto" } );
		p_arr.push( { pn: "server", pv: o.server } );
		p_arr.push( { pn: "photo", pv: o.photo } );
		p_arr.push( { pn: "hash", pv: o.hash } );
		rqCommon = new BkRequest(PROFILE_PHOTO_SAVE);
		sendRequestCommon(p_arr);
	}
	public function wallSavePost(wall_id:String, photo_id:String, message:String, usd:Object):void 
	{
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "wall.savePost" } );
		p_arr.push( { pn: "wall_id", pv: wall_id } );
		//p_arr.push( { pn: "post_id", pv: post_id } );
		if(photo_id != null) p_arr.push( { pn: "photo_id", pv: photo_id } );
		p_arr.push( { pn: "message", pv: message } );
		p_arr.push( { pn: "server", pv: usd.server } );
		p_arr.push( { pn: "photo", pv: usd.photo } );
		p_arr.push( { pn: "hash", pv: usd.hash } );
		rqCommon = new BkRequest(WALL_SAVE_POST);
		sendRequestCommon(p_arr);
	}
	
	public function saveToStream(message:String, resig:String, attachment:String = null):void 
	{
		var p_arr:Array = [];
		//p_arr.push( { pn: "uid", pv: uid } );
		p_arr.push( { pn: "method", pv: "stream.publish" } );
		p_arr.push( { pn: "message", pv: message } );
		p_arr.push( { pn: "resig", pv: resig } );
		if (attachment) p_arr.push( { pn: "attachment", pv: attachment } );
		
		/*if(photo_id != null) p_arr.push( { pn: "photo_id", pv: photo_id } );
		p_arr.push( { pn: "message", pv: message } );
		p_arr.push( { pn: "server", pv: usd.server } );
		p_arr.push( { pn: "photo", pv: usd.photo } );
		p_arr.push( { pn: "hash", pv: usd.hash } );*/
		rqCommon = new BkRequest(WALL_SAVE_POST);
		sendRequestCommon(p_arr);
	}
	
	public function saveStatus(text:String):void 
	{
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "activity.set" } );
		
		p_arr.push( { pn: "text", pv: text } );
		rqCommon = new BkRequest(SAVE_STATUS);
		sendRequestCommon(p_arr);
	}
	public function wallSavePostNoImage(wall_id:String, photo_id:String, message:String):void 
	{
		
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "wall.savePost" } );
		p_arr.push( { pn: "wall_id", pv: wall_id } );
		//p_arr.push( { pn: "post_id", pv: post_id } );
		if(photo_id != null) p_arr.push( { pn: "photo_id", pv: photo_id } );
		p_arr.push( { pn: "message", pv: message } );
		
		rqCommon = new BkRequest(WALL_SAVE_POST);
		sendRequestCommon(p_arr);
		
	}
	public function putVariable(key:uint, value:*, user_id:String = "", session:* = 0):void {
		var p_arr:Array = [];
		p_arr.push( { pn: "method", pv: "putVariable" } );
		p_arr.push( { pn: "key", pv: key } );
		p_arr.push( { pn: "value", pv: value } );
		if(user_id != "") p_arr.push( { pn: "user_id", pv: user_id } );
		p_arr.push( { pn: "session", pv: session } );
		rqCommon = new BkRequest(0);
		sendRequestCommon(p_arr);
	}
	
    // ------------------------------------------------------------------------------------ methods.
    private function srHelper( arr:Array, rq:BkRequest ):URLVariables
    {      
	
		var url_vars:URLVariables = new URLVariables();
		var request:Object = { };
		for( var i:uint = 0; i < arr.length; ++i ) {
			request[ (arr[i].pn) ] = arr[ i ].pv;
		}
		if (request["resig"] != undefined) {
			delete request["resig"];
		}
		
		url_vars["sig"] = SignUtil.signRequest(request)["sig"];
		
		arr.push( {pn: "application_key", pv: app_id } );
		arr.push( { pn: "format", pv: "JSON" } );
		if (SignUtil.sessionKey != null) arr.push( { pn: "session_key", pv: SignUtil.sessionKey } );
		
		
		for( i = 0; i < arr.length; ++i ) {
			url_vars[ (arr[i].pn) ] = arr[ i ].pv;
		}
		rq.url_vars = url_vars;
		return url_vars;
    }
    
    // --------------------------------------------------------------
    private function sendRequestCommon( arr:Array ):void
    {	  
      var url_vars:URLVariables = srHelper( arr, rqCommon );
      jsrHelper( url_vars, lchCommon );
    }
    
    private function jsrCommon( e:TimerEvent ):void
    {
      jsrHelper( rqCommon.url_vars, lchCommon );
    }
    
    // --------------------------------------------------------------
    private function makeSig( arr:Array ):String
    {
      var sig:String = "" + viewer_id;
      for( var i:uint = 0; i < arr.length; i++ ) sig += arr[i].pn + "=" + arr[i].pv;      
      sig += secret;      
      return MD5.encrypt( sig );
    }
    
    // ------------------------------------------------------------------------------- Just-Send-Request methods
    private function jsrHelper( url_vars:URLVariables, handler_:* ):void
    {
      var urlRequest:URLRequest = new URLRequest( api_url );
      urlRequest.method = URLRequestMethod.POST;
      urlRequest.data = url_vars;
      
      var ldr:URLLoader = new URLLoader();
      ldr.addEventListener( Event.COMPLETE, handler_, false, 0, true);
      ldr.addEventListener( IOErrorEvent.IO_ERROR, errHandler, false, 0, true );
      ldr.addEventListener( SecurityErrorEvent.SECURITY_ERROR, sequreErrHandler, false, 0, true );
      ldr.load( urlRequest );
    }
    
    // -------------------------------------------------------------------------------- LoaderComplete handlers
    // LoaderComplete handler
    private function lchCommon( e:Event ):void
    {
      var rq:BkRequest = lchHelper( e, rqCommon, jsrCommon );
	  Dbg.log("[OdnkApi] lchCommon: rq = " + rq );
      if ( rq == null ) return;
      
	  
		switch ( rq.type )
		{
			case LOC_GET:
				parseLoc( rq );
				break;
			case SETT_GET:
				parseUserSettings(rq);
				break;
			case PROFILE_GET:
				parseUserProfile(rq);
				break;
			case FRIENDS_PROFILE_GET:
				parseFriendsProfile(rq);
				break;
			case USERS_PROFILE_GET:
				parseUsersProfiles(rq);
				break;
			case GET_FRIENDS:
				parseGetFriends(rq);
				break;
			case GET_APP_FRIENDS:
				parseGetAppFriends(rq);
				break;
			case GET_PROFILES:
				parseProfiles( rq );
				break;
			case GET_USER_BALANCE:
				parseGetUserBalance( rq );
				break;
			case GET_USER_ALBUMS:
				parseGetUserAlbums( rq );
				break;
			case CREATE_ALBUM:
				parseCreateAlbum( rq );
				break;
			case GET_UPLOAD_SERVER:
				parseUploadserver( rq );
				break;
			case PHOTOS_SAVE:
				parsePhotosSave( rq );
				break;
			case GET_PROFILE_UPLOAD_SERVER:
				parseProfileUploadServer( rq );
				break;
			case PROFILE_PHOTO_SAVE:
				parseProfilePhotoSave( rq );
				break;
			case GET_PHOTO_UPLOAD_SERVER:
				parsePhotoUploadServer( rq );
				break;
			case WALL_SAVE_POST:
				parseWallSavePost( rq );
			break;
			case SAVE_STATUS:
				parseSaveStatus( rq );
			break;
		}	  
    }
    
    private function delayedCall( func:*, ms:int ):void
    {
      timer = new Timer( ms, 1 );
      timer.addEventListener( "timer", func, false, 0, true);
      timer.start();
    }

    private function lchHelper( e:Event, rq:BkRequest, handler_:* ):BkRequest
    {
      var resultLoader:URLLoader = URLLoader( e.target );
	 
      var s:String = resultLoader.data;
      trace(s);
      var err:int = checkError( s );
	  Dbg.log("err:"+err);
      if ( err != 0 )
      {
        if ( err == 6 )
        {
          delayedCall( handler_, 400 );
          return null;
        }
        
        if ( err == 7 )
        {
          try {
			  par.noPermission();
		  } catch (e:Error) {
			Dbg.log("noPermission");  
		  }
          return null;
        }

        //par.unkErr();
        return null;
      }
      
      rq.res = s;
      return rq;
    }

    // --------------------------------------------------------------------------- error handlers.
    private function errHandler( e:IOErrorEvent ):void
    {
      Dbg.log( "vkApi.errHandler( " + e.text + " )" );
      //par.unkErr();
	  trace("unkErr2");
    }
    
    private function sequreErrHandler( e:SecurityErrorEvent ):void
    {
      //Dbg.log( "vkApi.sequreErrHandler( " + e.text + " )" );
      //par.unkErr();
	  trace("unkErr3");
    }
    

    // -------------------------------------------------------------------------- check error methods.
    private function checkError( s:String ):int
    {
      var found:int = s.indexOf( "\"error\"" );
      if ( found != 1 )
        return 0;
      
      var i:int = 24;
      while ( i < s.length  &&  s.charAt(i) != "," )
        i++;

      var s1:String = s.substring( 23, i );
      return parseInt( s1 );
    }

    // -------------------------------------------------------------------------- Helper methods
    private function delQuotes( s_:String ):String
    {
      var s:String = s_;
      if ( s )
      {
        s = s.replace( /<br>/g, "\n" );
        s = s.replace( /&amp;/g, "\&" );
        s = s.replace( /&quot;/g, "\"" );
        s = s.replace( /&\#34;/g, "\"" );
        s = s.replace( /&\#39;/g, "\'" );
      }
      return s;
    }

    // ------------------------------------------------------------------- Common parsing methods.
    private function parseLoc( rq:BkRequest ):void
    {
      parseLocStr( rq.res );
    }
    
    public function parseLocStr( s:String ):void
    {
      var jsonObj:* = MyJson.decode( s );
      var res:Array = jsonObj.response;
      
      par.onLocValues( res[0] );
    }
    
    // ------------------------------------------------------------------- Game parsing methods.	
	private function parseLoginSettings(rq:BkRequest):void {
		var jsonObj:* = MyJson.decode( rq.res );
        par.on_login(jsonObj.data);
	}
	private function parseUserSettings(rq:BkRequest):void {
		var jsonObj:* = MyJson.decode( rq.res );
        var sett:int = int(jsonObj.response);
        par.onUserSettings(sett);
	}
	private function parseUserProfile(rq:BkRequest):void {		
		var jsonObj:* = MyJson.decode(rq.res);        
		par.onUserProfile( { profile:jsonObj.response[0] } );		       
	}	
	private function parseGetFriends(rq:BkRequest):void {		
		var jsonObj:* = JSON.deserialize(rq.res);	
		par.on_friends( { friends:jsonObj} );		
	}	
	private function parseGetAppFriends(rq:BkRequest):void {		
		var jsonObj:* = MyJson.decode(rq.res);
		
		par.on_app_friends( { friends:jsonObj.uids } );		
	}
	private function parseFriendsProfile(rq:BkRequest):void {		
		//var jsonObj:Object = MyJson.decode(rq.res);
		var jsonObj:Object = JSON.deserialize(rq.res);
		for each (var obj:Object in jsonObj) {
			obj["photo"] = obj["pic_1"];
			delete obj["pic_1"];
		}
		par.on_friends_profile( { profiles:jsonObj} );
	}
	private function parseUsersProfiles(rq:BkRequest):void {		
		//var jsonObj:* = MyJson.decode(rq.res);
		trace("parseUsersProfiles " + rq.res);
		var jsonObj:Object = JSON.deserialize(rq.res);
		for each (var obj:Object in jsonObj) {
			obj["photo"] = obj["pic_1"];
			delete obj["pic_1"];
		}
		par.on_users_profiles( { profiles:jsonObj} );		
	}
	private function parseProfiles(rq:BkRequest):void {		
		var jsonObj:* = JSON.deserialize(rq.res);        		
		par.on_profiles( { profiles:(jsonObj==null)?[]:jsonObj, userdata:rq.userdata} );
	}
	private function parseGetUserBalance(rq:BkRequest):void {		
		var jsonObj:* = MyJson.decode(rq.res);        		
		par.on_userbalance( { balance:jsonObj.response } );		
	}
	private function parseGetUserAlbums(rq:BkRequest):void {		
		var jsonObj:* = MyJson.decode(rq.res);        		
		par.on_userAlbums( jsonObj.albums );		
	}
	private function parseCreateAlbum(rq:BkRequest):void {		
		var jsonObj:* = MyJson.decode(rq.res);
   		//Pass response without quotes
		par.on_albumCreated( String(rq.res).substr(1, String(rq.res).length - 2) );
	}
	private function parseUploadserver(rq:BkRequest):void {	
		
		var jsonObj:*; 
		var _res:String = rq.res;
		if (_res.charAt(0) != "{" && _res.charAt(length - 1) != "}") jsonObj = {url: String(_res).substr(1, String(_res).length - 2)}; //Pass response without quotes
		else jsonObj = MyJson.decode(_res);
		par.on_getUploadServer( jsonObj );
	}
	private function parsePhotosSave(rq:BkRequest):void {		
		var jsonObj:* = MyJson.decode(rq.res);        		
		par.on_photosSaved( jsonObj.response );		
	}
	private function parseProfileUploadServer(rq:BkRequest):void {		
		var jsonObj:* = MyJson.decode(rq.res);        		
		par.on_getProfileUploadServer( jsonObj.response );		
	}
	private function parseProfilePhotoSave(rq:BkRequest):void {		
		var jsonObj:* = MyJson.decode(rq.res);        		
		par.on_profilePhotoSave( jsonObj.response );		
	}
	private function parsePhotoUploadServer(rq:BkRequest):void {
		if (rq.res == "") {
			//Use for upload same server
			par.on_photoUploadServer([{upload_url:api_url}]);
		}
		else {
			//Return getted server(s)
			var jsonObj:* = MyJson.decode(rq.res);		
			par.on_photoUploadServer( jsonObj);
		}
				
	}
	private function parseWallSavePost(rq:BkRequest):void {		
		trace("[OdnkAPI parseWallSavePost]");
		var jsonObj:* = MyJson.decode(rq.res);
		trace(rq.res);
		par.on_wallSavePost( jsonObj );
	}
    private function parseSaveStatus(rq:BkRequest):void {		
		//var jsonObj:* = MyJson.decode(rq.res);        		
		//par.on_wallSavePost( jsonObj.response );		
	}
  }
}