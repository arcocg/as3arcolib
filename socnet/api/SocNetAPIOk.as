package arco.socnet.api
{
	import arco.server.ServerRequest;
	import arco.server.ServerRequestEvent;
	import arco.socnet.api.ISocNetAPI;
	import arco.socnet.api.odnoklassniki.OdnkApi;
	import arco.socnet.api.odnoklassniki.SocNetAPIOkEvent;
	import arco.utils.CommonUtils;
	import com.adobe.images.JPGEncoder;
	import com.adobe.serialization.json.JSON;
	import com.api.forticom.ApiCallbackEvent;
	import com.api.forticom.ForticomAPI;
	import com.api.forticom.SignUtil;
	import flash.display.BitmapData;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import ru.inspirit.net.MultipartURLLoader;
	import ru.inspirit.net.events.MultipartURLLoaderEvent;
	
	
	import arco.utils.Dbg;
		
	/**
	 * SocNetAPIOk
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class SocNetAPIOk extends SocNetAPIBase implements ISocNetAPI
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		//Vars inited after wrapper init
		public var api_url:String;
		public var app_key:String;
		public var secret:String;
		public var referrer:String;
		public var refplace:String;
		public var resig:String;
		public var direct:String; //Direct link to application
		
		public var req_sig:String; //Signature of last request
		
		private var _stage:Stage;
		
		//Var for compatibility with VK-wrapper
		public var external:Object;
		
		//Login and password for testmode
		private var _testmode_login:String;
		private var _testmode_pass:String;
		
		private var _api_connection:String;
		private var api_request : Object
		
		private var _responsesCache:Dictionary = new Dictionary();
		private var _uid:String;
		private var _wrapper:Object;
		
		private var _saveToStreamSig:String;
		private var _saveToStreamMessage:String;
		private var _saveToStreamAttachment:Object;
	
		private var _odnKAPI:OdnkApi;
		
		
		private var _photoUploadAID:String; //Id of album where photo uploaded to
		private var _photoUploadBmpd:BitmapData; 
		private var _photoUploadData:Object = null; 
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		private var _debug:Boolean = false;
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------SocNetAPIOk
		public function SocNetAPIOk(instage:Stage, inappkey:String, insecret:String, inapiurl:String, inlogin:String = "", inpass:String = "", indebug:Boolean = false)
		{
			api_url = inapiurl;
			_stage = instage;
			app_key = inappkey;
			secret = insecret
			
			_testmode_login = inlogin;
			_testmode_pass = inpass;
			
			external = this;
			_debug = indebug;
			
			init();					
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//------apiIsReady
		///OdnK API is connected by LocalConnection. Site app running.
		private function apiIsReady(event : Event) : void
		{
			trace("[MainOdnk] ForticomAPI is ready");
			if (_debug) ForticomAPI.removeEventListener(ForticomAPI.ON_PROXY_NOT_RESPONDING, this.localLogin);
			initNormal();
		}
		
		/**
		* Checks was server return a error
		**/
		private function checkForError(inres:Object, inautohandle:Boolean = true):Boolean 
		{
			if (inres.error_code != undefined) {
				if (inautohandle)
					switch (int(inres.error_code)) 
					{
						case 10:
							dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ERROR_PERMISSION_DENIED, inres));						
						break;
						default:
							dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ERROR, inres));
						break;
					}
					
				return true;
			}
			
			return false;
		}
		
		//------handleCallback
		///Handles OdnkAPI-callback
		protected function handleCallback(event : ApiCallbackEvent) : void
		{
			//Alert.show(event.method + "\n" + event.result+ "\n" + event.data, "Callback");
			trace("[MainOdnk] ForticomAPI Callback" + event.method + "\n" + event.result + "\n" + event.data);
			switch (event.method) 
			{
				case "showConfirmation":
					if (event.result == "ok") {
						resig = event.data;
						dispatchEvent(new SocNetAPIOkEvent(SocNetAPIOkEvent.CONFIRM_OK));
					}
				break;
			}
			
		}
		
		//------onRequest_completed
		private function onRequest_completed(e:ServerRequestEvent):void 
		{		
			switch (e.currentTarget.type) 
			{
				case "auth.login":					
					if (e.data.data.uid != undefined) {
						initTestMode(e.data.data);
					}
				break;
			}
			
			e.currentTarget.removeEventListener(ServerRequestEvent.REQUEST_COMPLETE, onRequest_completed);
			e.currentTarget.removeEventListener(ServerRequestEvent.ERROR, onRequest_error);
		}
		
		//------onRequest_error
		private function onRequest_error(e:ServerRequestEvent):void 
		{	
			e.currentTarget.removeEventListener(ServerRequestEvent.REQUEST_COMPLETE, onRequest_completed);
			e.currentTarget.removeEventListener(ServerRequestEvent.ERROR, onRequest_error);
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		//------init
		///Entry point
		private function init():void 
		{    

			initDefaultVars();
			if (_stage && _stage.loaderInfo.parameters["logged_user_id"]!=undefined) _uid = _stage.loaderInfo.parameters["logged_user_id"];
			
			//Make API connection to show OdnK dialog boxes
			try{
				ForticomAPI.addEventListener(ForticomAPI.CONNECTED, this.apiIsReady);
				ForticomAPI.connection = _stage.loaderInfo.parameters["apiconnection"];
				ForticomAPI.addEventListener(ApiCallbackEvent.CALL_BACK, this.handleCallback);
				if (_debug) ForticomAPI.addEventListener(ForticomAPI.ON_PROXY_NOT_RESPONDING, this.localLogin);
			} 
			//Connection couldn't complete - local execution.
			catch (e:Error) {
				if (_debug) localLogin(); //Login to gain access to Odnk REST API
			}
		}
		
		//------initDefaultVars
		///Inits Vars constant for application
		protected function initDefaultVars():void 
		{
			SignUtil.applicationKey = app_key;
			SignUtil.secretSessionKey = SignUtil.secretKey = secret;
		}
		
		//------initVars
		//Init global vars from gotten flash-vars
		private function initNormal():void 
		{			
			api_url = _stage.loaderInfo.parameters["api_server"] ? _stage.loaderInfo.parameters["api_server"] + 'fb.do' : '/fb.do?';
			direct = _stage.loaderInfo.parameters["direct"];	
			
			SignUtil.applicationKey = app_key = _stage.loaderInfo.parameters["application_key"];
			SignUtil.sessionKey = _stage.loaderInfo.parameters["session_key"];
			SignUtil.secretSessionKey = _stage.loaderInfo.parameters["session_secret_key"];
			SignUtil.secretKey = secret;

			this.referrer = _stage.loaderInfo.parameters["referer"];
			this.refplace = _stage.loaderInfo.parameters["refplace"];
			
			_odnKAPI = new OdnkApi( this, api_url, _uid, app_key, secret);
			Dbg.log("[SocNetAPIOk] initNormal: WRAPPER_INITED");
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.INITED));
			//dispatchEvent(new OdnoklassnikiWrapperEvent(OdnoklassnikiWrapperEvent.WRAPPER_INITED));
		}
		
		//------initLocalExecVars
		///Inits var from data gotten after test-mode local Login.
		protected function initTestMode(d:Object) : void
		{	
			_uid = String(d.uid)
			api_url = (d.api_server!=undefined)? d.api_server + 'fb.do' : '/fb.do?';
			SignUtil.secretSessionKey = d.session_secret_key ;
			SignUtil.sessionKey = d.session_key;
			
			_odnKAPI = new OdnkApi( this, api_url, _uid, app_key, secret);
			Dbg.log("[SocNetAPIOk] initTestMode: WRAPPER_INITED_TESTMODE");
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.INITED));
			//dispatchEvent(new OdnoklassnikiWrapperEvent(OdnoklassnikiWrapperEvent.WRAPPER_INITED_TESTMODE));
		}
		
		
		
		
		/* INTERFACE arco.socnet.api.ISocNetAPI */
		public function createAlbum(title:String, privacy:uint = 0, comment_privacy:uint = 0, description:String = ""):void 
		{
			_odnKAPI.createAlbum(title, (privacy == 0)?"public":"friends", comment_privacy, description);
		}
		
		public function getAppFriends(inuseCache:Boolean = true):void
		{
			if (inuseCache && _responsesCache[SocNetApiEvent.ON_APP_FRIENDS]!=undefined) dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_APP_FRIENDS));
			else _odnKAPI.getAppFriends();
		}
	
		public function getFriends(inuseCache:Boolean = true):void
		{
			if (inuseCache && _responsesCache[SocNetApiEvent.ON_FRIENDS]!=undefined) dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_FRIENDS));
			else _odnKAPI.getFriends();
		}
		
		public function getUserAlbums(inuid:String = "", inaids:Array = null):void
		{
			_odnKAPI.getUserAlbums((inuid == "")?_uid:inuid);
			
		}
		
		public function getUserBalance():void
		{
			//_vkApi.getUserBalance();
		}
		
		/**
		 * Requests Vk for user profiles
		 * 
		 * @param uids		UIDs of requested profiles 
		 * @param fields	Fields needed in profile
		 * 
		**/
		public function getUsersProfiles(uids:Array, fields:String = null):Number
		{
			
			var reqid:Number = CommonUtils.getUniqueID();
			
			var arrProfilesToRequest:Array = getProfilesNeededToRequest(uids);
			if 	(!fields) fields = "first_name,last_name,nickname,sex,photo";
			 
			//Needed for VK-compatibility
			fields = fields.replace("nickname", "name");
			fields = fields.replace("sex", "gender");
			fields = fields.replace("photo", "pic_1");
			fields += ",uid";
			
			Dbg.log("[SocNetAPIOk] getUsersProfiles: fields = " + fields );
			Dbg.log("[SocNetAPIOk] getUsersProfiles: arrProfilesToRequest.join = " + arrProfilesToRequest.join(",") );
			//No need to requests profiles, all profiles in cache
			if (arrProfilesToRequest.length == 0) dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_USERS_PROFILES, { id: reqid }));
			else _odnKAPI.getUserProfiles(arrProfilesToRequest.join(","), fields, { id:reqid });
			
			return reqid;
		}
		
		public function getUserSettings():void
		{
			
		}
		
		public function  saveToStream(immessage:String, inattachment:Object):void 
		{
			var req:Object = {method:"stream.publish", message: immessage, attachment:JSON.encode(inattachment)}
			req = SignUtil.signRequest(req);
			_saveToStreamSig = req["sig"];
			_saveToStreamMessage = immessage;
			_saveToStreamAttachment = inattachment;
			ForticomAPI.showConfirmation("stream.publish", immessage, req["sig"]);
			addEventListener(SocNetAPIOkEvent.CONFIRM_OK, ﻿﻿onCofirmStreamSave);
		}
		
		private function ﻿﻿onCofirmStreamSave(e:SocNetAPIOkEvent):void 
		{
			removeEventListener(SocNetAPIOkEvent.CONFIRM_OK, ﻿﻿onCofirmStreamSave);
			
			_odnKAPI.saveToStream(_saveToStreamMessage, resig, JSON.encode(_saveToStreamAttachment));
		}
		
		/**
		* Shows box to invite friends
		**/
		public function showInviteBox(intext:String = ""):void {
			ForticomAPI.showInvite(intext);
		}
		
		/**
		* Shows box with payments
		**/
		public function showPaymentBox(inammount:uint, inname:String = "", incode:String = "", inoptions:Object = null):void {
			if (!inoptions) inoptions = { name:inname, price:inammount, code:incode };
			ForticomAPI.showPayment(inname, "Покупка игровой валюты", incode, inammount, JSON.encode(inoptions));

		}
		
		public function showPermissionstBox(inpermessions:Array):void 
		{
			ForticomAPI.showPermissions(inpermessions);
		}
		
		/**
		* Uploads photo in specified album
		* 
		* @param inalbumid	ID of album
		* @param inphoto	BitmapData of uploaded photo 
		**/
		public function uploadPhoto(inalbumid:String, inphoto:BitmapData):void {
			_photoUploadBmpd = inphoto;
			_photoUploadAID = inalbumid;
			_odnKAPI.getUploadServer(inalbumid);			
		}
		
		/**
		* Save post to wall with given id, image and message
		**/
		public function wallSavePost(wall_id:String, inbmpd:BitmapData, message:String, inimgid:String = null):void
		{
			dispatchNotAvailable();
		}
		
		//--------------------------------------
		//  CALLBACKS METHODS
		//--------------------------------------
		public function on_albumCreated(indata:Object):void 
		{
			_responsesCache[SocNetApiEvent.ON_ALBUM_CREATED] = {aid:indata};
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_ALBUM_CREATED, _responsesCache[SocNetApiEvent.ON_ALBUM_CREATED]));
		}
		
		public function on_app_friends(indata:Object):void 
		{
		if (indata.friends==undefined || indata.friends[0] == undefined) _responsesCache[SocNetApiEvent.ON_APP_FRIENDS] = [];
			else {
				//Needed for VK-compatibility v2.0
				//var arrAppFriend:Array = new Array();
				//while (arrAppFriend.length < indata.friends.length) { arrAppFriend.push( { uid: indata.friends[arrAppFriend.length] } ) };				
				_responsesCache[SocNetApiEvent.ON_APP_FRIENDS] = indata.friends;
			}
			Dbg.log("[SocNetAPIVk] on_app_friends: indata.friends = " + indata.friends );
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_APP_FRIENDS));
		}
		
		public function on_getUploadServer(inres:Object):void 
		{
			if (checkForError(inres)) return;
			
			//_photoUploadData = inres.url;
			
			var jpgEncoder:JPGEncoder = new JPGEncoder(90);
			var jpgStream:ByteArray = jpgEncoder.encode(_photoUploadBmpd);
			
			var mll:MultipartURLLoader = new MultipartURLLoader();
			mll.addEventListener(Event.COMPLETE, on_photoUploadComplete);
			mll.addFile(jpgStream, "FileName.jpg", "file_1");
			
			var item : Object = {};
			item["application_key"] = app_key;
			if (SignUtil.sessionKey != null) item["session_key"] = SignUtil.sessionKey;  
			item["aid"] = _photoUploadAID;
			item["method"] = "photos.upload";
			item["photos"] = "[" + JSON.encode({}) + "]";
			item["Filename"] = "FileName.jpg";
			item["Upload"] = "Submit Query";
			item["sig"] = SignUtil.signRequest(item, false, null ).sig;
			 
			delete item["Upload"];
			delete item["Filename"]
			 
			var path:String = String(inres.url + "fb.do?method=photos.upload&application_key=" + app_key +
			   "&sig=" + item["sig"] + String((SignUtil.sessionKey != null)?String("&session_key=" + SignUtil.sessionKey):"") + 
			   "&photos=" + item["photos"] + "&aid=" + _photoUploadAID);
			Dbg.log("[SocNetAPIOk] on_getUploadServer: inurl = " + path );
			mll.load(path);
			
		}
		
		public function on_friends(indata:Object):void 
		{
			/*Dbg.log("[SocNetAPIVk] on_friends:");
			Dbg.inspect(indata.friends);*/
			var _arrUIDs:Array = new Array();
			var n:int = 0;
			while (_arrUIDs.length < indata.friends.length) _arrUIDs.push(indata.friends[n++]);
			
			_responsesCache[SocNetApiEvent.ON_FRIENDS] = _arrUIDs;
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_FRIENDS));
			
		}
		
		public function on_photoUploadComplete(e:Object):void 
		{
			e.currentTarget.removeEventListener(e.type, arguments.callee);
			//data = "{"server": "10705", "photos_list": "", "aid": "132795217", "hash": "d30e47885e1e1751d7f6102efabdc171"}";
			
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_PHOTO_UPLOADED));
			/*var o:Object = JSON.decode(e.currentTarget.loader.data as String);
			_vkApi.savePhotos(o.aid, o.server, o.photos_list, o.hash);*/
			
			
		}
		
		public function on_profiles(indata:Object):void 
		{
			
			if (indata.profiles == undefined) return;
			
			var arrProfiles:Array = new Array();
			
			while (arrProfiles.length < indata.profiles.length) arrProfiles.push(indata.profiles[arrProfiles.length]);
			
			//Needed for VK-compatibility
			for each (var obj:Object in arrProfiles) {
				obj["photo"] = obj["pic_1"];
				delete obj["pic_1"];
				
				obj["nickname"] = obj["name"];
				delete obj["name"];
			}
			
			Dbg.log("[SocNetAPIOk] on_profiles: arrProfiles = " + arrProfiles );
			Dbg.inspect(indata.profiles);
			
			cacheProfiles(arrProfiles);
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_USERS_PROFILES, {id:indata.userdata.id}));
			
		}
		
		public function on_userAlbums(indata:Object):void 
		{	
			if (indata[0] == undefined) _responsesCache[SocNetApiEvent.ON_USER_ALBUMS] = [];
			else _responsesCache[SocNetApiEvent.ON_USER_ALBUMS] = indata;
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_USER_ALBUMS));
		}
		
		public function on_userbalance(indata:Object):void 
		{
			_responsesCache[SocNetApiEvent.ON_USER_BALANCE] = indata.balance.balance;
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.ON_USER_BALANCE));
		}
		
		/*public function on_wallUploadPhoto(e:Object):void 
		{
			e.currentTarget.removeEventListener(e.type, arguments.callee);

			var o:Object = JSON.decode(e.currentTarget.loader.data as String);
			//_vkApi.wallSavePost(_wallUploadID, null, _wallUploadMsg, o);
		}
		
		public function on_wallUploadServer(indata:Object):void 
		{
			_wallUploadData = indata;
			
			var jpgEncoder:JPGEncoder = new JPGEncoder(100);
			var jpgStream:ByteArray = jpgEncoder.encode(_wallUploadBmpd);
			
			var mll:MultipartURLLoader = new MultipartURLLoader();
			mll.addEventListener(Event.COMPLETE, on_wallUploadPhoto);
			mll.addFile(jpgStream, "photo.png", "photo");
			mll.load(_wallUploadData.upload_url);
		}*/
		
		public function on_wallSavePost(indata:Object):void 
		{
			
		}
		
		
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//------localLogin
		///Login to API. Uses when application runs localy.
		private function localLogin(e:Event = null):void 
		{
			/*if (!_debug) {
				//Try to make new connection with API Proxy in Production Mode
				ForticomAPI.connection = this.stage.loaderInfo.parameters["apiconnection"];
				return;
			}*/
			doRequest("auth.login");
			ForticomAPI.removeEventListener(ForticomAPI.ON_PROXY_NOT_RESPONDING, this.localLogin);
		}
		
		//------checkURL
		protected function checkURL(value : String) : String
		{
			if (value.search(/\?/) > 0)
			{
				if (value.search(/&$/) < 0)
				{
					value += "&";
				}
			}
			else
			{
				value += "?";
			}
			
			return value;
		}
		
		//------doRequest
		//Requestes Odnoklassniki REST API
		private function doRequest(inreqtype:String):void 
		{
			var _req:ServerRequest = new ServerRequest();
			var request:Object = {method:inreqtype};
			_req.addEventListener(ServerRequestEvent.REQUEST_COMPLETE, onRequest_completed);
			_req.addEventListener(ServerRequestEvent.ERROR, onRequest_error);
			var addVars:String = "";
			switch (inreqtype) 
			{
				case "auth.login":
					request["user_name"] = _testmode_login;
					request["password"] = _testmode_pass;							
					addVars = "&user_name=" + _testmode_login + "&password=" + _testmode_pass; //&gen_token=true
				break;
				case "users.isAppUser":
				break;
			}
			
			request = SignUtil.signRequest(request);
			_req.type = inreqtype;
			_req.doRequest(api_url, "method=" + inreqtype + "&application_key=" + SignUtil.applicationKey + 
							"&sig=" + request["sig"]+ 
							String((SignUtil.sessionKey != null)?String("&session_key=" + SignUtil.sessionKey):"") + 
							"&format=JSON" + addVars);
		}
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		///Array with friends uids, who uses the application
		public function get dataAppFriends():Array 
		{
			return(_responsesCache[SocNetApiEvent.ON_APP_FRIENDS]);
		}
		
		///Array with user friends' uids
		public function get dataFriends():Array 
		{
			return(_responsesCache[SocNetApiEvent.ON_FRIENDS]);
		}
		
		///Array with friends user's albums data
		public function get dataUserAlbums():Array 
		{
			return(_responsesCache[SocNetApiEvent.ON_USER_ALBUMS]);
		}
		
		///Array with user friends' uids
		public function get dataUserBalance():uint 
		{
			return(_responsesCache[SocNetApiEvent.ON_USER_BALANCE]);
		}
		
		///Array from all recieved profiles
		public function get dataUserProfiles():Array 
		{
			return(_usersProfiles);
		}
		
		///User ID in Socnet
		public function get uid():String 
		{
			return(_uid);
		}
	}

}