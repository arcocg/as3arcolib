package arco.socnet.api 
{
	import arco.utils.Dbg;
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
		
	/**
	 * SocNetAPIBase
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class SocNetAPIBase extends EventDispatcher
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		//Profiles cache
		protected var _usersProfiles:Array = new Array();
		protected var _usersProfilesIndexes:Dictionary = new Dictionary();
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------SocNetAPIBase
		public function SocNetAPIBase() 
		{
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		/**
		 * Returns profile data from cache
		 * 
		 * @param inuid	UID of profile
		**/
		public function getCachedProfile(inuid:String):Object 
		{
			return(_usersProfiles[_usersProfilesIndexes[inuid]]);
		}
		
		/**
		 * Returns multiple profiles from cache
		 * 
		 * @param inuid	UID of profile
		**/
		public function getCachedProfiles(inuids:Array):Array 
		{
			var _profiles:Array = new Array();
			
			var n:int = 0;
			while (_profiles.length < inuids.length) _profiles.push(_usersProfiles[_usersProfilesIndexes[inuids[n++]]]);

			return(_profiles);
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		/**
		 * Cache data of profiles
		 * 
		 * @param inprofiles Profiles array needed to cache
		**/
		protected function cacheProfiles(inprofiles:Array):void 
		{
			for each (var profile:Object in inprofiles) 
				if (profile && _usersProfilesIndexes[profile.uid] == null) _usersProfilesIndexes[profile.uid] = _usersProfiles.push(profile) - 1;
				//else _usersProfiles[_usersProfilesIndexes[profile.uid]] = profile;
		}
		/**
		 * Returns array from users' profiles which didn't be cached and need to be requested from server
		**/
		protected function getProfilesNeededToRequest(inuiddata:Array):Array 
		{
			var _arrNeededToRequest:Array = new Array();
			
			for (var i:int = 0; i < inuiddata.length; i++) 
				if (_usersProfilesIndexes[inuiddata[i]] == null) _arrNeededToRequest.push(inuiddata[i]);
			
			return(_arrNeededToRequest);
			
		}
		
		protected function dispatchNotAvailable(inmethodname:String=""):void 
		{			
			Dbg.log("[SocNetAPIBase] SocNetAPI method " + inmethodname + " is not available now!");
			dispatchEvent(new SocNetApiEvent(SocNetApiEvent.NOT_AVAILABLE));
		}
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}