package arco.socnet 
{
		
	/**
	 * SocNetType
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class SocNetType
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		public static const SOCNET_TYPE_VK:String = "VK";
		public static const SOCNET_TYPE_OK:String = "OK";
		public static const SOCNET_TYPE_MM:String = "MM";
		public static const SOCNET_TYPE_NONE:String = "NONE";
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function SocNetType() 
		{
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		//------init
		public function init():void{
			
		}
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}