package arco.socnet 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author arco
	 */
	public class SocnetWrapperEvent extends Event 
	{
		/** Definitions for all event types. **/
		public static const INITED:String = "onInited"; //data: socnet - socnet type
		/** The data associated with the event. **/
		private var _data:Object;
		
		/**
		* Constructor; sets the event type and inserts the new value.
		*
		* @param typ	The type of event.
		* @param dat	An object with all associated data.
		**/
		public function SocnetWrapperEvent(type:String, dat:Object=undefined, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			_data = dat;
		}
		
		/** Returns the data associated with the event. **/
		public function get data():Object {
			return _data;
		};
		
		public override function clone():Event 
		{ 
			return new SocnetWrapperEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("SocnetWrapperEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}