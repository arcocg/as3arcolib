package arco.starling.ui
{
	//import arco.sounds.SoundsLibrary;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	
	/**
	 * ButtonBase
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 11.0;
	 * 
	 * Base class for Starling Button
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class ButtonBase extends Sprite
	{
		/*CONFIG::SOUNDS { 
			public var soundID:String = SoundsLibrary.SOUND_HOVER; 
			//Last time when sound has been played
			private var _soundLastTime:Number = 0; 
		}*/
		protected var _isOver:Boolean = false;
		protected var _isInited:Boolean = false;
		
		public function ButtonBase() 
		{					
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		protected function onAdded(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemove);
			this.addEventListener(TouchEvent.TOUCH, onTouch);
			
			if (!_isInited) init();
		}
		
		protected function onRemove(e:Event):void 
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemove);
			this.removeEventListener(TouchEvent.TOUCH, onTouch);
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		protected function onTouch(e:TouchEvent):void 
		{
			var touch:Touch = e.getTouch(stage);
			if (!touch) return;
			switch (touch.phase)
			{
				case TouchPhase.BEGAN:
					_isOver = true;
					over();
				break;
				case TouchPhase.ENDED:
					_isOver = false;
					out();
				break;
			}
		}
		
		
		
		/*public function froze():void 
		{
			buttonMode = false;
			removeEventListener(MouseEvent.ROLL_OVER, onMouse_over);
			removeEventListener(MouseEvent.ROLL_OUT, onMouse_out);
			removeEventListener(MouseEvent.MOUSE_UP, onMouse_mouseUp);
		}
		
		public function defroze():void 
		{
			buttonMode = true;
			addEventListener(MouseEvent.ROLL_OVER, onMouse_over);
			addEventListener(MouseEvent.ROLL_OUT, onMouse_out);
			addEventListener(MouseEvent.MOUSE_UP, onMouse_mouseUp);
		}*/
	
		
		
		protected function init():void
		{
			_isInited = true;
		}
		
		/*protected function click():void
		{
			CONFIG::SOUNDS {
				SoundsLibrary.play(SoundsLibrary.SOUND_CLICK);
			}
		}*/
		
		protected function over():void
		{
			
		}
		
		protected function out():void
		{
			
		}
		/*
		public function get isFrozen():Boolean
		{
			return(!buttonMode);
		}*/
		
	}

}