package arco.starling.ui 
{
	import starling.display.Image;
	import starling.display.Sprite;
	
		
	/**
	 * ButtonInvert
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class ButtonInvert extends ButtonBase
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		public var bg:Image;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------ButtonInvert
		public function ButtonInvert() 
		{
			super();
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		override protected function over():void 
		{
			super.over();
			
			bg.scaleY = -1;
		}
		
		override protected function out():void 
		{
			super.out();
			
			bg.scaleY = 1;
		}
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}