/**
* Definitions for all event types fired by the Model.
*
**/
package arco.server{


import flash.events.Event;


public class ServerRequestEvent extends Event {


	/** Definitions for all event types. **/
	//Login state events
	public static const ERROR:String = "onError";
	public static const REQUEST_COMPLETE:String = "onRequestComplete";
	
	/** The data associated with the event. **/
	private var _data:Object;


	/**
	* Constructor; sets the event type and inserts the new value.
	*
	* @param typ	The type of event.
	* @param dat	An object with all associated data.
	**/
	public function ServerRequestEvent(typ:String,dat:Object=undefined,bbl:Boolean=false,ccb:Boolean=false):void {
		super(typ,bbl,ccb);
		_data = dat;
	};


	/** Returns the data associated with the event. **/
	public function get data():Object {
		return _data;
	};


}


}