﻿package arco.server
{
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLVariables;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.errors.IOError;
	import flash.events.SecurityErrorEvent;
	import flash.events.IOErrorEvent;
	import flash.events.Event;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequestHeader;
	
	/**
	 * ServerRequest
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion AIR 1.5;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	
	public class ServerRequest extends EventDispatcher
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		private var _variables:URLVariables;
		private var _serverpath:String;
		private var _loader:URLLoader;
		private var _lastError:String;
		private var _requestedData:Object;
		
		public var type:String;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function ServerRequest(inpath:String = null, invars:String=null) {
			_serverpath = inpath;
			_variables = new URLVariables(invars);
			_loader = new URLLoader();
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		private function completeHandler(e:Event):void 
		{
			//trace("[ServerRequest] completeHandler " + e.target.data);
			_requestedData =  JSON.parse(e.target.data);
			//dispatchEvent(e);
			dispatchEvent(new ServerRequestEvent(ServerRequestEvent.REQUEST_COMPLETE, { data:_requestedData} ));
		}
		
		private function ioerror(e:IOErrorEvent):void 
		{
			trace("[ServerRequest] ioerror: e = " + e );
			//dispatchEvent(e);
			dispatchEvent(new ServerRequestEvent(ServerRequestEvent.ERROR, { errortype:e.type, error:e.text } ));
			//dispatchEvent(new Event(Event.COMPLETE));
		}
		
		private function secerror(e:SecurityErrorEvent):void 
		{
			trace("[ServerRequest] secerror: e = " + e );
			//dispatchEvent(e);
			dispatchEvent(new ServerRequestEvent(ServerRequestEvent.ERROR,{errortype:e.type, error:e.text}))
			//dispatchEvent(new Event(Event.COMPLETE));
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		//------init
		public function init():void{
			
		}
		
		public function doRequest(inpath:String = null, invars:String = null):void {
			if (inpath!=null) _serverpath = inpath;
			if (invars!=null) _variables = new URLVariables(invars);
			
			
			if ((_serverpath == null) || (_variables == null)) {
				dispatchEvent(new ServerRequestEvent(ServerRequestEvent.ERROR,{error:"Additional data required. Request is not possible."}))
			}
			
			_loader.dataFormat = URLLoaderDataFormat.TEXT;
			_loader.addEventListener(Event.COMPLETE, completeHandler);
			_loader.addEventListener(IOErrorEvent.IO_ERROR, ioerror);
			_loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, secerror);

			var request:URLRequest = new URLRequest(_serverpath);
			//trace(request.url);
			
			//request.requestHeaders.push( new URLRequestHeader( 'Cache-Control', 'no-cache' ) );
			//request.contentType = "application/x-www-formurlencoded";
			//request.requestHeaders.push( new URLRequestHeader("Content-Type", "text/xml; charset=utf-8"));
			request.method = URLRequestMethod.POST;
			request.data = _variables;
			try {
				_loader.load(request);
			}
			catch (error:IOError) {
				dispatchEvent(new ServerRequestEvent(ServerRequestEvent.ERROR,{error:error.message}))
			}
			
			
		}
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		public function get data():Object {
			return(_requestedData);
		}
	}
	
}