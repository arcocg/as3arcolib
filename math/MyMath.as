﻿package arco.math 
{
		
	/**
	 * MyMath
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class MyMath
	{
		public static const RAD2DEG_KOEF:Number = 57.29578; //value multiply rads by to get degs or mult degs to get rads (180/PI) 
		
		public static function normalizeAngle(angle:Number):Number
        {
            // move into range [-180 deg, +180 deg]
            while (angle < -Math.PI) angle += Math.PI * 2.0;
            while (angle >  Math.PI) angle -= Math.PI * 2.0;
            return angle;
        }
	}

}