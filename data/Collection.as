package arco.data 
{
	import flash.utils.Dictionary;
		
	/**
	 * Collection
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class Collection
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		private var _keyfield:String;
		private var _list:Array = new Array();
		private var _listIndexes:Dictionary = new Dictionary();
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		/**
		* @param	inkeyfield	Field name will be used as key to index values
		*	 		For example collection consists from such objects {uid:xxxx, name:yyyy, money:zzz}
		* 			as inkeyfield could be used "uid", "name" or "money"
		* 					
		**/		
		public function Collection(inkeyfield:String) 
		{
			_keyfield = inkeyfield;
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		/**
		* Add one element to collection 
		* 
		* @param invalue	Value to add
		* @return "true" if added of "false" if element with key already exists
		**/
		public function add(invalue:*):Boolean 
		{
			if (_listIndexes[invalue[_keyfield]]!=undefined) return false;
			
			try {
				_list.push(invalue);
				createIndex(_list.length - 1);
			} catch (e:Error) {
				return false;
			}
			
			return true;
		}
		
		public function getIndex(inkey:*):* 
		{
			return _listIndexes[inkey];
		}
		
		public function getValue(inkey:*):* 
		{
			try {
				return _list[_listIndexes[inkey]];
			} catch (e:Error) {
				return null;
			}
			
		}
		
		/**
		* Removes all elements in collection
		**/
		public function clear():void 
		{
			while (_list.length > 0) removeIndex(_list.pop()[_keyfield]);
		}
		
		
		/**
		* Creates collection from array
		* 
		* @param inarr	Array with data
		**/
		public function create(inarr:Array):void 
		{
			clear();
			for (var i:int = 0; i < inarr.length; i++) 
				add(inarr[i]);
		}
		
		/**
		* Removes one element from collection
		* 
		* @param inkey	
		**/
		public function remove(inkey:*):*
		{
			if (_listIndexes[inkey] == undefined) return null;
			
			var index:int = _listIndexes[inkey];
			var _item:* = _list.splice(index, 1)[0];
			removeIndex(inkey);
			reindex(index);
			return _item;
		}
		
		/**
		* Reindexes collection in given range with needed key value, wich part of single data item
		* 
		* @param inrangefirst
		**/
		public function reindex(inrangefirst:int = 0, inrangelast:int = -1):void 
		{
			for (var i:int = inrangefirst; i < ((inrangelast == -1)?_list.length:inrangelast); i++) 
				_listIndexes[_list[i][_keyfield]] = i;
		}
		
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		private function createIndex(inindex:int):void 
		{
			_listIndexes[_list[inindex][_keyfield]] = inindex;
		}
		
		private function removeIndex(inkey:*):void 
		{
			delete _listIndexes[inkey];
		}
		
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		public function get length():uint { return _list.length; }
		public function get list():Array {	return _list; }
	}

}