﻿package arco.box2d
{
	import arco.box2d.Box2DHelper;
	import arco.math.MyMath;
	import flash.events.Event;
	
	import Box2D.Dynamics.b2Body;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	
	
	/**
	 * PhysSprite
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * Base class for renderable physic objects
	 * 
	 * @author arcomailbox@gmail.com
	 */
	
	public class PhysSprite extends Sprite
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		protected var _physBody:b2Body;
		protected var _ws:Number;
		protected var _radAngle:Number;
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		protected var _fAllowRotate:Boolean = true;
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		/// params: x, y, density, friction, restitution, lin_damping, ang_damping, name, allow_rot:Boolean
		public function PhysSprite(ingraphmc:MovieClip, inphysmc:MovieClip, params:Object = null) 
		{
			_ws = Box2DHelper.worldScale;
			
			_physBody = Box2DHelper.convertMC2Body(inphysmc, (params.bodytype!=undefined)?params.bodytype:b2Body.b2_dynamicBody, params);
			
			//Set userdata
			if (params.userdata != undefined) {
				if (params.userdata.name != undefined) this.name = params.userdata.name;
			}
			
			if (params.allow_rot != undefined) {
				_fAllowRotate = params.allow_rot;
			}
			ingraphmc.x = 0;
			ingraphmc.y = 0;
			ingraphmc.scaleX = inphysmc.scaleX;
			ingraphmc.scaleY = inphysmc.scaleY;
			addChild(ingraphmc);
			addEventListener(Event.REMOVED, onRemoved);

			update();
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//------onRemoved
		private function onRemoved(e:Event):void 
		{
			//ContactListener(Box2DHelper.contactListener).unregisterContactCallback(_name);
			Box2DHelper.world.DestroyBody(_physBody);
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		//------update
		///Update position and rotation of sprite from PhysicBody
		public function update():void {
			var curPhysX:Number = _physBody.GetPosition().x * _ws * this.scaleX;
			var curPhysY:Number = _physBody.GetPosition().y * _ws * this.scaleY;
			
			if ((int(curPhysX << 4) != int(this.x << 4)) || (int(curPhysY << 4) != int(this.y << 4))) {
				
				this.x = curPhysX;
				this.y = curPhysY;
			}
			
			if ((_fAllowRotate) && (_physBody.GetAngle() != _radAngle)) {
				_radAngle = _physBody.GetAngle();
				this.rotation = _radAngle * MyMath.RAD2DEG_KOEF;
			}
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		public function get physBody():b2Body
		{
			return(_physBody);
		}
	}

}