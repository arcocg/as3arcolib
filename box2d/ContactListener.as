﻿package arco.box2d 
{
	import Box2D.Dynamics.Contacts.b2Contact;
	import Box2D.Dynamics.b2ContactListener;
	import flash.utils.Dictionary;
	
		
	/**
	 * ContactListener
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class ContactListener extends b2ContactListener
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		private var _dictContactCallbacks:Dictionary;
		private var _numCallbacks:int=1;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function ContactListener() 
		{
			_dictContactCallbacks = new Dictionary();
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		public override function BeginContact(contact:b2Contact):void {
			var userdata1:Object = contact.GetFixtureA().GetBody().GetUserData();
			var userdata2:Object = contact.GetFixtureB().GetBody().GetUserData();
			
			//trace(userdata2);
			
			if ((_numCallbacks > 0) && (userdata1 != null || userdata2 != null) ) {
				if (userdata1 && _dictContactCallbacks[userdata1.name]) _dictContactCallbacks[userdata1.name].call(this, userdata1, userdata2);
				if (userdata2 && _dictContactCallbacks[userdata2.name]) _dictContactCallbacks[userdata2.name].call(this, userdata2, userdata1);
				
			}
			
		}
		
		public function registerContactCallback(incontactname:String, inclbkfunc:Function):void {
			_dictContactCallbacks[incontactname] = inclbkfunc;
			_numCallbacks++;
		}
		
		public function unregisterContactCallback(incontactname:String):void {
			if (_dictContactCallbacks[incontactname]) {
				delete _dictContactCallbacks[incontactname];
				_numCallbacks--;
			}
		}
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}