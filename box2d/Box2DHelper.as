﻿package arco.box2d
{	
	import Box2D.Dynamics.*;
	import Box2D.Collision.*;
	import Box2D.Collision.Shapes.*;
	import Box2D.Common.Math.*;
	import flash.display.Stage;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import arco.utils.Input;

	import Box2D.Dynamics.Joints.*;
	import Box2D.Dynamics.Contacts.*;
	import Box2D.Common.*;
	
	import flash.display.Sprite;
	import flash.display.MovieClip;
	
	import flash.utils.getTimer;
		
	/**
	 * Box2DHelper
	 *
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * @box2D version 2.1;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class Box2DHelper
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		public static const BODY_TYPE_BOX:String = "box";
		public static const BODY_TYPE_CIRCLE:String = "circle";
		public static const BODY_TYPE_EDGE:String = "edge";
		
		public static const JOINT_TYPE_REVOLUTE:String = "revolute";
		public static const JOINT_TYPE_DISTANCE:String = "distance";
		
		private static var _contactListener:b2ContactListener;
		private static var _dbgSprite:Sprite;
		private static var _stage:Stage;
		private static var _world:b2World;
		private static var _world_scale:Number = 20.0;
		private static var t:Number;
		public static var updateKoef:Number = 700;
		
		//Mouse properties
		private static var _fMouseInteract:Boolean;
		public static var m_input:Input;
		public static var m_mouseJoint:b2MouseJoint;
		private static var mousePVec:b2Vec2 = new b2Vec2();
		// world mouse position
		static public var mouseXWorldPhys:Number;
		static public var mouseYWorldPhys:Number;
		static public var mouseXWorld:Number;
		static public var mouseYWorld:Number;
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function Box2DHelper(){
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		//------buildWorld
		///Builds new world and returns Sprite of DebugView
		public static function buildWorld(instage:Stage, contactLs:b2ContactListener = null, inmouseinteract:Boolean = false):Sprite {
			
			_stage = instage;
			_fMouseInteract = inmouseinteract;
			
			var gravity:b2Vec2 = new b2Vec2(0, 10);
			var doSleep:Boolean = true;
			_world = new b2World(gravity, doSleep);
			
			if(contactLs!=null){
				_world.SetContactListener(contactLs);
			}
			
			if (_fMouseInteract) {
				m_input = new Input(_stage);
			}
			
			return (buildDebugView());
		}
		
		//------buildDebugView
		public static function buildDebugView():Sprite{
			var dbgDraw:b2DebugDraw = new b2DebugDraw();
			_dbgSprite = new Sprite();
			dbgDraw.SetSprite(_dbgSprite);
			dbgDraw.SetDrawScale(_world_scale);
			dbgDraw.SetFlags(b2DebugDraw.e_shapeBit| b2DebugDraw.e_jointBit |b2DebugDraw.e_centerOfMassBit|b2DebugDraw.e_centerOfMassBit);
			_world.SetDebugDraw(dbgDraw);
			return (_dbgSprite);
		}
		
		
		//------convertMC2Body
		///Convert MovieClip to compound b2Body
		///Child MovieClips should be named as "box", "circle"
		///Params: bodytype (b2Body.b2_staticBody by default), density, x, y, sensor
		public static function convertMC2Body(inmc:MovieClip, bodytype:uint = 0,  params:Object = null):b2Body
		{
			//trace("[Box2DHelper] convertMC2Body");
			if (!params) params = new Object();
			var arrBodyShapes:Array = new Array();
			var mirrorx:Boolean = (params.mirrorx != undefined)?params.mirrorx:false;
			if (mirrorx) inmc.scaleX = -inmc.scaleX;
			var _sx:Number = inmc.scaleX;
			var _sy:Number = inmc.scaleY;
			
			var density:Number = (params.density!=undefined)?params.density:1;
			var friction:Number = (params.friction != undefined)?params.friction:0;;
			var restitution:Number = (params.restitution != undefined)?params.restitution:0;
			var ldamp:Number = (params.lin_damping != undefined)?params.lin_damping:0;
			var adamp:Number = (params.ang_damping != undefined)?params.ang_damping:0;
			
			
			var temp_bd:b2BodyDef = new b2BodyDef();
			//Set body position
			inmc.x = (params.x != undefined)?params.x:inmc.x;
			inmc.y = (params.y != undefined)?params.y:inmc.y;
			temp_bd.position.Set(inmc.x / _world_scale, inmc.y / _world_scale);
			//Set type of body
			temp_bd.type = bodytype;		
			
			//Set body rotation
			temp_bd.angle = inmc.rotation / 180 * Math.PI;
			
			
			var angle:Number = 0;
			
			//Extract child MovieClips from inmc and create shapes from them
			for (var i:int = 0; i < inmc.numChildren; i++ ) {
				var child:MovieClip = MovieClip(inmc.getChildAt(i));
				
				var conc_matrix:Matrix = child.transform.concatenatedMatrix;
				var px:Point = new flash.geom.Point(0, 1);
				px = conc_matrix.deltaTransformPoint(px);
				
				angle = (mirrorx)?((180 / Math.PI) * Math.atan2(px.y, px.x) - 90):child.rotation;
				
				var child_x:Number = (mirrorx)?conc_matrix.tx:child.x;
				
				
				child.rotation = 0;
				
				
				if (child.name.substr(0, 6) == "circle") {
					arrBodyShapes.push(createShape(child_x * _sx, child.y * _sy, "circle", { radius:(child.width >> 1)*_sx } ));
				}
				if (child.name.substr(0, 3) == "box") {
					arrBodyShapes.push(createShape(child_x*_sx, child.y*_sy, "box", { width:child.width*_sx, height:child.height*_sy, angle:angle }));
				}
				if (child.name.substr(0, 4) == "edge") {
					trace("Edge creating");
					var vertex_array:Array = new Array();
					trace("child.numChildren " + child.numChildren);
					for (var j:int = 0; j < child.numChildren; j++ ) {
						var point_child:MovieClip = child.getChildAt(j) as MovieClip;
						if (point_child.name.substr(0, 5) == "point") {
							//vertex_array.push([point_child.x, point_child.y]);
							vertex_array.push(new b2Vec2(point_child.x/_world_scale + child.x / _world_scale, point_child.y/_world_scale + child.y / _world_scale));
						}
					}
					trace("vertex_array length "+vertex_array.length);
					trace("arrBodyShapes length " + arrBodyShapes.length);
					
					if (vertex_array.length > 0) {
						var temp_shape:b2PolygonShape = new b2PolygonShape();
						for (j = 0; j < vertex_array.length - 1; j++) {
							
							//arrBodyShapes.push(createShape(child_x, child.y, "edge", { edges:[vertex_array[j], vertex_array[j + 1]] } ));
							//trace(b2PolygonShape(arrBodyShapes[arrBodyShapes.length - 1]).GetVertexCount());
							//var vec1:b2Vec2 = new b2Vec2(vertex_array[j][0] / _world_scale + child.x / _world_scale, vertex_array[j][1] / _world_scale + child.y  / _world_scale);
							//var vec2:b2Vec2 = new b2Vec2(vertex_array[j+1][0] / _world_scale + child.x / _world_scale, vertex_array[j+1][1] / _world_scale + child.y / _world_scale);
							//temp_shape.SetAsEdge(vec1, vec2);
							temp_shape.SetAsArray(vertex_array,vertex_array.length);
						}
						
						
						arrBodyShapes.push(temp_shape);
						
					} else arrBodyShapes.push(createShape(child_x, child.y, "box"));
					
					trace("arrBodyShapes length "+arrBodyShapes.length);
					 
					//trace("shape vertex count "+b2PolygonShape(arrBodyShapes[arrBodyShapes.length - 1]).GetVertexCount());
					
					//arrBodyShapes.push(createShape(child_x, child.y, "box"));
				}
				

			}

			
			var temp_body:b2Body = world.CreateBody(temp_bd);
			
			//Add to body all shapes
			if (arrBodyShapes.length > 0) {
					for (j = 0; j < arrBodyShapes.length; j++) {
						
						var mc:MovieClip = MovieClip(inmc.getChildAt(j));
						//var mc:MovieClip = new MovieClip();
						addFixture(temp_body, arrBodyShapes[j], 
									(mc.density != undefined)?mc.density:density, 
									(mc.friction != undefined)?mc.friction:friction,
									(mc.restitution != undefined)?mc.restitution:restitution, params);	
						
					}
			}
			
			temp_body.SetAngularDamping(adamp);
			temp_body.SetLinearDamping(ldamp);
			
			//Set userdata
			if (params.userdata != undefined) {
				temp_body.SetUserData(params.userdata);
			}
						
			return (temp_body);
			
		}
		
		//------createBody
		///intype: "box", "circle", bodytype: b2Body.b2_staticBody by default;
		///params: x, y, rotation, width, height, density, friction, restitution, sensor
		public static function createBody(intype:String = "box", bodytype:uint = 0,  params:Object = null):b2Body 
		{
			if (!params) params = new Object();
			
			//General properties
			var _x:Number = (params.x != undefined)?params.x:0;
			var _y:Number = (params.y != undefined)?params.y:0;
			var _w:Number = (params.width != undefined)?params.width:10;
			var _h:Number = (params.height != undefined)?params.height:10;
			var _r:Number = (params.radius != undefined)?params.radius:(_w>>1);
			var _rot:Number = (params.rotation != undefined)?params.rotation:0;
			var _ldamp:Number = (params.lin_damping != undefined)?params.lin_damping:0;
			var _adamp:Number = (params.ang_damping != undefined)?params.ang_damping:0;
			
			
			//Material properties
			var density:Number = (params.density!=undefined)?params.density:1;
			var friction:Number = (params.friction != undefined)?params.friction:1;
			var restitution:Number = (params.restitution != undefined)?params.restitution:0;
			
			//Edge(s) properties
			var edge_v1:b2Vec2, edge_v2:b2Vec2 = null;
			var vertex_array:Array = null;
			
			if (intype == "edge") {
				edge_v1 = (params.point1 != undefined)?params.vertex1:(new b2Vec2(0, 0));
				edge_v2 = (params.point2 != undefined)?params.vertex2:(new b2Vec2(100, -100));
				vertex_array = (params.edges != undefined)?params.edges:[edge_v1, edge_v2];
				//trace(vertex_array);
			}
			
			var temp_bd:b2BodyDef = new b2BodyDef();
			//Set body position
			temp_bd.position.Set(_x / _world_scale, _y / _world_scale);
			//Set type of body
			temp_bd.type = bodytype;
			//Set body rotation
			temp_bd.angle = _rot / 180 * Math.PI;
			
			var shape:b2Shape;
			
			//Get type
			if (intype.substr(0, 4) == "edge") {
				shape = createShape(0, 0, "edge", { edges:vertex_array } );
			}
			else if (intype.substr(0, 6) == "circle") {
				shape = createShape(0, 0, "circle", { radius:_r } );
			}
			else {
				shape = createShape(0, 0, "box", { width:_w, height:_h});
			}
			
			var temp_body:b2Body = world.CreateBody(temp_bd);
			addFixture(temp_body, shape, density, friction, restitution, params);
			temp_body.SetAngularDamping(_adamp);
			temp_body.SetLinearDamping(_ldamp);
			
			//Set userdata
			if (params.userdata != undefined) {
				temp_body.SetUserData(params.userdata);
			}
				
			
			return (temp_body);
		}
		
		
		
		//------buildJoint
		public static function buildJoint(body1:b2Body, body2:b2Body, type:String = "revolute", inparams:Object = null): b2Joint {
			switch (type) 
			{
				case JOINT_TYPE_REVOLUTE:
					return buildRevJoint(body1, body2, inparams);
				break;
				case JOINT_TYPE_DISTANCE:
					return buildDistJoint(body1, body2, inparams);
				break;
				default:
					return buildPrisJoint(body1, body2, inparams);
				break;
			}
		}
		
		/**
		* Builds distance joint
		* One of the simplest joint is a distance joint which says that the distance between two points on two bodies must be constant.
		* When you specify a distance joint the two bodies should already be in place. Then you specify the two anchor points in world coordinates.
		* The first anchor point is connected to body 1, and the second anchor point is connected to body 2.
		* These points imply the length of the distance constraint.
		* 
		* @param
		**/
		public static function buildDistJoint(body1:b2Body, body2:b2Body, inparams:Object = null):b2DistanceJoint{
			var jointDef:b2DistanceJointDef = new b2DistanceJointDef();
			jointDef.Initialize(body1, body2, body1.GetWorldCenter(), body2.GetWorldCenter());
			var joint:b2DistanceJoint = b2DistanceJoint(world.CreateJoint(jointDef));
			return joint;
		}
		
		//------buildRevJoint
		public static function buildRevJoint(body1:b2Body, body2:b2Body, inparams:Object = null):b2RevoluteJoint{
			var jointDef:b2RevoluteJointDef = new b2RevoluteJointDef();
			jointDef.Initialize(body1, body2, body2.GetWorldCenter());
			jointDef.enableMotor = true;
			jointDef.maxMotorTorque = 100;
			var joint:b2RevoluteJoint = b2RevoluteJoint(world.CreateJoint(jointDef));
			return joint;
		}
		
		//------buildPrisJoint
		public static function buildPrisJoint(body1:b2Body, body2:b2Body, inparams:Object = null):b2PrismaticJoint{
			var jointDef:b2PrismaticJointDef = new b2PrismaticJointDef();
			jointDef.Initialize(body1, body2, body2.GetWorldCenter(), new b2Vec2(1,0));
			jointDef.enableLimit = true;
			jointDef.lowerTranslation = jointDef.upperTranslation = 0;
			var joint:b2PrismaticJoint=b2PrismaticJoint(world.CreateJoint(jointDef));
			return joint;
		}
		
		//------removeBody
		public static function removeBody(body:b2Body):void{
			_world.DestroyBody(body);
		}
		
		//------removeJoint
		public static function removeJoint(joint:b2Joint):void{
			_world.DestroyJoint(joint);
		}
		
		//------update
		public static function update():void {
			var newt:Number = getTimer(); 
			_world.Step((newt - t) / updateKoef, 5, 5);
			_world.ClearForces();
			_world.DrawDebugData();
			t = newt;
			
			if (_fMouseInteract) {
				updateMouse();
				mouseDrag();
				mouseDestroy();
			}
		}
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//------addFixture
		public static function addFixture(inbody:b2Body, inshape:b2Shape, indensity:Number=1, infriction:Number=0, inrestitution:Number=0, params:Object = null):void {
			var my_fixture:b2FixtureDef = new b2FixtureDef();
			my_fixture.shape = inshape;
			my_fixture.density = indensity;
			my_fixture.friction = infriction;
			my_fixture.restitution = inrestitution;
			if (params) {
				if (params.sensor) my_fixture.isSensor = params.sensor;
			}
			
			inbody.CreateFixture(my_fixture);
		}
		
		

		//------createShape
		///Creates shape by type: "box", "circle"
		///Params: height, width, angle (degrees), radius
		public static function createShape(inx:Number, iny:Number, type:String="box", params:Object=null):b2Shape 
		{
			
			if (!params) params = new Object();
			var w:Number = (params.width == undefined)?10:params.width;
			var h:Number = (params.height == undefined)?10:params.height;
			var r:Number = (params.radius == undefined)?10:params.radius;
			var angle:Number = (params.angle == undefined)?0:params.angle;
			angle = angle / 180 * Math.PI; //Convert to radians
			
			if (type == "box"&& (params.angle != 0 || inx != 0 || iny != 0)) type = "obox";

			var temp_shape:b2Shape; 
			
			 switch (type) 
			 {
				 case "box":
					temp_shape = new b2PolygonShape();
					b2PolygonShape(temp_shape).SetAsBox(w / 2 / _world_scale, h / 2 / _world_scale);
				 break;
				 case "obox":
					temp_shape = new b2PolygonShape();
					b2PolygonShape(temp_shape).SetAsOrientedBox(w / 2 / _world_scale, h / 2 / _world_scale, new b2Vec2(inx/_world_scale, iny/_world_scale),angle);
				 break;
				 case "circle":
					temp_shape = new b2CircleShape(r/_world_scale);
					b2CircleShape(temp_shape).SetLocalPosition(new b2Vec2(inx/_world_scale, iny/_world_scale));
				 break;
				 case "edge":
					temp_shape = new b2PolygonShape();
					
					//b2PolygonShape(temp_shape).SetAsEdge(params.edges[0], params.edges[1]);
					
					/*for (var i:int = 0; i < params.edges.length; i++ ) {
						b2Vec2(params.edges[i]).x /= _world_scale;
						b2Vec2(params.edges[i]).y /= _world_scale;
						b2Vec2(params.edges[i]).x += (inx / _world_scale);
						b2Vec2(params.edges[i]).y += (iny / _world_scale);
					}*/
					/*for (i = 0; i < params.edges.length - 2; i++) {
						
					}*/
					if (params.edges.length >= 2) {
						var vec1:b2Vec2 = new b2Vec2(params.edges[0][0] / _world_scale + inx / _world_scale, params.edges[0][1] / _world_scale + iny / _world_scale);
						var vec2:b2Vec2 = new b2Vec2(params.edges[1][0] / _world_scale + inx / _world_scale, params.edges[1][1] / _world_scale + iny / _world_scale);
						b2PolygonShape(temp_shape).SetAsEdge(vec1, vec2);
						trace(vec1.y+"/"+vec2.y);
						//b2PolygonShape(temp_shape).SetAsArray(params.edges, params.edges.length);
					}
					/*else b2PolygonShape(temp_shape).SetAsEdge(params.edges[0], params.edges[1]);*/

				 break;
			 }
			
			return (temp_shape);

		}
		
		
		//======================
		// Update mouseWorld
		//======================
		/**
		* Should be called when mouse interact needed, before call of mouseDrag() and mouseDestroy() in update function
		* 
		* @param
		**/
		private static function updateMouse():void{
			mouseXWorldPhys = (Input.mouseX)/_world_scale; 
			mouseYWorldPhys = (Input.mouseY)/_world_scale; 
			
			mouseXWorld = (Input.mouseX); 
			mouseYWorld = (Input.mouseY); 
		}
		
		
		
		//======================
		// Mouse Drag 
		//======================
		private static function mouseDrag():void{
			// mouse press
			if (Input.mouseDown && !m_mouseJoint){
				
				var body:b2Body = GetBodyAtMouse();
				
				if (body)
				{
					var md:b2MouseJointDef = new b2MouseJointDef();
					md.bodyA = _world.GetGroundBody();
					md.bodyB = body;
					md.target.Set(mouseXWorldPhys, mouseYWorldPhys);
					md.collideConnected = true;
					md.maxForce = 300.0 * body.GetMass();
					m_mouseJoint = _world.CreateJoint(md) as b2MouseJoint;
					body.SetAwake(true);
				}
			}
			
			
			// mouse release
			if (!Input.mouseDown){
				if (m_mouseJoint)
				{
					_world.DestroyJoint(m_mouseJoint);
					m_mouseJoint = null;
				}
			}
			
			
			// mouse move
			if (m_mouseJoint)
			{
				var p2:b2Vec2 = new b2Vec2(mouseXWorldPhys, mouseYWorldPhys);
				m_mouseJoint.SetTarget(p2);
			}
		}
		
		
		
		//======================
		// Mouse Destroy
		//======================
		private static function mouseDestroy():void{
			// mouse press
			if (!Input.mouseDown && Input.isKeyPressed(68/*D*/)){
				
				var body:b2Body = GetBodyAtMouse(true);
				
				if (body)
				{
					_world.DestroyBody(body);
					return;
				}
			}
		}
		
		
		
		//======================
		// GetBodyAtMouse
		//======================
		
		public static function GetBodyAtMouse(includeStatic:Boolean = false):b2Body {
			// Make a small box.
			mousePVec.Set(mouseXWorldPhys, mouseYWorldPhys);
			var aabb:b2AABB = new b2AABB();
			aabb.lowerBound.Set(mouseXWorldPhys - 0.001, mouseYWorldPhys - 0.001);
			aabb.upperBound.Set(mouseXWorldPhys + 0.001, mouseYWorldPhys + 0.001);
			var body:b2Body = null;
			var fixture:b2Fixture;
			
			// Query the world for overlapping shapes.
			function GetBodyCallback(fixture:b2Fixture):Boolean
			{
				var shape:b2Shape = fixture.GetShape();
				if (fixture.GetBody().GetType() != b2Body.b2_staticBody || includeStatic)
				{
					var inside:Boolean = shape.TestPoint(fixture.GetBody().GetTransform(), mousePVec);
					if (inside)
					{
						body = fixture.GetBody();
						return false;
					}
				}
				return true;
			}
			_world.QueryAABB(GetBodyCallback, aabb);
			return body;
		}
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		public static function set contactListener(inlistener:b2ContactListener):void 
		{
			_contactListener = inlistener;
			_world.SetContactListener(inlistener);
		}
		
		public static function get contactListener():b2ContactListener 
		{
			return _contactListener;
		}
		
		public static function get world():b2World{
			if(_world==null){
				buildWorld(_stage);
			}
			return _world;
		}
		
		public static function get worldScale():Number {
			return (_world_scale);
		}
		
		public static function get debugSprite():Sprite {
			return(_dbgSprite);
		}
	}

}