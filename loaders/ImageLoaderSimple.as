﻿package arco.loaders
{
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.events.IOErrorEvent;
	
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.display.MovieClip;
	import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.Loader;
	import flash.display.Stage;
	import flash.display.Sprite;
	
	/**
	 * ImageLoader
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * 
	 * Helper class for loading image by given URL and downsample it if needed
	 * 
	 * Using: loadImage(inurl, inmaxw, inmaxh, incrop, inbmp)
	 * 			@inurl - url of image
	 * 
	 * Events: 	Event.COMPLETE - image loaded
	 * 			ProgressEvent.PROGRESS - loading progress
	 * 
	 * 
	 * @author arcomailbox@gmail.com
	 */
	
	public class ImageLoaderSimple extends MovieClip
	{	
		//Event types generating by object
		static public const VERSION_:Number = 0.01;
				
		private var currentURL:String;
		
		//Loader object to load an external image
		protected var _loader:Loader;
		private var loaded:Boolean = false;		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////CONSTRUCTOR///////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		public function ImageLoaderSimple()
		{
			
			//Initialize image loader
			_loader = new Loader();
			//add an onInit event to the contentLoadedInfo
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onImageLoaded);
			_loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			
			addChild(_loader);
		}
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////EVENTS-HANDLERS//////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////////////////////////onImageLoaded////////////////////////////////////////////////////
		///Stuff after image is loaded
		protected function onImageLoaded(event:Event):void {
			
			//var clipRect:Rectangle = null;
			var matrix:Matrix = null;
			//Bitmap(loader.content).smoothing = fSmoothing;
			
			
			_loader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, onProgress);
			dispatchEvent(event);
		}
		
		////////////////////////////////////////////////////onProgress////////////////////////////////////////////////////
		///Progress handler
		private function onProgress(e:ProgressEvent):void {
			dispatchEvent(e);
		}
		
		private function ioErrorHandler(e:IOErrorEvent):void 
		{
			dispatchEvent(e);
		}
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////MAIN FUNCTIONS///////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		///Load the images via this function
  		public function loadImage(inurl:String):void
		{
			if (_loader.contentLoaderInfo.bytesLoaded != _loader.contentLoaderInfo.bytesTotal) {
				
				_loader.close();
			}
			_loader.unloadAndStop();
			_loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgress);
			loaded = false;

		   //assign it to the urlRequest object
		   var urlRequest:URLRequest = new URLRequest(inurl);
		   
		   
		   currentURL = inurl;		   
		   
		   //load it
		   _loader.load(urlRequest);
  		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////GETTERS AND SETTERS//////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////version////////////////////////////////////////////////////
		///Returns url of loading(ed) image
		public function get url():String{
			return currentURL;
		}
		

		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////ADD FUNCTIONS///////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		
		//------isLoaded
		///Status of loading
		public function get isLoaded():Boolean 
		{
			return(loaded);
		}
		
		public function get loader():Loader 
		{
			return(_loader);
		}
		
		
		
	}
}