﻿package arco.loaders
{
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.events.IOErrorEvent;
	import flash.system.LoaderContext;
	
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.display.MovieClip;
	import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.Loader;
	import flash.display.Stage;
	import flash.display.Sprite;
	
	/**
	 * ImageLoader
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * 
	 * Helper class for loading image by given URL and downsample it if needed
	 * 
	 * Using: loadImage(inurl, inmaxw, inmaxh, incrop, inbmp)
	 * 			@inurl - url of image
	 * 			@inmaxw, @inmaxh - needed size of image, downsample by smallest side, if "0" - no downsampling occurs
	 * 			@incrop - if "true", image crops in the center with size inmaxw, inmaxh
	 * 
	 * Events: 	Event.COMPLETE - image loaded
	 * 			ProgressEvent.PROGRESS - loading progress
	 * 
	 * 
	 * @author arcomailbox@gmail.com
	 */
	
	public class ImageLoader extends MovieClip
	{	
		//Event types generating by object
		static public const VERSION_:Number = 0.546;
		
		static public const RESIZE_BY_MAX:int = 0; ///Resizes by longest of 2 sides
		static public const RESIZE_BY_MIN:int = 1; ///Resizes by shortest of 2 sides
		
		private var currentURL:String;
		
		//Loader object to load an external image
		protected var _loader:Loader;
		
		//which will be our resizer, after loading an image with the loader, 
		// we resize the fakeholder and retrieve the bitmap data
		private var fakeHolder:Sprite;
		
		//Bitmap object
		protected var bmp:Bitmap;
		//Current Bitmap data
		private var bmpData:BitmapData;
		
		//Maximum size of image (if greater - image downsample is occuring)
		private var maxImageWidth:uint, maxImageHeight:uint;
		
		//Size of image after fixing
		private var fixedWidth:uint, fixedHeight:uint;
		
		private var fResize:Boolean;
		private var fSmoothing:Boolean;
		private var fCrop:Boolean;

		private var loaded:Boolean = false;
		
		private var _resizeMode:int = RESIZE_BY_MAX;
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////CONSTRUCTOR///////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		public function ImageLoader()
		{
			
			//Initialize image loader
			_loader = new Loader();
			//add an onInit event to the contentLoadedInfo
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onImageLoaded);
			_loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			
			//create the fake holder
			fakeHolder = new Sprite();
			//and add to it the loader
			fakeHolder.addChild(_loader);
			fSmoothing = true;
		}
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////EVENTS-HANDLERS//////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////////////////////////onImageLoaded////////////////////////////////////////////////////
		///Stuff after image is loaded
		protected function onImageLoaded(event:Event):void {
			
			//var clipRect:Rectangle = null;
			var matrix:Matrix = null;
			if (loader.getChildAt(0) is Bitmap) Bitmap(loader.getChildAt(0)).smoothing = fSmoothing;
			
			//fix the size of the loaded which is inside the fakeHolder
    		if (fResize) fixSize(_loader);
			
			//Create new clipping rectancle if needed, clip center of image
			if (fCrop && fResize) {
				var left:int = Math.abs(maxImageWidth - fixedWidth) >> 1;
				var top:int = Math.abs(maxImageHeight - fixedHeight) >> 1;
				
				//clipRect = new Rectangle(left, top, _loader.width - left, _loader.height - top);

				matrix = new Matrix(1, 0, 0, 1, -left, -top);
			}
			
			//create a new bitmap data
			bmp.bitmapData = new BitmapData((fCrop&&fResize)?maxImageWidth:_loader.width,(fCrop&&fResize)?maxImageHeight:_loader.height, true, 0x00000000);

			_loader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, onProgress);
			
			//draw it from the fakeHolder to retrieve the data of the resized image
			bmp.bitmapData.draw(fakeHolder,matrix, null, null, null, fResize);
			bmp.smoothing = fSmoothing;
			loaded = true;
			
			dispatchEvent(event);
		}
		
		////////////////////////////////////////////////////onProgress////////////////////////////////////////////////////
		///Progress handler
		private function onProgress(e:ProgressEvent):void {
			dispatchEvent(e);
		}
		
		private function ioErrorHandler(e:IOErrorEvent):void 
		{
			
			dispatchEvent(e);
			trace("[ImageLoader] " + e);
		}
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////MAIN FUNCTIONS///////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		///Load the images via this function
  		public function loadImage(inurl:String, inmaxw:int = 0, inmaxh:int = 0, incrop:Boolean = false, inbmp:Bitmap = null):void
		{
			if (_loader.contentLoaderInfo.bytesLoaded != _loader.contentLoaderInfo.bytesTotal) {
				
				try 
				{
					_loader.close();
				}
				catch (e:Error){
					trace("[ImageLoader] loadImage: e = " + e );
				}
				
			}
			_loader.unloadAndStop();
			
			//If Bitmap object if given - use it, else - create new
		   if (inbmp) bmp = inbmp;
		   else {
			   if (bmp) {
				   removeChild(bmp);
			   }
			   bmp = new Bitmap();
			   addChild(bmp);
		   }
		   
			
			if (!inurl) return;
			
			
			_loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgress);
			loaded = false;
			if (inmaxw>0&&inmaxh>0) fResize=true;
			else fResize = false;
			
			currentURL = inurl;
			
			maxImageWidth = inmaxw;
			maxImageHeight = inmaxh;
			
		   //assign it to the urlRequest object
		   var urlRequest:URLRequest = new URLRequest(inurl);
		   
		   currentURL = inurl;
		   fCrop = incrop;
		   
		   //load it
		   var context:LoaderContext = new LoaderContext();
		   context.checkPolicyFile = true;
		   _loader.load(urlRequest, context);
  		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////GETTERS AND SETTERS//////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////version////////////////////////////////////////////////////
		///Returns url of loading(ed) image
		public function get url():String{
			return currentURL;
		}
		
		////////////////////////////////////////////////////outBitmapData////////////////////////////////////////////////////
		///Setter for output Bitmap
		public function set outBitmapData(bmpdata:BitmapData):void
		{
			bmpData = bmpdata;
		}
		
		
		////////////////////////////////////////////////////imageData////////////////////////////////////////////////////
		///Set Bitmap data
		public function set imageData(value:BitmapData):void{
			//if the old value is not null dispose the data
			 if (bmp.bitmapData!=null) {
				  bmp.bitmapData.dispose();
			 }			 
			 //set the new data
			 bmp.bitmapData = value; 
			 //force the smoothing
			 bmp.smoothing=true;			 
			
		}
		
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////ADD FUNCTIONS///////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////fixSize////////////////////////////////////////////////////
		///Fix size of new image
		private function fixSize(inloader:Loader):void {
	   		var sw:Number = 1;
			var sh:Number = 1;
			inloader.scaleX = 1;
			inloader.scaleY = 1;
			
			//Rescale uniform loaded image using given maxImageWidth and maxImageHeight
			if (inloader.width>maxImageWidth) {
				sw=maxImageWidth/inloader.width;
			}
			if (inloader.height>maxImageHeight) {
				sh=maxImageHeight/inloader.height;
  			}
			var s:Number;
			switch (_resizeMode) 
			{
				case RESIZE_BY_MAX:
					s = Math.max(sw, sh);
				break;
				case RESIZE_BY_MIN:
					s = Math.min(sw, sh);
				break;
			}
			
			fixedWidth = inloader.width *= s;
			fixedHeight = inloader.height *= s;
  		}
		
		
	
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		//------bitmapData
		///Returns BitmapData of loaded image
		public function get bitmap():Bitmap 
		{
			return(bmp);
		}
		
		//------bitmapData
		///Returns BitmapData of loaded image
		public function get bitmapData():BitmapData 
		{
			try 
			{
				return(bmp.bitmapData);
			}
			catch (e:Error){
				return null;
			}
			return null;
		}
		
		//------imageHeight
		///Returns fixed image height
		public function get imageHeight():Number{
			return fixedHeight;
		}

		//------imageWidth
		///Returns fixed image width
		public function get imageWidth():Number{
			return fixedWidth;
		}
		
		//------isLoaded
		///Status of loading
		public function get isLoaded():Boolean 
		{
			return(loaded);
		}
		
		public function get loader():Loader 
		{
			return(_loader);
		}
		
		//------smoothing
		//Sets smoothing
		public function set smoothing(inSmoothing:Boolean):void{
			fSmoothing = inSmoothing;
		}
		
		public function set resizeMode(inmode:int):void 
		{
			_resizeMode = inmode;
		}
		
	}
}