﻿package arco.loaders
{
	import classes.events.CustomEvent;
	
	import flash.net.URLRequest;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.display.MovieClip;
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	
	
		
	public class SWFLoader extends MovieClip
	{
		//Event types generating by object
		public static const ON_COMPLETE : String = 'onLoadComplete';
		public static const ON_PROGRESS : String = 'onLoadProgress';
		
		//Link to loaded SWF content
		private var loadedMC:DisplayObject;
		//Mask for hide swf-content out of boundaries
		private var swfMask:MovieClip;
		//Loader
		var mLoader:Loader;
		public var isLoaded:Boolean=false;
		private var bResize:Boolean;
		private var maxImageWidth, maxImageHeight:Number;
		private var srcUrl;
		

	public function SWFLoader(){
		//Init mask
		swfMask = new MovieClip();
		swfMask.graphics.clear();
		swfMask.graphics.beginFill(0xFFCC00);
		swfMask.graphics.drawRect(0,0, 100,100);
		swfMask.graphics.endFill();
		swfMask.visible=false;
		addChild(swfMask);
		//Init loader
		mLoader= new Loader();
	}
	
	public function load(url:String,maxW:int = 0,maxH:int = 0){
		isLoaded = false;
		srcUrl = url;
		var mRequest:URLRequest = new URLRequest(url);
		
		if (maxW>0&&maxH>0) bResize=true;
		else bResize=false;
		
		maxImageWidth=maxW;
		maxImageHeight=maxH;
		
		mLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onCompleteHandler);
		mLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgressHandler);
		mLoader.load(mRequest);
	}
	
	private function onCompleteHandler(loadEvent:Event)
	{
		var loader:Loader = Loader(loadEvent.target.loader);
		
		if (loadedMC) removeChild(loadedMC);
		loadedMC=addChild(loader.content);
			
		//Setting cut-out mask fo swf contents
		swfMask.width=loader.contentLoaderInfo.width;
		swfMask.height=loader.contentLoaderInfo.height;
		loadedMC.mask=swfMask;
		
		dispatchEvent( new CustomEvent (ON_COMPLETE , {loadedWidth:loader.contentLoaderInfo.width,loadedHeight:loader.contentLoaderInfo.height,loadedURL:srcUrl,mc:loadedMC} ) );
		
		mLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onCompleteHandler);
		mLoader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, onProgressHandler);
		
		if (bResize) fixSize();
	}

	private function onProgressHandler(mProgress:ProgressEvent)
	{
	var percent:Number = mProgress.bytesLoaded / mProgress.bytesTotal;
	
	dispatchEvent( new CustomEvent ( ON_PROGRESS, {progress:percent} ) );
	
	if (percent==1) isLoaded=true;
	
	}
	
	public function get Area():Rectangle{
		return new Rectangle(swfMask.x,swfMask.y,swfMask.width,swfMask.height);
	}
	
private function fixSize() {
	trace("fixSize");
	var sw:Number=1;
	var sh:Number=1;
	this.scaleX=1;
	this.scaleY=1;
	
	//depending on the stageW and stageH and without loosing the ratio
	//the loaded is resized
	if (this.width>maxImageWidth) {
		sw=maxImageWidth/this.width;
	}
	if (this.height>maxImageHeight) {
		sh=maxImageHeight/this.height;
  	}
	var s:Number=Math.min(sw,sh);
	s=sw;
	this.width*=s;
	this.height*=s;

 	}
	}//end of class
}//end of package