package arco.sockets
{
	import arco.utils.Dbg;
	import com.greensock.TweenMax;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.XMLSocket;
	import flash.system.Security;
	import mx.utils.URLUtil;
	import com.adobe.serialization.json.JSON;
		
	/**
	 * SocketConnector
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class SocketConnector extends EventDispatcher
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		public static const DATAFORMAT_JSON:String = "JSON";
		
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		private var _dataFormat:String;
		private var _name:String;
		private var _path:String;
		private var _socket:XMLSocket = new XMLSocket();
		
		
		private var _autotartTryCount:int = 5;
		private var _autotartTryTotal:int = 0;
		private var _autostartTryDelay:int = 2;
		private var _autostartEnabled:Boolean = true;
		
		private var _delayedPacket:Object;
		private var _commandsBuffer:Array = new Array();
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------SocketConnector
		public function SocketConnector(inpath:String, inconnectionname:String = "", indataformat:String = DATAFORMAT_JSON) 
		{
			
			_dataFormat = indataformat;
			_name = inconnectionname;
			_path = inpath;
			_socket.addEventListener(Event.CLOSE, onSocket_closed);
			_socket.addEventListener(Event.CONNECT, onSocket_connect);
			_socket.addEventListener(DataEvent.DATA, onSocket_data);
			_socket.addEventListener(IOErrorEvent.IO_ERROR, onSocket_ioError);
			_socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSocket_secError);
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		private function onSocket_data(event:DataEvent):void
		{
			Dbg.log("[SocketConnector] onSocket_data: DATA = " + event.data, Dbg.CH_RESPONSE);
			var _data:Object;
			switch (_dataFormat) 
			{
				case DATAFORMAT_JSON:
					_data = JSON.decode(event.data);
				break;
				default:
					_data = event.data;
				break;
			}
			
			dispatchEvent(new SocketConnectorEvent(SocketConnectorEvent.ON_DATA, _data));
		}
		
		
		private function onSocket_closed(e:Event):void 
		{
			Dbg.log("[SocketConnector] DISCONNECTED " + _name, Dbg.CH_WARN);
			_socket.close();
			if (_autostartEnabled) initReconnect();
			dispatchEvent(new SocketConnectorEvent(SocketConnectorEvent.DISCONNECTED));
		}
		
		private function onSocket_connect(e:Event):void 
		{
			Dbg.log("[SocketConnector] CONNECTED " + _name);
			killReconnect();
			dispatchEvent(new SocketConnectorEvent(SocketConnectorEvent.CONNECTED));
			if (_delayedPacket) {
				send(_delayedPacket);
				_delayedPacket = null;
			}
		}
		private function onSocket_secError(e:SecurityErrorEvent):void 
		{
			Dbg.log("[SocketConnector] SECURITY ERROR " + _name + " " + e.toString(), Dbg.CH_ERROR);
			if (_autostartEnabled) connect();
			dispatchEvent(new SocketConnectorEvent(SocketConnectorEvent.SECURITY_ERROR));
			
		}
		
		private function onSocket_ioError(e:IOErrorEvent):void 
		{
			Dbg.log("[SocketConnector] ERROR " + _name + " " + e.toString(), Dbg.CH_ERROR);
			dispatchEvent(new SocketConnectorEvent(SocketConnectorEvent.IO_ERROR));
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		 * init
		 * 
		 * @param intryamount	Number of tries to startup server if connection has been lost
		 * @param intrydelay	Delay beetween connection tries
		**/
		public function enableAutostartup(intryamount:int, intrydelay:Number):void 
		{
			_autostartEnabled = true;
			_autotartTryTotal = _autotartTryCount = intryamount;
			_autostartTryDelay = intrydelay;
		}
		
		
		
		public function connect():void {
			Security.loadPolicyFile("xmlsocket://46.4.86.227:843");
			if (!_socket.connected)	_socket.connect(URLUtil.getServerName(_path), URLUtil.getPort(_path));
			else dispatchEvent(new SocketConnectorEvent(SocketConnectorEvent.CONNECTED));
		}
		
		
		public function disconnect():void 
		{
			killReconnect();
			if (_socket.connected) _socket.close();		
		}
		
		public function disableAutostartup():void 
		{
			killReconnect();
			_autostartEnabled = false;
		}
		
		public function send(indata:Object, intrace:Boolean = true):void 
		{
			if (!_socket.connected) {
				Dbg.log("[SocketConnector] send: " + _name + " DISCONNECTED!", Dbg.CH_REQUEST);
				return;
			}
			/*if (!_socket.connected && _autostartEnabled) {
				_commandsBuffer.push(indata);
				connect();
				return;
			}*/
			var _json:String = JSON.encode(indata);
			if (intrace) {
				Dbg.log("[SocketConnector] SEND: ", Dbg.CH_REQUEST);
				//Dbg.inspect(indata);
				Dbg.log(_json);
			}
			

			if (_dataFormat == DATAFORMAT_JSON)
				_socket.send(_json)
			else _socket.send(indata);
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		private function initReconnect():void 
		{
			_autotartTryCount = 0;
			tryToReconnect();
		}
		
		private function killReconnect():void 
		{
			TweenMax.killDelayedCallsTo(tryToReconnect);
			_autotartTryCount = _autotartTryTotal;
		}
		
		private function tryToReconnect():void 
		{
			_autotartTryCount ++;
			if (_autotartTryCount > _autotartTryTotal) return;
			connect();
			TweenMax.delayedCall(_autostartTryDelay, tryToReconnect);
		}
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------\
		public function get isConnected():Boolean { return _socket.connected; }
	}

}