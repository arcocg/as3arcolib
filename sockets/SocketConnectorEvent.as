package arco.sockets
{
	import flash.events.Event;
	
	/**
	 * SocketConnectorEvent
	 * 
	 * @author arco
	 */
	public class SocketConnectorEvent extends Event 
	{
		/** Definitions for all event types. **/
		
		public static const IO_ERROR:String = "onIOError";
		public static const SECURITY_ERROR:String = "onSecurityError";
		public static const CONNECTED:String = "onConnected";
		public static const DISCONNECTED:String = "onDisconnected";
		public static const ON_DATA:String = "onData"; //data - recieved data
		
		/** The data associated with the event. **/
		private var _data:Object;
		
		/**
		* Constructor; sets the event type and inserts the new value.
		*
		* @param typ	The type of event.
		* @param dat	An object with all associated data.
		**/
		public function SocketConnectorEvent(type:String, dat:Object=undefined, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			_data = dat;
		}
		
		/** Returns the data associated with the event. **/
		public function get data():Object {
			return _data;
		};
		
		public override function clone():Event 
		{ 
			return new SocketConnectorEvent (type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("SocketConnectorEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}