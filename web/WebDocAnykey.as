package arco.web 
{
	import flash.events.ContextMenuEvent;
	import flash.net.URLRequest;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	import flash.net.navigateToURL;
	/**
	 * WebDocAnykey
	 * 
	 * @playerversion Flash 9.0;
	 * @author arcomailbox@gmail.com
	 */
	public class WebDocAnykey extends WebDoc
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------

		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function WebDocAnykey() 
		{
			addAnykeyToMenu();
		}
		
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		private function addAnykeyToMenu():void 
		{
			var my_menu:ContextMenu = new ContextMenu();
			my_menu.hideBuiltInItems();
			contextMenu = my_menu;
			var my_notice:ContextMenuItem = new ContextMenuItem("сделано в «Anykey»");
			my_menu.customItems.push(my_notice);
			my_notice.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, openAnykeyLink)
		}
		
		private function openAnykeyLink(e:ContextMenuEvent):void{
			navigateToURL(new URLRequest("http://www.anykey.kz"));
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		public override function init():void 
		{
			super.init();
			//addAnykeyToMenu();
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}