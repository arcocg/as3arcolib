package arco.web 
{
	import flash.display.MovieClip;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	/**
	 * WebDoc
	 * 
	 * @playerversion Flash 9.0;
	 * @author arcomailbox@gmail.com
	 */
	public class WebDoc extends MovieClip
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------

		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function WebDoc() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		private function onAdded(e:Event):void 
		{
			init();
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		public function init():void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}