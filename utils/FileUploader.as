package arco.utils 
{
	import flash.events.DataEvent;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import flash.events.Event;
	import flash.events.ProgressEvent;
	
	import arco.utils.FileUploaderEvent;
	/**
	 * FileUploader
	 * 
	 * @playerversion Flash 9.0;
	 * @author arcomailbox@gmail.com
	 * 
	 * Version 0.11
	 * 
	 */
	public class FileUploader extends EventDispatcher
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		private var _path:String;
		private var _serverPath:String;
		
		private var imageTypes:FileFilter = new FileFilter("Images (*.avi, *.mpeg, *.mpg, *.mov, *.flv, *.f4v)", "*.avi; *.mpeg; *.mpg; *.mov; *.flv; *.f4v; *.mp4");
		private var allTypes:Array = new Array(imageTypes);
		private var _fileRef:FileReference = new FileReference();
		
		private var _request:URLRequest;
	
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function FileUploader(inserverpath:String) 
		{
			_serverPath = inserverpath;
			_request = new URLRequest(_serverPath);
			_fileRef.addEventListener(Event.SELECT, syncVariables);
			_fileRef.addEventListener(Event.COMPLETE, completeHandler);
			_fileRef.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			_fileRef.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			_fileRef.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			_fileRef.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, responseHandler);
		}
		
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		// Function that fires off when File is selected from PC and Browse dialogue box closes
		private function syncVariables(event:Event):void {
			_path = "" + _fileRef.name;
			
			dispatchEvent(new FileUploaderEvent(FileUploaderEvent.FILE_SELECTED, {path:_path}));
		}
		
		// Function that fires off when upload is complete
		private function completeHandler(event:Event):void {
			/*uploadMsg.visible = false;
			blocker.visible = true;
			status_txt.text = _fileRef.name + " has been uploaded.";
			fileDisplay_txt.text = "";*/
			//trace(event);
			dispatchEvent(new FileUploaderEvent(FileUploaderEvent.UPLOAD_COMPLETE));
		}
		
		// Function that fires off when the upload progress begins
		private function progressHandler(event:ProgressEvent):void {
			// we want our progress bar to be 200 pixels wide when done growing so we use 200*
			// Set any width using that number, and the bar will be limited to that when done growing
			//progressBar.width = Math.ceil(200*(event.bytesLoaded/event.bytesTotal));
			dispatchEvent(new FileUploaderEvent(FileUploaderEvent.UPLOAD_PROGRESS, { percent:event.bytesLoaded / event.bytesTotal} ));
		}
		
		private function ioErrorHandler(event:IOErrorEvent):void {
            trace("ioErrorHandler: " + event);
			dispatchEvent(new FileUploaderEvent(FileUploaderEvent.ERROR, { error:event} ));
        }
		
		private function responseHandler(event:DataEvent):void 
		{
			/*var response:XML = XML( event.data );
			
			trace("[FileUploader] responseHandler: response = " + response.toString() );*/
			trace("[FileUploader] responseHandler: event.data = " + event.data );
			dispatchEvent(new FileUploaderEvent(FileUploaderEvent.UPLOAD_RESPONSE, {data:event.data} ));
		}
		
		private function securityErrorHandler(event:SecurityErrorEvent):void {
			trace("securityErrorHandler: " + event);
			dispatchEvent(new FileUploaderEvent(FileUploaderEvent.ERROR, { error:event} ));
        }

		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		public function selectFile():void
		{
			_fileRef.browse(allTypes);
		}
		
		// Function that fires off when the user presses the "upload it now" btn
		public function upload(invars:URLVariables = null):void {
			if (_path == null) {
				throw(new Error("[FileUploader] File not selected!"));
				return;
			}
			var variables:URLVariables ;
			
			if (!invars) variables = new URLVariables();
			else variables = invars;
			//variables.todayDate = new Date();
			//variables.Name = "Dude"; // This could be an input field variable like in my contact form tutorial : )
			//variables.Email = "someDude@someEmail.com"; // This one the same
			_request.method = URLRequestMethod.POST;
			_request.data = variables;
			
			/*uploadMsg.visible = true;
			
			upload_btn.visible = false;*/
			_fileRef.upload(_request);
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		public function get fileRef():FileReference { return _fileRef; }
		public function get path():String 
		{
			return(_path);
		}
	}

}