﻿package arco.utils 
{
	import flash.display.Stage;
	
	/**
	 * Dbg
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * Debug helper class
	 * 
	 * @author arcomailbox@gmail.com
	 */
	import com.junkbyte.console.Cc;
	 
	public class Dbg
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		private static var stage:Stage;
		
		
		public static const CH_REQUEST:int = 2;
		public static const CH_RESPONSE:int = 3;
		public static const CH_DEBUG:int = 5;
		public static const CH_WARN:int = 7;
		public static const CH_ERROR:int = 9;
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function Dbg() 
		{
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		//------init
		public static function addConsole(instage:Stage, inwidth:int = 0, inheight:int = 0):void 
		{
			stage = instage;
			
			CONFIG::CONSOLE {
				Cc.startOnStage(instage, "`");
				Cc.x = 5;
				Cc.y = 5;
				if (inwidth == 0) Cc.width = instage.stageWidth - 10;
				else Cc.width = inwidth;
				if (inheight == 0) Cc.height = instage.stageHeight - 10;
				else Cc.height = inheight;
				
				//Config
				Cc.commandLine = true;
				//Cc.addGraph("mouse", stage, "mouseX", 0xFF0000, "x");
				//Cc.config.commandLineAllowed = true
			}
		}
		
		//------log
		/**
		 * Logs to JunkByte Console, if CONFIG::CONSOLE = true and trace it, it CONFIG::TRACE = true
		 * 
		 * @param intext	Text to trace
		 * @param inch		Number of channel, accepts staitic consts values CH_
		 * 
		**/
		public static function log(intext:*, inch:int = 0):void {
			var _msg:String = String("\n" + intext);
			CONFIG::CONSOLE {
				
				switch (inch) 
				{
					case 0:
						Cc.log(_msg);
					break;
					case CH_ERROR:
						Cc.errorch("Err", _msg);
					break;
					case CH_WARN:
						Cc.warnch("Warn", _msg);
					break;
					case CH_DEBUG:
						Cc.debugch("Warn", _msg);
					break;
					case CH_REQUEST:
						Cc.ch("Request", _msg, CH_RESPONSE);
					break;
					case CH_RESPONSE:
						Cc.ch("Response", _msg, CH_RESPONSE);
					break;
					case CH_RESPONSE:
						Cc.ch("Response", _msg, CH_RESPONSE);
					break;
				}
			}
			CONFIG::TRACE {
				switch (inch) 
				{
					case CH_ERROR:
						trace("[**Err] " + _msg);
					break;
					case CH_WARN:
						trace("[*Warn] " + _msg);
					break;
					case CH_REQUEST:
						trace("[Request] " + _msg);
					break;
					case CH_RESPONSE:
						trace("[Response] " + _msg);
					break;
					default:
						trace(_msg);
					break;
				}
					
			}
		}
		
		//------inspect
		/**
		 * Logs object hierarhicaly to JunkByte Console, if CONFIG::CONSOLE = true and trace it, it CONFIG::TRACE = true
		 * 
		 * @param obj	Object to convert
		 * 
		**/
		public static function inspect(inobj:Object):void 
		{
			CONFIG::CONSOLE {
				Cc.inspect(inobj);
			}
			CONFIG::TRACE {
				trace(print_r(inobj));
			}
		}
		
		
		//------print_r
		/**
		 * Returns gotten object in hierarhical string
		 * 
		 * @param obj	Object to convert
		 * 
		**/
		public static function print_r(obj:*, level:int = 0, output:String = ""):String
		{
			var tabs:String = "";
			for (var i:int = 0; i < level; i++, tabs += "\t") { };

			for (var child:* in obj)
			{
				output += tabs + "["+ child + "] => " + obj[child];

				var childOutput:String = print_r(obj[child], level + 1);
				if (childOutput != "") output += " {\n"+ childOutput + tabs + "}";

				output += "\n";
			}

			if (level > 20) return "";
			//else if (level == 0) trace(output); else return output;
			return output;
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}