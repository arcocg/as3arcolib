package arco.utils 
{
	/**
	 * PadZero
	 * 
	 * @playerversion Flash 9.0;
	 * @author arcomailbox@gmail.com
	 */
	public class PadZero
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------

		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function PadZero() 
		{
			
		}
		
		//------convert
		///Convert int to String with given number of zeros in the begin
		public static function convert(inputNumber:Number,numberOfDigits:int):String {
			var paddedString:String = inputNumber.toString();		
			while (paddedString.length < numberOfDigits) {
				paddedString = "0" + paddedString;
			}
			return paddedString;
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		//------init
		public function init():void {
			
			
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}