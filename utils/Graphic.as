package arco.utils 
{
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
		
	/**
	 * Graphic
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class Graphic
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------Graphic
		public function Graphic() 
		{
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		/**
		* Draw rectatgle with passed color and size in specified Sprite
		* 
		* @param inmc MovieClip 
		* 
		**/
		public static function drawRect(inmc:Sprite, inx:int, iny:int, inw:int, inh:int, incolor:uint = 0xff0000, inalpha:Number = 1):void 
		{
			inmc.graphics.beginFill(incolor, inalpha);
			inmc.graphics.drawRect(inx, iny, inw, inh);
			inmc.graphics.endFill();
		}
		
		/**
		* Create and return rectangle Sprite with specified parameters of x, y, width, height
		**/
		public static function  newMask(inx:int, iny:int, inw:int, inh:int):Sprite {
			var _msk:Sprite = new Sprite();
			drawRect(_msk, inx, iny, inw, inh);
			return(_msk);
		}
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}