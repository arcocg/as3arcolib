package arco.utils 
{
	import com.greensock.easing.Sine;
	import com.greensock.TweenMax;
	import flash.display.DisplayObject;
		
	/**
	 * ColorUtils
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class ColorUtils
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------ColorUtils
		public function ColorUtils() 
		{
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		* Set color of car
		**/
		public static function setColor(inmc:DisplayObject, incolor:uint):void 
		{
			var r: Number = ((incolor>> 16) & 0xff);
			var g: Number = ((incolor>> 8) & 0xff);
			var b: Number = (incolor & 0xff);
			
			TweenMax.to(inmc, 0, { colorTransform:{redOffset:r, greenOffset:g, blueOffset:b}} );
	
		}
		
		/**
		* Set color of car with animation
		**/
		public static function setColorSmooth(inmc:DisplayObject, incolor:uint):void 
		{
			var r: Number = ((incolor>> 16) & 0xff);	
			var g: Number = ((incolor>> 8) & 0xff);
			var b: Number = (incolor & 0xff);
			
			TweenMax.to(inmc, .5, { colorTransform: { redOffset:r, greenOffset:g, blueOffset:b }, ease:Sine.easeOut } );
		}
		
		
		public static function uint2hex(dec:uint) : String
		{
			var digits:String = "0123456789ABCDEF";
			var hex:String = '';

			while (dec > 0)
			{
				var next:uint = dec & 0xF;
				dec >>= 4;
				hex = digits.charAt(next) + hex;
			}

			if (hex.length == 0) hex = '0'

			return hex;
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}