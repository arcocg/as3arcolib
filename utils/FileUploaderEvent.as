/**
* Definitions for all event types fired by FileUploader
*
**/
package arco.utils{


import flash.events.Event;


public class FileUploaderEvent extends Event {


	/** Definitions for all event types. **/
	public static const FILE_SELECTED:String = "onFileSelected";
	public static const UPLOAD_PROGRESS:String = "onUploadProgress";
	public static const UPLOAD_COMPLETE:String = "onUploadComplete";
	public static const UPLOAD_RESPONSE:String = "onUploadResponse"; //data: data
	public static const ERROR:String = "onUploadError";
	
	/** The data associated with the event. **/
	private var _data:Object;


	/**
	* Constructor; sets the event type and inserts the new value.
	*
	* @param typ	The type of event.
	* @param dat	An object with all associated data.
	**/
	public function FileUploaderEvent(typ:String,dat:Object=undefined,bbl:Boolean=false,ccb:Boolean=false):void {
		super(typ,bbl,ccb);
		_data = dat;
	};


	/** Returns the data associated with the event. **/
	public function get data():Object {
		return _data;
	};


}


}