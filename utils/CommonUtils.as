package arco.utils 
{
	import adobe.utils.CustomActions;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
		
	/**
	 * CommonUtils
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class CommonUtils
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------CommonUtils
		public function CommonUtils() 
		{
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		 * Constrains value in min and max bounds
		 * 
		 * @param invalue	Value to constrain
		 * @param inmin		Min possible value
		 * @param inmax		Max possible value
		**/
		public static function constrain(invalue:Number, inmin:Number, inmax:Number):Number{
			return((invalue<inmin)?inmin:((invalue>inmax)?inmax:invalue));
		}
		
		/**
		 * Constrains value in min and max bounds, but if it greater than inmax - set it to inmin and vice versa
		 * 
		 * @param invalue	Value to constrain
		 * @param inmin		Min possible value
		 * @param inmax		Max possible value
		**/
		public static function constrainCycle(invalue:Number, inmin:Number, inmax:Number):Number{
			return((invalue<inmin)?inmax:((invalue>inmax)?inmin:invalue));
		}
	
		
		public static function col(i:int,n:int):int {			
			return ((i%n)==0)?n:(i%n);
		}
		
		public static function row(i:int,n:int):int {			
			return (Math.floor(i/n) + (i%n!=0?1:0));
		}		
		
		public static function rand(max:int, min:int=0):int {			
			return (Math.floor(Math.random() * (max - min + 1)) + min);
		}
		
		public static function openURL(url:String):void {
			navigateToURL(new URLRequest(url), '_blank');
		}
		
		public static function getLastDelayTime(startTime:uint):Number {
			var elapsedTime:uint = CommonUtils.time() - startTime;
			return (elapsedTime < 1.5 * 1000?(1.5 * 1000 - elapsedTime) * 0.001:0);			
		}
		
		
		public static function getUniqueID():Number 
		{
			return(Number((new Date().time) + uint(Math.random() * 100000)));
		}
		
		public static function time():uint {
			return (new Date()).getTime();
		}
		
		public static function nouns(num:*, s1:String, s2:String, s3:String):String {
			num = int(num);
			var num_str:String = num.toString();
			if (num == 0) return s3;
			else if (num != 11 && num_str.charAt(num_str.length - 1) == "1") return s1;
			else if (num < 5) return s2;
			else if (num == 11) return s3;
			else return s3;
			return "";
		}
		
		public static function nounsArr(num:uint, arr:Array):String {
			return(nouns(num, arr[0], arr[1], arr[2]));
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}