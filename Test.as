package arco
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.display.StageAlign;
	import flash.events.Event;

	
	/**
	 * Test
	 * 
	 * @playerversion Flash 9.0;
	 * @author arcomailbox@gmail.com
	 */
	public class Test extends MovieClip
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		var bmp1:Bitmap;
		var bmp2:Bitmap;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		private var _fFirstPhase:Boolean = true;
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function Test() 
		{
			stage.displayState = StageDisplayState.FULL_SCREEN;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			init();
		}
		
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		private function onResize(e:Event=null):void 
		{
			bmp1.x = (stage.stageWidth >> 1) - (bmp1.width >> 1);
			bmp1.y = (stage.stageHeight >> 1) - (bmp1.height >> 1);
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		//------init
		public function init():void {
			bmp1 = new Bitmap(new Bmp(2000, 1500), "auto", true);
			bmp1.smoothing = true;
			addChild(bmp1);
			bmp2 = new Bitmap(new Bmp(2000, 1500), "auto", true);
			bmp2.smoothing = true;
			bmp2.scaleX = bmp2.scaleY = 0;
			bmp2.alpha = 0;
			addChild(bmp2);
			
			addEventListener(Event.RESIZE, onResize);
			
			scaleBmps(bmp1, bmp2);
			
			onResize();
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		private function scaleBmps(inbmp1:Bitmap, inbmp2:Bitmap):void 
		{
			/*TweenMax.to(inbmp1, 7, { alpha:0, onUpdate:function() {
				inbmp1.x = (stage.stageWidth >> 1) - (inbmp1.width >> 1);
				inbmp1.y = (stage.stageHeight >> 1) - (inbmp1.height >> 1)
			}, scaleX:5, scaleY:5, onComplete:function() {
				inbmp1.scaleX = inbmp1.scaleY = 0;
				
			},
			ease:Cubic.easeInOut } );
			
			TweenMax.to(inbmp2, 7, { alpha:1, onUpdate:function() {
				inbmp2.x = (stage.stageWidth >> 1) - (inbmp2.width >> 1);
				inbmp2.y = (stage.stageHeight >> 1) - (inbmp2.height >> 1)
			}, scaleX:1, scaleY:1, onComplete:function() {
				_fFirstPhase = !_fFirstPhase;
				if (_fFirstPhase) scaleBmps(bmp1, bmp2);
				else scaleBmps(bmp2, bmp1);
			},
			ease:Cubic.easeInOut } );*/
		/*	inbmp1.scaleX = inbmp1.scaleY = 1;
			inbmp2.scaleX = inbmp2.scaleY = 0;
			inbmp1.alpha = 1;
			inbmp2.alpha = 0;*/
			TweenMax.to(inbmp1, 7, { alpha:0, onUpdate:function() {
				inbmp1.x = (stage.stageWidth >> 1) - (inbmp1.width >> 1);
				inbmp1.y = (stage.stageHeight >> 1) - (inbmp1.height >> 1);
				inbmp2.scaleX = inbmp2.scaleY = inbmp1.scaleX / 5;
				inbmp2.alpha = 1 - inbmp1.alpha;
				inbmp2.x = (stage.stageWidth >> 1) - (inbmp2.width >> 1);
				inbmp2.y = (stage.stageHeight >> 1) - (inbmp2.height >> 1)
			}, scaleX:5, scaleY:5, onComplete:function() {
				
				_fFirstPhase = !_fFirstPhase;
				if (_fFirstPhase) scaleBmps(bmp1, bmp2);
				else scaleBmps(bmp2, bmp1);
			},
			ease:Cubic.easeInOut } );
		}
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}