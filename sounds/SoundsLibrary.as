package arco.sounds 
{
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.utils.Dictionary;
		
	/**
	 * SoundsLibrary
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class SoundsLibrary
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		public static const SOUND_CLICK:String = "soundClick";
		public static const SOUND_HOVER:String = "soundHover";
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		public static var cha:SoundChannel;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		public static var isMuted:Boolean = false;
		private static var _dictSounds:Dictionary = new Dictionary();
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------SoundsLibrary
		public function SoundsLibrary() 
		{
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		/**
		* Play sound from library with specified id
		* 
		* @param insoundid	ID of sound to play
		**/
		public static function play(insoundid:String, involume:Number = 1):void 
		{
			if (!_dictSounds[insoundid]) return;		
			if (isMuted) return;
			
			cha = _dictSounds[insoundid].play();
			var stransform:SoundTransform = new SoundTransform();
			stransform.volume = involume;
			cha.soundTransform = stransform;
		}
		
		/**
		* Add one sound with specified id
		* 
		* @param insoundid	String with sound name to access with it thu play() to this sound in future
		* @param insound 	Sound to keep in library
		**/
		public static function registerSound(insoundid:String, insound:Sound):void 
		{
			 _dictSounds[insoundid] = insound;
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}