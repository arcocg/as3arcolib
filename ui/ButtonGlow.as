package arco.ui
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	
	/**
	 * ButtonGlow
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * Class for button
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class ButtonGlow extends ButtonBase
	{
	
		private var _skin:MovieClip;
	
		
		override protected function out():void 
		{
			TweenMax.to(_skin, 0.5, {  colorTransform: { redOffset:0, greenOffset:0, blueOffset:0 }, glowFilter:{color:0xFFFFFF, strength:0, quality:2, blurX:0,blurX:0}} );
			super.out();
		}
		
		override protected function over():void 
		{
			TweenMax.to(_skin, .3, {  colorTransform: { redOffset:50, greenOffset:50, blueOffset:50 },  glowFilter:{color:0xFFFFFF, strength:1, quality:2, blurX:10,blurX:10}} );			
			super.over();
		}
	
		public function ButtonGlow() 
		{
			_skin = this;
			
			buttonMode = true;
		
			
		}
		
		public function get isActive():Boolean
		{
			return(buttonMode);
		}
		
	}

}