package arco.ui
{
	import arco.utils.Dbg;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import com.greensock.TweenMax;
	import com.greensock.easing.*;

	
	/**
	 * ButtonBaseExt
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * Class for button
	 * 
	 * @author arcomailbox@gmail.com
	 */

	
	public class ButtonBaseExt extends ButtonBase
	{
		protected var _isDown:Boolean = false;
		
		
		protected function onMouse_down(e:MouseEvent):void 
		{
			_isDown = true;
			//Dbg.log("[ButtonBaseExt] onMouse_down: stage = " + stage, Dbg.CH_DEBUG);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouse_up);
			root.addEventListener(MouseEvent.MOUSE_UP, onMouse_up);
			down();
		}
		
		protected function onMouse_up(e:MouseEvent):void 
		{
			_isDown = false;
			//Dbg.log("[ButtonBaseExt] onMouse_up: e.currentTarget = " + e.currentTarget, Dbg.CH_DEBUG );
			stage.removeEventListener(MouseEvent.MOUSE_UP, onMouse_up);
			root.removeEventListener(MouseEvent.MOUSE_UP, onMouse_up);
			up();
		}
		
		protected override function onRemove(e:Event):void 
		{			
			super.onRemove(e);
			//Dbg.log("[ButtonBaseExt] onRemove: e = " + e, Dbg.CH_DEBUG);
			removeEventListener(MouseEvent.MOUSE_DOWN, onMouse_down);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onMouse_up);
			root.removeEventListener(MouseEvent.MOUSE_UP, onMouse_up);
		}
	
	
		public function ButtonBaseExt() 
		{
			super();
		}
		
		protected override function init():void
		{
			super.init();
			addEventListener(MouseEvent.MOUSE_DOWN, onMouse_down);
			
		}
		
		override public function froze():void 
		{
			super.froze();
			removeEventListener(MouseEvent.MOUSE_DOWN, onMouse_down);
		}
		
		override public function defroze():void 
		{
			super.defroze();
			addEventListener(MouseEvent.MOUSE_DOWN, onMouse_down);
		}
		
		protected function down():void
		{
			
		}
		
		protected function up():void
		{
			
		}
		
	}

}