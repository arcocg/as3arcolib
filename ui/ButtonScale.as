package arco.ui 
{
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Elastic;
	import com.greensock.easing.Back;
	
	/**
	 * ButtonScale
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class ButtonScale extends ButtonBase
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------ButtonScale
		public function ButtonScale() 
		{
			super();
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		protected override function over():void
		{
			TweenMax.to(this, .5, {  scaleX:1.2, scaleY:1.2, ease:Elastic.easeOut} );
		}
		
		protected override function out():void
		{
			TweenMax.to(this, .2, {  scaleX:1, scaleY:1, ease:Back.easeOut } );
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}