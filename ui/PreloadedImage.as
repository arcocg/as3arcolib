﻿package arco.ui 
{
	import arco.loaders.ImageLoader;
	import flash.display.Bitmap;
	import flash.events.Event;
	
	import com.greensock.easing.*;
	import com.greensock.TweenMax;
	
	import arco.ui.preloaders.PreloaderIndicatorBase;
	/**
	 * PreloadedImage
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * Image loader with preloader based on MovieClip extends from PreloaderIndicatorBase
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class PreloadedImage extends ImageLoader
	{	
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		//Event types generating by object
		static public const VERSION_:Number = 0.12;
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		private var _mcPreloader:PreloaderIndicatorBase;
		private var _preloaderX:Number;
		private var _preloaderY:Number;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		private var _fFadeIn:Boolean = true;
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function PreloadedImage(inpreloadermc:PreloaderIndicatorBase) 
		{
			super();
			_mcPreloader = inpreloadermc;
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		private function onAdded(e:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
		}
		
		protected override function onImageLoaded(event:Event):void {
			super.onImageLoaded(event);
			
			if (_fFadeIn) {
				this.alpha = 0;
				TweenMax.to(this, 1, { alpha:1, ease:Sine.easeOut} );
			}
			
			removeChild(_mcPreloader);
			//_mcPreloader = null;
		}
		
		private function onRemoved(e:Event):void {
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
			_loader.unloadAndStop();
			
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		public override function loadImage(inurl:String, inmaxw:int = 0, inmaxh:int = 0, incrop:Boolean = false, inbmp:Bitmap = null):void 
		{
			super.loadImage(inurl, inmaxw, inmaxh, incrop, inbmp);
			_mcPreloader.play();
			addChild(_mcPreloader);
			
			if (inmaxw > 0) _mcPreloader.x = inmaxw >> 1;
			else _mcPreloader.x = _preloaderX;
			if (inmaxh > 0) _mcPreloader.y = inmaxh >> 1;
			else _mcPreloader.y = _preloaderY;
			
		}

		public function setPreloaderPos(inx:Number, iny:Number):void 
		{
			_preloaderX = inx;
			_preloaderY = iny;
		}
		
		//------setHeight		
		//Height of scrollbar would be setted through this function
		public function setHeight(inh:Number):void 
		{
			
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		public function set doFadeIn(inval:Boolean):void {	_fFadeIn = inval; }
		public function get doFadeIn():Boolean {	return(_fFadeIn) }
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		
		
	}

}

