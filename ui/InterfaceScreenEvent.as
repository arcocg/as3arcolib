﻿/**
* Definitions for all event types fired by the JamModel
*
**/
package arco.ui {


import flash.events.Event;


public class InterfaceScreenEvent extends Event {


	/** Definitions for all event types. **/
	public static const HIDED:String = "onScreenHided";
	public static const SHOWED:String = "onScreenShowed";
	
	/** The data associated with the event. **/
	private var _data:Object;

	/**
	* Constructor; sets the event type and inserts the new value.
	*
	* @param typ	The type of event.
	* @param dat	An object with all associated data.
	**/
	public function InterfaceScreenEvent(typ:String,dat:Object=undefined,bbl:Boolean=false,ccb:Boolean=false):void {
		super(typ,bbl,ccb);
		_data = dat;
	};


	/** Returns the data associated with the event. **/
	public function get data():Object {
		return _data;
	};


}


}