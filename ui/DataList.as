package arco.ui 
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
		
	/**
	 * DataList
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class DataList extends Sprite
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		protected var _itemsTotal:uint = 0;
		protected var _holder:Sprite = new Sprite();
		protected var _holderHeight:Number = 0;
		protected var _mask:Sprite = new Sprite();
		protected var _spacer:int; //Spacer between items
		protected var _itemH:int; //Height of item
		protected var _pos:Number = 0; //Position of _holder from 0 to 1
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------DataList
		/**
		 * init
		 * 
		 * @param inw 		Width of list
		 * @param inh 		Height of list
		 * @param initemh 	Height of item placed to list, if 0 - height calculate fro
		 * @param inspacer 	Spacer between items
		**/
		public function DataList(inw:uint, inh:uint, initemh:int = 0, inspacer:int = 0) 
		{
			_mask.graphics.beginFill(0xff0000);
			_mask.graphics.drawRect(0, 0, inw, inh);
			addChild(_holder);
			addChild(_mask);
			_holder.mask = _mask;
			
			_spacer = inspacer;
			_itemH = initemh;
			
			addEventListener(Event.ADDED_TO_STAGE, init);
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
	
		
		/**
		 * Adds one item to list
		 * 
		 * @param initem	Item to add
		**/
		public function addItem(initem:MovieClip):MovieClip {
			
			
			initem.y = (_itemH != 0)?_holderHeight:_holder.height;
			
			if (_holder.numChildren > 0) {
				initem.y += _spacer;
				_holderHeight += _spacer;
			}
			
			if (_itemH != 0) {
				_holderHeight += _itemH;
				
			}else {
				_holderHeight += initem.height;
			}
					
			_holder.addChild(initem);
			_itemsTotal++;
			
			return(initem);
		}
		
		
		
		/**
		 * Removes all items in list
		**/
		public function clear():void 
		{
			while (_holder.numChildren > 0) _holder.removeChildAt(0);
			_itemsTotal = 0;
			_holderHeight = 0;
			position = 0;
		}
		
		/**
		 * Return item in specified position
		**/
		public function getItem(inindex:int):MovieClip 
		{
			return(MovieClip(_holder.getChildAt(inindex)));
		}
		
		/**
		 * Changes size of items' mask
		**/
		public function setSize(inw:int, inh:int):void 
		{
			_mask.graphics.beginFill(0xff0000);
			_mask.graphics.drawRect(0, 0, inw, inh);
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		
		
		/**
		 * Inits list
		**/
		private function init(e:Event):void {
			
			addEventListener(Event.REMOVED_FROM_STAGE, remove);
			removeEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		/**
		 * Removes list contents
		**/
		private function remove(e:Event):void {
			
			removeEventListener(Event.REMOVED_FROM_STAGE, remove);
			addEventListener(Event.ADDED_TO_STAGE, init);
			
		}
		
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		/**
		* Height of container with items in pixels
		**/
		public function get containerHeight():Number 
		{
			return(_holderHeight);
		}
		
		public function set itemHeight(inval:uint):void 
		{
			_itemH = inval;
		}
		
		/**
		* Position of data list container in percents from 0 to 1
		**/
		public function get position():Number 
		{
			return(_pos);
		}
		
		/**
		* Position of data list container in percents from 0 to 1
		**/
		public function set position(inpos:Number):void 
		{
			if (_mask.height > _holderHeight) {
				_holder.y = 0;
				return;
			}
			
			inpos = (inpos > 1)?1:((inpos < 0)?0:inpos);
			_pos = inpos;
			_holder.y = (_mask.height - _holderHeight) * inpos;
		}
		
		
		
		public function set spacer(inval:int):void 
		{
			_spacer = inval;			
		}
		
		public function get totalItems():int { return _itemsTotal; }
	}

}