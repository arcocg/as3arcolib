package arco.ui
{
	import arco.sounds.SoundsLibrary;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	
	/**
	 * ButtonBase
	 * 
	 * @langversion Ac	tionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * Class for button
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class ButtonBase extends MovieClip
	{
		CONFIG::SOUNDS { 
			public var soundID:String = SoundsLibrary.SOUND_HOVER; 
			//Last time when sound has been played
			private var _soundLastTime:Number = 0; 
		}
		protected var _isOver:Boolean = false;
		
		protected function onAdded(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemove);
			init();
		}
		
		protected function onRemove(e:Event):void 
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemove);
			removeEventListener(MouseEvent.ROLL_OVER, onMouse_over);
			removeEventListener(MouseEvent.ROLL_OUT, onMouse_out);
			removeEventListener(MouseEvent.MOUSE_UP, onMouse_mouseUp);
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		
		protected function onMouse_over(e:MouseEvent):void 
		{
			_isOver = true;
			CONFIG::SOUNDS {
				var _t:Number = new Date().time;
				if ((_t - _soundLastTime) > 500) SoundsLibrary.play(SoundsLibrary.SOUND_HOVER);
				
				_soundLastTime  = _t;
			}
			
			over();
		}
		
		protected function onMouse_out(e:MouseEvent):void 
		{
			_isOver = false;
			out();
		}
	
		protected function onMouse_mouseUp(e:MouseEvent):void 
		{
			click();
		}
	
		
		public function froze():void 
		{
			buttonMode = false;
			removeEventListener(MouseEvent.ROLL_OVER, onMouse_over);
			removeEventListener(MouseEvent.ROLL_OUT, onMouse_out);
			removeEventListener(MouseEvent.MOUSE_UP, onMouse_mouseUp);
		}
		
		public function defroze():void 
		{
			buttonMode = true;
			addEventListener(MouseEvent.ROLL_OVER, onMouse_over);
			addEventListener(MouseEvent.ROLL_OUT, onMouse_out);
			addEventListener(MouseEvent.MOUSE_UP, onMouse_mouseUp);
		}
	
		public function ButtonBase() 
		{			
			buttonMode = true;
			tabEnabled = false;
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		protected function init():void
		{
			addEventListener(MouseEvent.ROLL_OVER, onMouse_over);
			addEventListener(MouseEvent.ROLL_OUT, onMouse_out);
			addEventListener(MouseEvent.MOUSE_UP, onMouse_mouseUp);
		}
		
		protected function click():void
		{
			CONFIG::SOUNDS {
				SoundsLibrary.play(SoundsLibrary.SOUND_CLICK);
			}
		}
		
		protected function over():void
		{
			
		}
		
		protected function out():void
		{
			
		}
		
		public function get isFrozen():Boolean
		{
			return(!buttonMode);
		}
		
	}

}