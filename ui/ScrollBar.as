﻿package arco.ui
{
	import arco.utils.Dbg;
	
	import com.greensock.easing.*;
	import com.greensock.TweenNano;
	import flash.geom.Point;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	/**
	 * ScrollBar
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * 
	 * @author arcomailbox@gmail.com
	 */
	
	public class ScrollBar extends MovieClip
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		public var mcSlider:MovieClip;
		public var mcTrackbar:MovieClip;
		public var mcBG:MovieClip;
		
		public var animSpeedPercent:Number = 1;
		
		private var _sliderPosPercent:Number=0;
		private var _skin:MovieClip;
		
		private var _sliderY:Number;
		//private var _c:uint = 0;
		private var _twSBAnim:TweenNano;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function ScrollBar() {
			addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemove_fromStage);
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS	
		//--------------------------------------
		//------onMouse_leave
		private function onMouse_leave(e:MouseEvent):void 
		{
			onSlider_mouse(new MouseEvent("mouseUp"));
		}
		
		//------onSlider_mouse
		private function onSlider_mouse(me:MouseEvent):void 
		{
			switch(me.type){
				case "mouseDown":
					/*if (_twSBAnim) _twSBAnim.kill();
					animSpeedPercent = 0;*/
					mcSlider.startDrag(false, new Rectangle(mcTrackbar.x + (mcTrackbar.width >> 1), mcTrackbar.y, 0, mcTrackbar.height - mcSlider.height + 1));
					dispatchEvent(new ScrollBarEvent(ScrollBarEvent.START_DRAG));
					/*stage.addEventListener(MouseEvent.MOUSE_MOVE, onSlider_mouse);
					root.addEventListener(MouseEvent.MOUSE_MOVE, onSlider_mouse);*/
					stage.addEventListener(MouseEvent.MOUSE_UP, onSlider_mouse);
					root.addEventListener(MouseEvent.MOUSE_UP, onSlider_mouse);
					stage.addEventListener(Event.MOUSE_LEAVE, onMouse_leave);
					addEventListener(Event.ENTER_FRAME, onUpdateSliderPos);
				break;
				case "mouseUp":
					mcSlider.stopDrag();
					/*stage.removeEventListener(MouseEvent.MOUSE_MOVE, onSlider_mouse);
					root.removeEventListener(MouseEvent.MOUSE_MOVE, onSlider_mouse);*/
					dispatchEvent(new ScrollBarEvent(ScrollBarEvent.STOP_DRAG));
					stage.removeEventListener(MouseEvent.MOUSE_UP, onSlider_mouse);
					root.removeEventListener(MouseEvent.MOUSE_UP, onSlider_mouse);
					stage.removeEventListener(Event.MOUSE_LEAVE, onMouse_leave);
					removeEventListener(Event.ENTER_FRAME, onUpdateSliderPos);
					
				break;
				/*case "mouseMove":
					_sliderPosPercent = mcSlider.y / (mcTrackbar.height - mcSlider.height);
					_sliderPosPercent = (_sliderPosPercent<0)?0:((_sliderPosPercent>1)?1:_sliderPosPercent);
					dispatchEvent(new ScrollBarEvent(ScrollBarEvent.POS_UPDATED, { percent:_sliderPosPercent } ));
				break;*/
			}
		}
		
		//------onRemove_fromStage
		private function onRemove_fromStage(e:Event):void 
		{
			
			mcSlider.removeEventListener(MouseEvent.MOUSE_DOWN, onSlider_mouse);
			mcTrackbar.removeEventListener(MouseEvent.MOUSE_DOWN, onTrackbar_mouse);
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemove_fromStage);
			removeEventListener(Event.ENTER_FRAME, onUpdateSliderPos);
			
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		//------onTrackbar_mouse
		private function onTrackbar_mouse(me:MouseEvent):void {
			
			switch(me.type){
				case "mouseDown":
					/*_sliderPosPercent */ percent = (mouseY / mcTrackbar.height);
					dispatchEvent(new ScrollBarEvent(ScrollBarEvent.TRACKBAR_CLICK, { percent:_sliderPosPercent } ));
					TweenNano.to(mcSlider,0.5,{y:(mcTrackbar.height - mcSlider.height)*_sliderPosPercent, ease:Sine.easeOut});
				break;
			}
		}
		
		private function onUpdateSliderPos(e:Event):void 
		{
			if (_sliderY == mcSlider.y) return;
			_sliderPosPercent = mcSlider.y / (mcTrackbar.height - mcSlider.height);
			_sliderPosPercent = (_sliderPosPercent<0)?0:((_sliderPosPercent>1)?1:_sliderPosPercent);
			_sliderY = mcSlider.y;		
			//Dbg.log("[ScrollBar] onUpdateSliderPos: _c = " + _c++, Dbg.CH_DEBUG);
			dispatchEvent(new ScrollBarEvent(ScrollBarEvent.POS_UPDATED, { percent:_sliderPosPercent } ));
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		//------init
		public function init(e:Event=null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			mcSlider.buttonMode = true;
			mcTrackbar.buttonMode = true;
			
			mcSlider.addEventListener(MouseEvent.MOUSE_DOWN, onSlider_mouse);
			mcTrackbar.addEventListener(MouseEvent.MOUSE_DOWN, onTrackbar_mouse);
			
			
		}
		
		public function hide():void { visible = false; }
		
		//------setSize		
		///Size of scrollbar would be setted through this function
		public function setSize(insize:Number):void 
		{	
			mcTrackbar.height = mcBG.height = insize;
		}
		
		public function show():void { visible = true; }
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		///Return position of sroll slider in percent
		public function get percent():Number 
		{			
			return(_sliderPosPercent);
		}
		
		//------set percent
		///Sets position of sroll slider in percent
		public function set percent (inPercent:Number):void 
		{
			inPercent = (inPercent<0)?0:((inPercent>1)?1:inPercent);
			//if (inPercent == _sliderPosPercent) return;
			_sliderPosPercent = inPercent;
			_twSBAnim = TweenNano.to(mcSlider, 1 * animSpeedPercent, { y:(mcTrackbar.height - mcSlider.height) * _sliderPosPercent, ease:Quint.easeOut } );
			dispatchEvent(new ScrollBarEvent(ScrollBarEvent.POS_UPDATED, { percent:_sliderPosPercent } ));
			//mcSlider.y = (mcTrackbar.height - mcSlider.height) * _sliderPosPercent
		}
	}
	
}