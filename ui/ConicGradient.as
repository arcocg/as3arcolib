package arco.ui 
{
	import flash.display.Sprite;
	import flash.geom.Point;
		
	/**
	 * ConicGradient
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class ConicGradient extends Sprite
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function ConicGradient() 
		{
			
			//
			// Generate 4 lists of hex colors with different
			// quantities of "new" colors between each "key" color.
			//var colors1 = fadeHexes ([0xFFFF00,  4, 0x00FFFF,  4, 0xFF00FF,  4, 0xFFFF00]);
			//var colors2 = fadeHexes ([0xFFFF00,  8, 0x00FFFF,  8, 0xFF00FF,  8, 0xFFFF00]);
			//var colors3 = fadeHexes ([0xFFFF00, 16, 0x00FFFF, 16, 0xFF00FF, 16, 0xFFFF00]);
			//var colors4 = fadeHexes ([0xFFFF00, 32, 0x00FFFF, 32, 0xFF00FF, 32, 0xFFFF00]);
			//
			// Draw 4 conic gradients using the colors generated above.
			//conicGradient( 70, 70, 50, 50, 0, colors1);
			//conicGradient(190, 70, 50, 50, 0, colors2);
			//conicGradient(310, 70, 50, 50, 0, colors3);
			//conicGradient(430, 70, 50, 50, 0, colors4);
			//
			//
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//
		//
		// "FadeHexes" accepts an array of hexadecimal colors
		// separated by numbers specifying how many new colors
		// should be generated between the adjacent colors.
		// For example:
		// [Red, 1,        Black, 3,                                  White] yields...
		// [Red, DARK-RED, Black, DARK-GRAY, MIDDLE-GRAY, LIGHT-GRAY, White].
		public function fadeHexes (arry){
			var len = arry.length-1;
			var newArry = [];
			for(var i=0; i<len; i+=2){
				var hex = arry[i];
				var hex2 = arry[i+2];
				newArry.push(hex);
				var r = hex >> 16;
				var g = hex >> 8 & 0xFF;
				var b = hex & 0xFF;
				var rd = (hex2 >> 16)-r;
				var gd = (hex2 >> 8 & 0xFF)-g;
				var bd = (hex2 & 0xFF)-b;
				var steps = arry[i+1]+1;
				for (var j=1; j<steps; j++){
					var ratio = j/steps;
					newArry.push((r+rd*ratio)<<16 | (g+gd*ratio)<<8 | (b+bd*ratio));
				}
			}
			newArry.push(arry[len]);
			return newArry;
		}
		//
		//
		// The "ConicPattern" function reads the 'colors' array and draws an
		// 'N' sided polygon, one side for each color.  The pie-wedge formed
		// by each side and the center point is filled with the specified color.
		/////
		// X = Center of the conic pattern on the X axis.
		// Y = Center of the conic pattern on the Y axis.
		// XR = Width of the radius on the X axis.
		// YR = Width of the radius on the Y axis.
		// ANGLE = Angle of the first color (0= 3 O'clock), (90/360= 6 O'clock), (-60/360= 1 O'clock).
		// COLORS = Array of colors used to fill each slice of the pie.  1 slice for each color.
		public function conicGradient(x, y, xr, yr, angle, colors) {
			//
			// Record the length of the colors array.
			var len = colors.length;
			//
			// Convert the initial angle of rotation to radians.
			var startRadians = angle * 2 * Math.PI;
			//
			// Determine the size of each pie slice.
			var stepRadians = (2 * Math.PI)/len;
			//
			// Find the first point on the circumference.
			var oldX = x + Math.cos(startRadians) * xr;
			var oldY = y + Math.sin(startRadians) * yr;
			//
			// Prepare some variables for future use.
			var newX, newY;
			this.graphics.clear();
			for(var i=0; i<len; i++){
				//
				// Determine the angle of the next point on the circumference.
				newRadians = startRadians + stepRadians*(i+1);
				//
				// Locate the next point on the circumference.
				newX = x + Math.cos(newRadians) * xr;
				newY = y + Math.sin(newRadians) * yr;
				//
				// Get color of pie slice from provided list.
				this.graphics.beginFill(colors[i]);
				//
				// Draw wedge shape.
				with (this.graphics){
					moveTo(oldX, oldY);
					lineTo(x, y);
					lineTo(newX, newY);
					lineTo(oldX, oldY);
					endFill();
				}
				//
				// Store new point cordinates in old variables
				oldX = newX;
				oldY = newY;
			}
		}
		
		//
		//
		// The "ConicPattern" function reads the 'colors' array and draws an
		// 'N' sided polygon, one side for each color.  The pie-wedge formed
		// by each side and the center point is filled with the specified color.
		/////
		// X = Center of the conic pattern on the X axis.
		// Y = Center of the conic pattern on the Y axis.
		// radmin = Minimum donut radius.
		// radmax = Maximum donut radius.
		// ANGLE = Angle of the first color (0= 3 O'clock), (90/360= 6 O'clock), (-60/360= 1 O'clock).
		// COLORS = Array of colors used to fill each slice of the pie.  1 slice for each color.
		public function conicGradientDonut(x, y, radmin, radmax, angle, colors) {
			//
			// Record the length of the colors array.
			var len = colors.length;
			//
			// Convert the initial angle of rotation to radians.
			var startRadians = angle * 2 * Math.PI;
			//
			// Determine the size of each pie slice.
			var stepRadians = (2 * Math.PI)/len;
			//
			// Find the first point on the circumference.
			var oldX = x + Math.cos(startRadians) * radmax;
			var oldY = y + Math.sin(startRadians) * radmax;
			//
			// Prepare some variables for future use.
			var newX, newY;
			this.graphics.clear();
			var pointInner1, pointInner2:Point;
			pointInner1 = new Point();
			pointInner2 = new Point();
			var newRadians:Number = startRadians;
			for (var i = 0; i < len; i++) {
				pointInner1.x = x + Math.cos(newRadians) * radmin;
				pointInner1.y = y + Math.sin(newRadians) * radmin;
				
				//
				// Determine the angle of the next point on the circumference.
				newRadians = startRadians + stepRadians * (i + 1);
				
				pointInner2.x = x + Math.cos(newRadians) * radmin;
				pointInner2.y = y + Math.sin(newRadians) * radmin;
				//
				// Locate the next point on the circumference.
				newX = x + Math.cos(newRadians) * radmax;
				newY = y + Math.sin(newRadians) * radmax;

				//
				// Draw wedge shape.
				with (this.graphics) {
					//
				// Get color of pie slice from provided list.
					beginFill(colors[i]);
					moveTo(oldX, oldY);
					lineTo(pointInner1.x, pointInner1.y);
					lineTo(pointInner2.x, pointInner2.y);
					lineTo(newX, newY);
					lineTo(oldX, oldY);
					endFill();
				}
				//
				// Store new point cordinates in old variables
				oldX = newX;
				oldY = newY;
			}
		}
		
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}