package arco.ui
{
	import arco.ui.InterfaceScreenEvent;
	import arco.ui.Interface;
	import flash.display.MovieClip;
	import flash.events.Event;
	/**
	 * InterfaceBase
	 * 
	 * @playerversion Flash 9.0;
	 * @author arcomailbox@gmail.com
	 */
	public class ScreenBase extends MovieClip implements IScreen
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		protected var _skin:MovieClip;
		protected var _model:Interface;
		protected var _screenNum:int;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function ScreenBase(inskin:MovieClip, inmodel:Interface, innum:int) 
		{
			_skin = inskin;
			_model = inmodel;
			_screenNum = innum;
			addChild(_skin);
			addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
		}
		
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		protected function onRemoved(e:Event):void 
		{	
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
			removeChild(_skin);
			_skin = null;
			_model = null;
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		//------init
		public function init(e:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
		}
		
		public function show():void 
		{
			visible = true;
		}
		
		public function hide():void 
		{
			visible = false;
			dispatchScreenHided();
		}
		
		//------dispatchScreenHided
		//Need to call after screen was showed completely
		protected function dispatchScreenShowed():void 
		{
			
			dispatchEvent(new InterfaceScreenEvent(InterfaceScreenEvent.SHOWED));
		}
		
		//------dispatchScreenHided
		//Need to call after screen was hided completely
		protected function dispatchScreenHided():void 
		{
			
			dispatchEvent(new InterfaceScreenEvent(InterfaceScreenEvent.HIDED));
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		public function get screenNum():int
		{
			return(_screenNum);
		}
		
		public function get skin():MovieClip 
		{
			return(_skin);
		}
	}

}