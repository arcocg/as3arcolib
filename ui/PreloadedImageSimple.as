﻿package arco.ui 
{
	import arco.loaders.ImageLoader;
	import arco.loaders.ImageLoaderSimple;
	import flash.display.Bitmap;
	import flash.events.Event;
	
	import com.greensock.easing.*;
	import com.greensock.TweenMax;
	
	import arco.ui.preloaders.PreloaderIndicatorBase;
	/**
	 * PreloadedImageSimple
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * Image loader with preloader based on MovieClip extends from PreloaderIndicatorBase
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class PreloadedImageSimple extends ImageLoaderSimple
	{	
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		//Event types generating by object
		static public const VERSION_:Number = 0.12;
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		private var _mcPreloader:PreloaderIndicatorBase;
		private var _preloaderX:Number;
		private var _preloaderY:Number;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		private var _fFadeIn:Boolean = true;
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function PreloadedImageSimple(inpreloadermc:PreloaderIndicatorBase) 
		{
			super();
			_mcPreloader = inpreloadermc;
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		private function onAdded(e:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
		}
		
		protected override function onImageLoaded(event:Event):void {
			super.onImageLoaded(event);
			
			if (_fFadeIn) {
				this.alpha = 0;
				TweenMax.to(this, 1, { alpha:1, ease:Sine.easeOut} );
			}
			
			removeChild(_mcPreloader);
		}
		
		private function onRemoved(e:Event):void {
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
			_loader.unloadAndStop();
			
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		public override function loadImage(inurl:String):void 
		{
			super.loadImage(inurl);
			_mcPreloader.play();
			addChild(_mcPreloader);
			
			_mcPreloader.x = _preloaderX;
			_mcPreloader.y = _preloaderY;
			
			
		}

		public function setPreloaderPos(inx:Number, iny:Number):void 
		{
			_preloaderX = inx;
			_preloaderY = iny;
		}
		
		//------setHeight		
		//Height of scrollbar would be setted through this function
		public function setHeight(inh:Number):void 
		{
			
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		public function set doFadeIn(inval:Boolean):void {	_fFadeIn = inval; }
		public function get doFadeIn():Boolean {	return(_fFadeIn) }
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		override public function get width():Number { return _loader.width; }
		
		override public function set width(value:Number):void 
		{
			_loader.width = value;
		}
		
		override public function get height():Number { return _loader.height; }
		
		override public function set height(value:Number):void 
		{
			_loader.height = value;
		}
		
	}

}

