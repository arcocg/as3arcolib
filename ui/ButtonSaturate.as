package arco.ui
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	
	/**
	 * ButtonSaturate
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * Class for button
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class ButtonSaturate extends MovieClip
	{
	
		private var _skin:MovieClip;
		
		protected function onMouse_over(e:MouseEvent):void 
		{
			TweenMax.to(_skin, .3, {  colorMatrixFilter: { saturation:1 }} );
		}
		
		protected function onMouse_out(e:MouseEvent):void 
		{
			TweenMax.to(_skin, 0.5, {  colorMatrixFilter: { saturation:0 }} );
		}
	
		public function ButtonSaturate() 
		{
			_skin = this;
			
			buttonMode = true;
			
			TweenMax.to(_skin, 0, {  colorMatrixFilter: { saturation:0 }} );
			
			addEventListener(MouseEvent.MOUSE_OVER, onMouse_over);
			addEventListener(MouseEvent.MOUSE_OUT, onMouse_out);
		}
		
		public function get isActive():Boolean
		{
			return(buttonMode);
		}
		
	}

}