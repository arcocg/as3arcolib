package arco.ui 
{
	import arco.ui.preloaders.PreloaderIndicatorBase;
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import arco.utils.Dbg;
		
	/**
	 * Extends PreloadedInstage class, adds functionality for load image 
	 * when it in defined area
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class PreloadedInareaImage extends PreloadedImage
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		//Load image parameters
		private var _url:String;
		private var _maxw:int;
		private var _maxh:int;
		private var _crop:Boolean;
		private var _bmp:Bitmap;
		
		private var _rect:Rectangle; //Rectangle in global coordinates, used to decide if loading needs to start. In null - stage is used.
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		private var _fLoadStarted:Boolean = false;
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------PreloadedInstageImage
		public function PreloadedInareaImage(inpreloadermc:PreloaderIndicatorBase) 
		{
			super(inpreloadermc);
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		/**
		 * Would be called in some time interval to check when loading need to start
		 * @param inrect Rectangle in global coordinates, used to decide if loading needs to start. In null - stage is used.
		 * @param inintersect If true - image start load when intersects with area, else - when area contain it
		**/
		public function checkForLoad(inrect:Rectangle = null):Boolean 
		{
			if (isLoaded || _fLoadStarted) return true;
		
			
			if (inrect) _rect = inrect;
			var p:Point = this.localToGlobal(new Point(0, 0));
			
			if (_rect.containsPoint(p)) {
				
				_fLoadStarted = true;
				loadImage(_url, _maxw, _maxh, _crop, _bmp);
				return(true);
			}
			
			return(false);
			
			
		}
		
		/**
		 * Pass parameters for image loading, but not load it instantly
		 * For actual load use checkForLoad() method
		 * 
		 * @param inurl URL of image
		 * @param inrect Rectangle in global coordinates, used to decide if loading needs to start. In null - stage is used.
		 * @param inmaxw Max width of image, if 0 - no resize occured
		 * @param inmaxh Max height of image, if 0 - no resize occured
		 * @param incrop Does image need to crop
		 * @param inbmp Bitmap used to load image to
		**/
		public function loadImageDelayed(inurl:String, inrect:Rectangle = null, inmaxw:int = 0, inmaxh:int = 0,
										incrop:Boolean = false, inbmp:Bitmap = null):void{
			_url = inurl;
			_maxw = inmaxw;
			_maxh = inmaxh;
			_crop = incrop;
			_bmp = inbmp;
			
			_rect = inrect;
			
			if (!_rect) _rect = new Rectangle(0, 0, stage.stageWidth, stage.stageHeight);
											
		}
		
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}