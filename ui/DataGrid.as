package arco.ui 
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	
	import arco.utils.Dbg;
	/**
	 * DataGrid
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class DataGrid extends Sprite
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		private var _itemsTotal:uint = 0;
		private var _holder:Sprite = new Sprite();
		private var _rowWidth:Number = 0;
		private var _holderWidth:Number = 0;
		private var _holderHeight:Number = 0;
		private var _mask:Sprite = new Sprite();
		private var _numColumns:int = 0;
		private var _spacerW:int; //Spacer between colums
		private var _spacerH:int; //Spacer between rows
		private var _itemW:int; //Width of item
		private var _itemH:int; //Height of item
		private var _posX:Number = 0; //Position of _holder by X from 0 to 1
		private var _posY:Number = 0; //Position of _holder by Y from 0 to 1
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------DataGrid
		/**
		 * @param inw 		Width of list
		 * @param inh 		Height of list
		 * @param incolumns Number of columns
		 * @param initemw 	Width of item placed to list, if 0 - width gets from item
		 * @param initemh 	Height of item placed to list, if 0 - height gets from item
		 * @param inspacerw Spacer between columns
		 * @param inspacerh Spacer between rows
		**/
		public function DataGrid(inw:uint, inh:uint, incolumns:int, initemw:int = 0, initemh:int = 0, inspacerw:int = 0, inspacerh:int = 0) 
		{
			_mask.graphics.beginFill(0xff0000);
			_mask.graphics.drawRect(0, 0, inw, inh);
			addChild(_holder);
			addChild(_mask);
			_holder.mask = _mask;
			
			_spacerW = inspacerw;
			_spacerH = inspacerh;
			_numColumns = incolumns;
			
			_itemW = initemw;
			_itemH = initemh;
			
			addEventListener(Event.ADDED_TO_STAGE, init);
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
	
		
		/**
		 * Adds one item to list
		 * 
		 * @param initem	Item to add
		**/
		public function addItem(initem:MovieClip):void {
			var _rowHolder:Sprite;
			
			if (_itemsTotal % _numColumns == 0) {
				//Crate new row
				_rowHolder = new Sprite();
				_rowHolder.y = (_itemH != 0)?_holderHeight:_holder.height;				
				_holder.addChild(_rowHolder);
				_rowWidth = 0;
				
			} else {
				_rowHolder = _holder.getChildAt(_holder.numChildren - 1) as Sprite;
			}
			
			initem.x = (_itemW != 0)?_rowWidth:_rowHolder.width;
			
			//initem.y = (_itemH != 0)?_holderHeight:_holder.height;
			
			
			if ((_itemsTotal % _numColumns  == 0) && (_holder.numChildren - 1) > 0) {
				_rowHolder.y += _spacerH;
				_holderHeight += _spacerH;
			}
			
			if (_rowHolder.numChildren > 0) {
				initem.x += _spacerW;
				_rowWidth += _spacerW;
			}
			
			if (_itemW != 0) {
				_rowWidth += _itemW;
				
			} else {
				_rowWidth += initem.width;
			}
			
			if ((_itemsTotal % _numColumns  == 0)) {
				if (_itemH != 0) {
					_holderHeight += _itemH;
				} else {
					_holderHeight += _rowHolder.height;
				}
			}
			
			if (_itemsTotal < _numColumns) _holderWidth = _rowWidth;
			
			_rowHolder.addChild(initem);
			_itemsTotal++;
		}
		
		/**
		 * Removes all items in list
		**/
		public function clear():void 
		{
			while (_holder.numChildren > 0) _holder.removeChildAt(0);
			_itemsTotal = 0;
			_holderHeight = 0;
			positionX = 0;
			positionY = 0;
		}
		
		/**
		 * Return item in specified position
		**/
		public function getItem(inindex:int):MovieClip 
		{
			return(MovieClip(_holder.getChildAt(inindex)));
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		
		
		/**
		 * Inits list
		**/
		private function init(e:Event):void {
			
			addEventListener(Event.REMOVED_FROM_STAGE, remove);
			removeEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		/**
		 * Removes list contents
		**/
		private function remove(e:Event):void {
			
			removeEventListener(Event.REMOVED_FROM_STAGE, remove);
			addEventListener(Event.ADDED_TO_STAGE, init);
			
		}
		
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		/**
		* Height of container with items in pixels
		**/
		public function get containerHeight():Number 
		{
			return(_holderHeight);
		}
		
		public function set spacerX(inval:int):void 
		{
			_spacerW = inval;
		}
		
		public function set spacerY(inval:int):void 
		{
			_spacerH = inval;
		}
		
		public function set itemHeight(inval:uint):void 
		{
			_itemH = inval;
		}
		
		override public function get mask():DisplayObject { 
			return _mask; 
		}
		
		override public function set mask(value:DisplayObject):void 
		{
			_mask = value as Sprite;
		}
		
		/**
		* Position of data list container by y in percents from 0 to 1
		**/
		public function get positionX():Number 
		{
			return(_posX);
		}
		
		
		public function set positionX(inpos:Number):void 
		{
			if (_mask.width > _holder.width) {
				_holder.x = 0;
				return;
			}
			
			inpos = (inpos > 1)?1:((inpos < 0)?0:inpos);
			_posX = inpos;
			_holder.x = (_mask.width - _holderWidth) * inpos;
		}
		
		public function get positionY():Number 
		{
			return(_posY);
		}
		
		/**
		* Position of data list container in percents from 0 to 1
		**/
		public function set positionY(inpos:Number):void 
		{
			if (_mask.height > _holder.height) {
				_holder.y = 0;
				return;
			}
			
			inpos = (inpos > 1)?1:((inpos < 0)?0:inpos);
			_posY = inpos;
			_holder.y = (_mask.height - _holderHeight) * inpos;
		}
		
		public function get itemsTotal():int { return(_itemsTotal);}
	}

}