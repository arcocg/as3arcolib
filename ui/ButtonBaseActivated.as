﻿package arco.ui 
{
	import flash.events.MouseEvent;
		
	/**
	 * ButtonBaseActivated
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class ButtonBaseActivated extends ButtonBase
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		protected var _active:Boolean = false;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function ButtonBaseActivated() 
		{
			super();
			
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		public function activate():void 
		{
			over();
			_active = true;
		}
		
		public function deactivate():void 
		{
			_active = false;
			out();
		}
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		public function set active(inval:Boolean):void 
		{
			if (!inval && _active) {
				_active = inval;
				deactivate();
				
			}
			else if (inval && !_active) {
				_active = inval;
				activate();
			}
		}
		
		public function get active():Boolean 
		{
			return(_active);
		}
	}

}