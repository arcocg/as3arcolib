package arco.ui 
{
	import flash.display.MovieClip;
	
		
	/**
	 * ProgressBar
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class ProgressBar extends MovieClip
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		public var fill:MovieClip;
		
		private var _max_w:Number = 0;
		private var _percent:Number;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------ProgressBar
		public function ProgressBar() 
		{
			init();
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		 * init
		 * 
		 * @param 
		**/
		public function init():void{
			
		}
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		public function set maxWidth(value:Number):void 
		{
			_max_w = value;
		}
		
		public function set percent(inpercent:Number):void 
		{
			if (_max_w == 0) throw(new Error("[ProgressBar] Set maxWidth first!"));
			
			_percent = inpercent;
			fill.width = inpercent * _max_w;
		}
		
		public function get percent():Number 
		{
			return(_percent);
		}
	}

}