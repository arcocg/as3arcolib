package arco.ui
{
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.display.Sprite;
	
	import arco.ui.InterfaceEvent;
	import arco.ui.InterfaceScreenEvent;
	//import arco.kenthd.packgame.screens.*;
	

	/**
	 * Interface
	 * 
	 * @playerversion Flash 9.0;
	 * @author arcomailbox@gmail.com
	 */
	public class Interface extends MovieClip
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		private var _arrScreens:Array;
		private var _loader:Loader;
		private var _interfacesData:MovieClip;
		private var _curScreen:MovieClip;
		private var _curScreenNum:int=0;
		private var _prevScreenNum:int;
		private var _nextScreenNum:int = 1;
		
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		private var fLoadNextScreen:Boolean = true;
		private var _isLoaded:Boolean = false;
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		///inscreens - array, consists of { skin, screen}, where skin - name of class with graphics, screen - class of screen
		public function Interface(inscreens:Array) 
		{
			_arrScreens = inscreens;
			/*_imageLoader.addEventListener(ProgressEvent.PROGRESS, onDownloadProgress);
			_imageLoader.addEventListener(Event.COMPLETE, onImageDownloaded);*/
		}
		
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//Interface handlers
		//--------------------------------------
		private function onInterfaceLoaded(e:Event):void 
		{
			_isLoaded = true;
			_interfacesData = _loader.content as MovieClip;
			_loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onInterfaceLoaded);
			dispatchEvent(e);
		}
		
		private function onInterfaceLoadProgress(e:ProgressEvent):void 
		{
			dispatchEvent(e);
		}
		
		private function onScreen_hided(e:InterfaceScreenEvent):void 
		{
			unloadScreen(_curScreenNum);
			if (fLoadNextScreen) loadScreen(_nextScreenNum);
		} 
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		public function doAction(inaction:int):void 
		{
			dispatchEvent(new InterfaceEvent(InterfaceEvent.DO_ACTION, { action:inaction } ));
		}
		
		//------goTo
		public function goTo(inscreen:int):void 
		{
			_nextScreenNum = (inscreen<0)?0:(inscreen>(_arrScreens.length-1))?(_arrScreens.length-1):inscreen;
			if (_nextScreenNum != _curScreenNum) {
				_curScreen.hide();
				
			}
		}

		
		//------goBackward
		public function goBackward():void 
		{
			_nextScreenNum = _curScreenNum  - 1;
			_nextScreenNum = (_nextScreenNum < 0)?0:_nextScreenNum;
			if (_nextScreenNum != _curScreenNum) {
				_curScreen.hide();
				
			}
		}
		
		//------goForward
		public function goForward():void 
		{
			_nextScreenNum = _curScreenNum  + 1;
			_nextScreenNum = (_nextScreenNum >= _arrScreens.length)?(_arrScreens.length - 1):_nextScreenNum;
			if (_nextScreenNum != _curScreenNum) {
				_curScreen.hide();
			}
		}
		
		
		public function show(inscreen:int):void 
		{
			visible = true;
			//trace("[Interface] show");
			fLoadNextScreen = true;
			loadScreen(inscreen);
		}
		
		public function hide():void 
		{
			fLoadNextScreen = false;
			if (_curScreen) _curScreen.hide();
		}

		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//------getInterfaceItem
		///Returns interface item by name 
		private function getInterfaceItem(inname:String):Object 
		{
			if (loaderInfo.applicationDomain.hasDefinition(inname)) {
				var ItemClass:Class =  loaderInfo.applicationDomain.getDefinition(inname) as Class;
				return (new ItemClass());
			}
			else return null;
		}
		
		private function loadScreen(innum:int):void 
		{
			
			_prevScreenNum = _curScreenNum;
			
			var skin:MovieClip = getInterfaceItem(_arrScreens[innum].skin) as MovieClip;
			
			if (!skin) {
				trace("[Interface] Skin cannot be loaded!");
				return;
			}
			
			_curScreen = addChild(new _arrScreens[innum].screen(skin, this, innum)) as MovieClip;
			
			_curScreen.addEventListener(InterfaceScreenEvent.HIDED, onScreen_hided);
			_curScreen.show();
			
			_nextScreenNum = _curScreenNum = innum;
			
			dispatchEvent(new InterfaceEvent(InterfaceEvent.INTERFACE_CHANGED,{screen:_curScreenNum}));
		}
		
		private function unloadScreen(innum:int):void 
		{
			_curScreen.removeEventListener(InterfaceScreenEvent.HIDED, onScreen_hided);
			removeChild(_curScreen);
			_curScreen = null;
		}

		
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		//------get isLoaded
		public function get isLoaded():Boolean 
		{
			return _isLoaded;
		}
		
		//------get prevScreenNum
		public function get prevScreenNum():int
		{
			return (_prevScreenNum);
		}
		
		//------get nextScreenNum
		public function get nextScreenNum():int
		{
			return (_nextScreenNum);
		}
		
		public function get curScreen():MovieClip 
		{
			return(_curScreen);
		}
		
		public function get curScreenSkin():MovieClip 
		{
			if (_curScreen) return(_curScreen.skin);
			
			return(null);
		}
	
	}

}