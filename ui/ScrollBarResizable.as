﻿package arco.ui
{
	import arco.ui.ScrollBar;
	import flash.display.MovieClip;
	import flash.events.Event;
	
	
	/**
	 * ScrollBarResizable
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class ScrollBarResizable extends ScrollBar
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		private const UP_HEIGHT:int = 5; //Height of up part
		
		public static const MIN_SLIDER_SIZE:int = 0;
		public static var MAX_SLIDER_SIZE:int = 199;
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		private var _sliderSizePercent:Number; //Size of scroll bar slider
		
		
		
		public var _mcSlider:MovieClip;
		public var _mcTrackbar:MovieClip;
		public var _mcBG1:MovieClip;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------ScrollBarResizable
		public function ScrollBarResizable() 
		{
			super();
			
			mcBG = _mcBG1;
			mcTrackbar = _mcTrackbar;
			mcSlider = _mcSlider;
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		 * init
		 * 
		 * @param 
		**/
		public override function init(e:Event = null):void {
			
			super.init(e);
		}
		
		/**
		 * Set size of scroll bar in pixels
		**/
		override public function setSize(insize:Number):void 
		{
			//super.setSize(insize);
			trackSize = insize;
		}
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		/**
		 * Sets slider size in percent from MIN_SLIDER_SIZE to MAX_SLIDER_SIZE
		 * 
		 * @param insize	Number from 0 to 1
		**/
		public function set sliderSize(insize:Number):void 
		{
			_sliderSizePercent = insize;			
			_mcSlider.center.height = /*MIN_SLIDER_SIZE + (MAX_SLIDER_SIZE - MIN_SLIDER_SIZE)*/ _mcTrackbar.height * insize;
			_mcSlider.down.y = _mcSlider.center.height + (UP_HEIGHT << 1);
			
		}
		
		
		/**
		 * Size slider in percents
		**/
		public function get sliderSize():Number 
		{
			return(_sliderSizePercent);
		}
		
		/**
		 * Sets scrollbar track size in pixels
		 * 
		 * @param insize	Height of trackbar in pixels
		**/
		public function set trackSize(insize:int):void 
		{
			_mcTrackbar.height = insize;
			_mcBG1.center.height = insize - (_mcBG1.up.height + _mcBG1.down.height);
			_mcBG1.down.y = _mcBG1.center.y + _mcBG1.center.height + _mcBG1.down.height;
			MAX_SLIDER_SIZE = insize - UP_HEIGHT * 2 - 2;
			percent = percent;
		}
		
		
		/**
		 * Size of scrollbar track
		**/
		public function get trackSize():int 
		{
			return(_mcBG1.height);
		}
	}

}