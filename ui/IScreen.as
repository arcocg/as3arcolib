package arco.ui
{
	
	/**
	 * IScreen
	 * @author arcomailbox@gmail.com
	 */
	
	public interface IScreen 
	{
		function show():void;	
		function hide():void;
		function get screenNum():int;
	}
	
}