﻿/**
* Definitions for all event types fired by the JPScrollBar
*
**/
package arco.ui {


import flash.events.Event;


public class ScrollBarEvent extends Event {


	/** Definitions for all event types. **/
	public static const TRACKBAR_CLICK:String = "onSliderTrackkBarClick"; //data: percent
	public static const POS_UPDATED:String = "onSliderPosUpdated"; //data: percent
	public static const START_DRAG:String = "onSliderStartDrag"; 
	public static const STOP_DRAG:String = "onSliderStopDragDrag"; 
	
	
	/** The data associated with the event. **/
	private var _data:Object;


	/**
	* Constructor; sets the event type and inserts the new value.
	*
	* @param typ	The type of event.
	* @param dat	An object with all associated data.
	**/
	public function ScrollBarEvent(typ:String,dat:Object=undefined,bbl:Boolean=false,ccb:Boolean=false):void {
		super(typ,bbl,ccb);
		_data = dat;
	};


	/** Returns the data associated with the event. **/
	public function get data():Object {
		return _data;
	};


}


}