package arco.ui.preloaders
{
	import flash.display.MovieClip;
	
		
	/**
	 * Base class for preloader indicators
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class PreloaderIndicatorBase extends MovieClip
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		private var _percent:Number;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------PreloaderIndicatorBase
		public function PreloaderIndicatorBase() 
		{
			
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		 * Set percent to indicator
		 * 
		 * @param inpercent Percent from 0 to 1
		**/
		public function setPercent(inpercent:Number):void{
			_percent = inpercent;
		}
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}