﻿package arco.ui.preloaders
{
	import flash.display.Sprite;
	import flash.events.Event;

	/**
	 * PreloaderIndicatorSm
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0
	 * 
	 * @author arcomailbox@gmail.com
	 */
	
	public class PreloaderIndicatorSm extends PreloaderIndicatorSmSkin
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function PreloaderIndicatorSm(){
			this.addEventListener(Event.REMOVED, onRemoved);
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		private function onEnterFrame(e:Event):void 
		{
			content.rotation += 18;
		}
		
		private function onRemoved(e:Event):void 
		{
			this.removeEventListener(Event.REMOVED, onRemoved);
			this.stop();
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		//------init
		public function init():void{
			
		}
		
		
		public function start():void 
		{
			this.addEventListener(Event.ENTER_FRAME,  onEnterFrame);
		}
		
		public override function stop():void 
		{
			this.removeEventListener(Event.ENTER_FRAME,  onEnterFrame);
		}
		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}
	
}