package arco.ui 
{
	import arco.utils.Dbg;
	import flash.display.Sprite;
	import flash.events.*;
	
	/**
	 * MouseSpeed
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 10.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */
	public class MouseSpeed extends Sprite
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------

		private var newY:Number = 0;
		private var oldY:Number = 0;
		public var ySpeed:Number;
		private var newX:Number = 0;
		private var oldX:Number = 0;
		public var xSpeed:Number;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		//------MouseSpeed
		public function MouseSpeed() 
		{
			 //this.addEventListener(Event.ENTER_FRAME, calculateMouseSpeed);
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		 public function calculateMouseSpeed(e:Event = null):void	
		 {
			
			  newY = mouseY;
			  ySpeed = newY - oldY;
			  oldY = newY;
			  
			  newX = mouseX;
			  xSpeed = newX - oldX;
			  oldX = newX;
		  }
  		
		  public function getYSpeed():Number
		  {
			  return ySpeed;
		  }
  		
		  public function getXSpeed():Number
		  {
			  return xSpeed;
		  }
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
	}

}