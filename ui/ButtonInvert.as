﻿package arco.ui
{
	import arco.utils.Dbg;
	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	import arco.ui.ButtonBaseExt;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	/**
	 * ButtonAlpha
	 * 
	 * @langversion ActionScript 3.0;
	 * @playerversion Flash 9.0;
	 * 
	 * @author arcomailbox@gmail.com
	 */ 
	
	
	
	public class ButtonInvert extends ButtonBaseExt
	{
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// CLASS VARIABLES
		//--------------------------------------
		public var mcDown:MovieClip;
		//--------------------------------------
		// BOOLEANS
		//--------------------------------------
		//private var _active:Boolean = false;
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function ButtonInvert() 
		{
			mcDown.alpha = 0;
		}
		//--------------------------------------
		//  EVENT-HANDLERS
		//--------------------------------------
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		
		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------
		protected override function over():void 
		{
			//if (_active) return;
			super.over();
			TweenMax.to(bg, .3, {  colorTransform: { redOffset:30, greenOffset:30, blueOffset:30 },  glowFilter:{color:0xFFFFFF, strength:1, quality:2, blurX:10,blurX:10}} );
		}
		
		protected override function out():void 
		{
			//if (_active) return;
			super.out();
			TweenMax.to(bg, 0.5, {  colorTransform: { redOffset:0, greenOffset:0, blueOffset:0 }, glowFilter:{color:0xFFFFFF, strength:0, quality:2, blurX:0,blurX:0}} );
		}
		
		protected override function up():void 
		{
			//if (_active) return;
			//stage.removeEventListener(MouseEvent.MOUSE_UP, onMouse_up);
			TweenMax.to(mcDown, 0.4, { alpha:0, ease:Sine.easeOut } );
			super.up();
		}
		
		protected override function down():void 
		{
			//stage.addEventListener(MouseEvent.MOUSE_UP, onMouse_up);
			TweenMax.to(mcDown, 0.2, { alpha:0.5, ease:Sine.easeOut } );
			super.down();
		}
		
		override public function froze():void 
		{
			super.froze();
			TweenMax.to(bg, 0, {  colorMatrixFilter:{saturation:0}} );
		}
		
		override public function defroze():void 
		{
			super.defroze();
			TweenMax.to(bg, 0, {  colorMatrixFilter:{saturation:1}} );
		}
		//--------------------------------------
		//  GETTERS AND SETTERS
		//--------------------------------------
		
	}

}